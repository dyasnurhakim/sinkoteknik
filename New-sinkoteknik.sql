/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.8-MariaDB : Database - sinkoteknik
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sinkoteknik` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sinkoteknik`;

/*Table structure for table `bahan_baku` */

DROP TABLE IF EXISTS `bahan_baku`;

CREATE TABLE `bahan_baku` (
  `id` varchar(30) NOT NULL,
  `nama_bb` varchar(30) NOT NULL,
  `satuan` varchar(15) DEFAULT NULL,
  `harga` bigint(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bahan_baku` */

LOCK TABLES `bahan_baku` WRITE;

insert  into `bahan_baku`(`id`,`nama_bb`,`satuan`,`harga`) values ('B-0001','Sulfur','Kg',1200),('B-0002','Bahan Tekstil','Kg',2000),('B-0003','Lem','Kg',1000),('B-0004','Burner','Kali',2000),('B-0005','Silinder','Kg',17000),('B-0006','Gigik',NULL,750000),('B-0007','Kuningan','Plat',400000),('B-0008','Kanal(100mx6m)',NULL,375000),('B-0009','Plat Strip','Kg',125000),('B-0010','Plat Besi','Kg',100000),('B-0011','As Brance','Kali',70000),('B-0012','Plastik','Kg',1500),('B-0013','Karet','Kg',20000);

UNLOCK TABLES;

/*Table structure for table `bop` */

DROP TABLE IF EXISTS `bop`;

CREATE TABLE `bop` (
  `kd_bop` varchar(20) NOT NULL,
  `nm_bop` varchar(150) DEFAULT NULL,
  `satuan_bop` varchar(20) DEFAULT NULL,
  `kd_kelompok_bop` int(11) DEFAULT NULL,
  `kd_coa` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_bop`),
  KEY `kd_kelompok_bop` (`kd_kelompok_bop`),
  KEY `kd_coa` (`kd_coa`),
  CONSTRAINT `bop_ibfk_1` FOREIGN KEY (`kd_kelompok_bop`) REFERENCES `kelompok_bop` (`kd_kelompok_bop`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bop_ibfk_2` FOREIGN KEY (`kd_coa`) REFERENCES `coa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bop` */

LOCK TABLES `bop` WRITE;

insert  into `bop`(`kd_bop`,`nm_bop`,`satuan_bop`,`kd_kelompok_bop`,`kd_coa`) values ('AKTV-0001','Biaya Kemasan','Buah',NULL,4),('AKTV-0002','Ongkos Kirim','Kali',NULL,3),('AKTV-0003','Pembelian Perawatan Mesin','Kali',2,18),('AKTV-0004','Service Mesin','Kali',2,5),('AKTV-0005','Biaya Listrik','Kali',1,17),('AKTV-0006','Biaya PDAM','Kali',1,16);

UNLOCK TABLES;

/*Table structure for table `ci_session` */

DROP TABLE IF EXISTS `ci_session`;

CREATE TABLE `ci_session` (
  `session_id` varchar(255) DEFAULT NULL,
  `ip_address` varchar(13) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ci_session` */

LOCK TABLES `ci_session` WRITE;

insert  into `ci_session`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('f88f83fc8483aa7210311798bae8b345','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";a:5:{s:11:\"kd_produksi\";s:6:\"P-0001\";s:12:\"id_pelanggan\";s:8:\"PEL-0001\";s:10:\"kd_pesanan\";s:8:\"TRX-0001\";s:12:\"nm_pelanggan\";s:10:\"PT. SOLUSI\";s:13:\"tgl_transaksi\";s:10:\"2018-01-27\";}s:15:\"karyawan_produk\";a:1:{s:8:\"IDP-0001\";a:3:{s:9:\"PKRJ-0001\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0002\";s:11:\"nm_karyawan\";s:7:\"Dyas NS\";s:5:\"tarif\";s:5:\"70000\";}}}s:9:\"PKRJ-0002\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0006\";s:11:\"nm_karyawan\";s:8:\"Musyaffa\";s:5:\"tarif\";s:5:\"30000\";}}}s:9:\"PKRJ-0004\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0007\";s:11:\"nm_karyawan\";s:3:\"Adi\";s:5:\"tarif\";s:5:\"20000\";}}}}}}}'),('e383108043b8adc5eae0c9e859a64a46','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('80da00717be18dae20863968247e6965','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('25132c41d897b15c2b473a1b6a649878','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('819f72e14889a0a323e0be3ebf666bff','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('1c1cbb467e0f9873ec3ce5e06eac6159','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('285c79f57a9c75c660deb76d3c997d6b','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('57f1e9254abcdc32978c25bc86511437','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('810db037539619462f48e3f70ef998ad','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('13409af06e8108d305e01ba0400d5139','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('068183dbf1e87a5ea0900f553e08e01c','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('957e07000deb052e435e875ed51a8a6b','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('620aae044a75ae780caba7f59015b8eb','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('f78558736f7aac1ccea5acb0f7ac69cd','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('fbba14c5991a0c90049fc6336f832896','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('2349263168b0b6b3b0532368e94c59d0','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0','0000-00-00 00:00:00',''),('aeb27b4ef1a91e96002ea1c3d87bf1c5','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('26a2942a6c354842718920f4384db1ac','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";a:2:{s:9:\"pekerjaan\";a:3:{s:5:\"total\";i:0;s:4:\"last\";i:3;s:4:\"data\";a:3:{i:0;a:4:{s:12:\"kd_pekerjaan\";s:9:\"PKRJ-0002\";s:12:\"nm_pekerjaan\";s:5:\"Press\";s:8:\"jml_hari\";s:2:\"10\";s:5:\"harga\";N;}i:1;a:4:{s:12:\"kd_pekerjaan\";s:9:\"PKRJ-0003\";s:12:\"nm_pekerjaan\";s:8:\"Borpriss\";s:8:\"jml_hari\";s:1:\"5\";s:5:\"harga\";N;}i:2;a:4:{s:12:\"kd_pekerjaan\";s:9:\"PKRJ-0004\";s:12:\"nm_pekerjaan\";s:9:\"Finishing\";s:8:\"jml_hari\";s:1:\"2\";s:5:\"harga\";N;}}}s:5:\"bbaku\";a:3:{s:5:\"total\";i:0;s:4:\"last\";i:1;s:4:\"data\";a:2:{i:0;a:3:{s:8:\"kd_bbaku\";s:6:\"B-0012\";s:8:\"nm_bbaku\";s:7:\"Plastik\";s:3:\"qty\";s:3:\"100\";}i:1;N;}}}s:6:\"produk\";a:4:{s:9:\"kd_produk\";s:8:\"IDP-0004\";s:9:\"nm_produk\";s:13:\"Rubber Stripe\";s:5:\"harga\";s:7:\"2500000\";s:13:\"tgl_transaksi\";s:10:\"2018-02-07\";}s:15:\"pembelian_bbaku\";a:5:{s:3:\"tgl\";s:10:\"2018-02-07\";s:8:\"kodeunik\";s:8:\"PBB-0007\";s:5:\"total\";i:11375000;s:4:\"last\";i:3;s:4:\"data\";a:3:{i:0;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0008\";s:7:\"nama_bb\";s:14:\"Kanal(100mx6m)\";s:3:\"qty\";s:2:\"20\";s:5:\"harga\";s:6:\"375000\";}i:1;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0009\";s:7:\"nama_bb\";s:10:\"Plat Strip\";s:3:\"qty\";s:1:\"5\";s:5:\"harga\";s:6:\"225000\";}i:2;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0010\";s:7:\"nama_bb\";s:9:\"Plat Besi\";s:3:\"qty\";s:2:\"25\";s:5:\"harga\";s:6:\"110000\";}}}s:18:\"supplier_pembelian\";O:8:\"stdClass\":5:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:15:\"alamat_supplier\";s:34:\"Jl. Arjuna No.52 Blok J 22 Bandung\";s:14:\"email_supplier\";s:1:\"-\";s:12:\"tlp_supplier\";s:12:\"085100267949\";}s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('c877f7a2ef1d218a982fe8fc4c9aa767','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('ca2216ba62e14463be3e365088687c92','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";a:5:{s:3:\"tgl\";s:10:\"2018-02-07\";s:8:\"kodeunik\";s:8:\"PBB-0007\";s:5:\"total\";i:11375000;s:4:\"last\";i:3;s:4:\"data\";a:3:{i:0;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0008\";s:7:\"nama_bb\";s:14:\"Kanal(100mx6m)\";s:3:\"qty\";s:2:\"20\";s:5:\"harga\";s:6:\"375000\";}i:1;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0009\";s:7:\"nama_bb\";s:10:\"Plat Strip\";s:3:\"qty\";s:1:\"5\";s:5:\"harga\";s:6:\"225000\";}i:2;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0010\";s:7:\"nama_bb\";s:9:\"Plat Besi\";s:3:\"qty\";s:2:\"25\";s:5:\"harga\";s:6:\"110000\";}}}s:18:\"supplier_pembelian\";O:8:\"stdClass\":5:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:15:\"alamat_supplier\";s:34:\"Jl. Arjuna No.52 Blok J 22 Bandung\";s:14:\"email_supplier\";s:1:\"-\";s:12:\"tlp_supplier\";s:12:\"085100267949\";}s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('531052bad965c4cd8539fcff00545d09','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";a:5:{s:11:\"kd_produksi\";s:6:\"P-0008\";s:12:\"id_pelanggan\";s:8:\"PEL-0002\";s:10:\"kd_pesanan\";s:8:\"TRX-0007\";s:12:\"nm_pelanggan\";s:28:\"PT. TOKAI TEXPRINT INDONESIA\";s:13:\"tgl_transaksi\";s:10:\"2018-02-07\";}s:15:\"karyawan_produk\";a:0:{}}}'),('59a31cb45b783d502ddc970f603b16bb','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('eb9c9e71365e39e72fa9d006474a82c8','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('9de9156e19ebba014f7671fe238eb394','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('9d4dde47ace1a0bacdcbba245e281437','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";a:6:{s:3:\"tgl\";s:10:\"2018-02-07\";s:8:\"kodeunik\";s:9:\"PNRL-0005\";s:10:\"keterangan\";s:36:\"Pengeluaran produksi PD.Sinko Teknik\";s:5:\"total\";i:850000;s:4:\"last\";i:3;s:4:\"data\";a:3:{i:0;a:6:{s:6:\"kd_bop\";s:9:\"AKTV-0002\";s:6:\"nm_bop\";s:12:\"Ongkos Kirim\";s:10:\"satuan_bop\";s:4:\"Kali\";s:6:\"id_coa\";s:1:\"3\";s:9:\"kebutuhan\";s:1:\"1\";s:7:\"nominal\";s:6:\"300000\";}i:1;a:6:{s:6:\"kd_bop\";s:9:\"AKTV-0005\";s:6:\"nm_bop\";s:13:\"Biaya Listrik\";s:10:\"satuan_bop\";s:4:\"Kali\";s:6:\"id_coa\";N;s:9:\"kebutuhan\";s:1:\"1\";s:7:\"nominal\";s:6:\"300000\";}i:2;a:6:{s:6:\"kd_bop\";s:9:\"AKTV-0006\";s:6:\"nm_bop\";s:10:\"Biaya PDAM\";s:10:\"satuan_bop\";s:4:\"Kali\";s:6:\"id_coa\";N;s:9:\"kebutuhan\";s:1:\"1\";s:7:\"nominal\";s:6:\"250000\";}}}s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('317ccb7065c47f7b2b18b148b4011216','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('d38aa8f8996a48a21b7fb8f84d5606ea','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('3e321833e520ee69dd1be729a3e9812d','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('365b84487a47fc3b6c95eb1e4ebfff32','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','0000-00-00 00:00:00',''),('47e265b74bee81c6ec6dc0331b605ba6','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('09949262e522f5f110f5878d0492110b','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00',''),('704c6d6df02955d9721cfeb2487c7ccd','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('420bde0769d8042db4376575acb6fe68','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('28cbd9947172fa7fcaab5497865d8add','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";a:3:{s:9:\"pekerjaan\";a:3:{s:5:\"total\";i:0;s:4:\"last\";i:3;s:4:\"data\";a:3:{i:0;a:4:{s:12:\"kd_pekerjaan\";s:9:\"PKRJ-0002\";s:12:\"nm_pekerjaan\";s:5:\"Press\";s:8:\"jml_hari\";s:2:\"10\";s:5:\"harga\";N;}i:1;a:4:{s:12:\"kd_pekerjaan\";s:9:\"PKRJ-0003\";s:12:\"nm_pekerjaan\";s:8:\"Borpriss\";s:8:\"jml_hari\";s:1:\"5\";s:5:\"harga\";N;}i:2;a:4:{s:12:\"kd_pekerjaan\";s:9:\"PKRJ-0004\";s:12:\"nm_pekerjaan\";s:9:\"Finishing\";s:8:\"jml_hari\";s:1:\"2\";s:5:\"harga\";N;}}}s:5:\"bbaku\";a:3:{s:5:\"total\";i:0;s:4:\"last\";i:2;s:4:\"data\";a:2:{i:0;a:4:{s:8:\"kd_bbaku\";s:6:\"B-0012\";s:8:\"nm_bbaku\";s:7:\"Plastik\";s:6:\"satuan\";s:2:\"Kg\";s:3:\"qty\";s:2:\"10\";}i:1;a:4:{s:8:\"kd_bbaku\";s:6:\"B-0013\";s:8:\"nm_bbaku\";s:5:\"Karet\";s:6:\"satuan\";s:2:\"Kg\";s:3:\"qty\";s:2:\"10\";}}}s:3:\"bop\";a:3:{s:5:\"total\";i:1000;s:4:\"last\";i:1;s:4:\"data\";a:1:{i:0;a:5:{s:6:\"kd_bop\";s:9:\"AKTV-0001\";s:6:\"nm_bop\";s:13:\"Biaya Kemasan\";s:6:\"satuan\";s:4:\"Kilo\";s:9:\"kebutuhan\";s:2:\"10\";s:7:\"nominal\";s:3:\"100\";}}}}s:6:\"produk\";a:4:{s:9:\"kd_produk\";s:8:\"IDP-0004\";s:9:\"nm_produk\";s:13:\"Rubber Stripe\";s:5:\"harga\";s:7:\"2500000\";s:13:\"tgl_transaksi\";s:10:\"2018-02-19\";}s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('ec5e3cbb484e210c2f1b157512bb5593','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('9e845c20faccbaa6208976db78d13a33','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('310279efcacaa96070a86b67502d476e','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";a:5:{s:3:\"tgl\";s:10:\"2018-02-24\";s:8:\"kodeunik\";s:8:\"PBB-0001\";s:5:\"total\";i:14050000;s:4:\"last\";i:5;s:4:\"data\";a:5:{i:0;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0008\";s:7:\"nama_bb\";s:14:\"Kanal(100mx6m)\";s:3:\"qty\";s:2:\"20\";s:5:\"harga\";s:6:\"375000\";}i:1;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0009\";s:7:\"nama_bb\";s:10:\"Plat Strip\";s:3:\"qty\";s:2:\"10\";s:5:\"harga\";s:6:\"125000\";}i:2;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0010\";s:7:\"nama_bb\";s:9:\"Plat Besi\";s:3:\"qty\";s:2:\"30\";s:5:\"harga\";s:6:\"100000\";}i:3;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0012\";s:7:\"nama_bb\";s:7:\"Plastik\";s:3:\"qty\";s:3:\"200\";s:5:\"harga\";s:4:\"1500\";}i:4;a:6:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:5:\"kd_bb\";s:6:\"B-0013\";s:7:\"nama_bb\";s:5:\"Karet\";s:3:\"qty\";s:3:\"100\";s:5:\"harga\";s:5:\"20000\";}}}s:18:\"supplier_pembelian\";O:8:\"stdClass\":5:{s:11:\"kd_supplier\";s:6:\"S-0003\";s:11:\"nm_supplier\";s:20:\"RAKAN PRATAMA MANDIR\";s:15:\"alamat_supplier\";s:34:\"Jl. Arjuna No.52 Blok J 22 Bandung\";s:14:\"email_supplier\";s:1:\"-\";s:12:\"tlp_supplier\";s:12:\"085100267949\";}s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}'),('837ed8a61c4aec27a14f0331016e6075','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";a:5:{s:11:\"kd_produksi\";s:6:\"P-0001\";s:12:\"id_pelanggan\";s:8:\"PEL-0002\";s:10:\"kd_pesanan\";s:8:\"TRX-0001\";s:12:\"nm_pelanggan\";s:28:\"PT. TOKAI TEXPRINT INDONESIA\";s:13:\"tgl_transaksi\";s:10:\"2018-02-24\";}s:15:\"karyawan_produk\";a:3:{s:8:\"IDP-0004\";a:3:{s:9:\"PKRJ-0002\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0006\";s:11:\"nm_karyawan\";s:8:\"Musyaffa\";s:5:\"tarif\";s:5:\"30000\";}}}s:9:\"PKRJ-0003\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0008\";s:11:\"nm_karyawan\";s:5:\"Danny\";s:5:\"tarif\";s:5:\"35000\";}}}s:9:\"PKRJ-0004\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0007\";s:11:\"nm_karyawan\";s:3:\"Adi\";s:5:\"tarif\";s:5:\"20000\";}}}}s:8:\"IDP-0005\";a:3:{s:9:\"PKRJ-0001\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0010\";s:11:\"nm_karyawan\";s:5:\"Bicky\";s:5:\"tarif\";s:5:\"75000\";}}}s:9:\"PKRJ-0002\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0006\";s:11:\"nm_karyawan\";s:8:\"Musyaffa\";s:5:\"tarif\";s:5:\"30000\";}}}s:9:\"PKRJ-0004\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0007\";s:11:\"nm_karyawan\";s:3:\"Adi\";s:5:\"tarif\";s:5:\"20000\";}}}}s:8:\"IDP-0006\";a:3:{s:9:\"PKRJ-0001\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0010\";s:11:\"nm_karyawan\";s:5:\"Bicky\";s:5:\"tarif\";s:5:\"75000\";}}}s:9:\"PKRJ-0003\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0008\";s:11:\"nm_karyawan\";s:5:\"Danny\";s:5:\"tarif\";s:5:\"35000\";}}}s:9:\"PKRJ-0004\";a:1:{s:8:\"karyawan\";a:1:{i:0;a:3:{s:11:\"id_karyawan\";s:6:\"K-0007\";s:11:\"nm_karyawan\";s:3:\"Adi\";s:5:\"tarif\";s:5:\"20000\";}}}}}}}'),('5629f25809c539a8cc95f48967c43d6e','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36','0000-00-00 00:00:00','a:2:{s:9:\"user_data\";s:0:\"\";s:7:\"session\";a:9:{s:4:\"user\";a:4:{s:7:\"isLogin\";b:1;s:7:\"user_id\";s:6:\"K-0001\";s:8:\"username\";s:12:\"dyasnurhakim\";s:5:\"level\";s:5:\"Admin\";}s:13:\"produk_detail\";b:0;s:6:\"produk\";b:0;s:15:\"pembelian_bbaku\";b:0;s:18:\"supplier_pembelian\";b:0;s:16:\"pembelian_produk\";b:0;s:18:\"detail_pengeluaran\";b:0;s:15:\"produksi_produk\";b:0;s:15:\"karyawan_produk\";b:0;}}');

UNLOCK TABLES;

/*Table structure for table `coa` */

DROP TABLE IF EXISTS `coa`;

CREATE TABLE `coa` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `kd_akun` int(8) DEFAULT NULL,
  `nama_akun` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `coa` */

LOCK TABLES `coa` WRITE;

insert  into `coa`(`id`,`kd_akun`,`nama_akun`) values (1,311,'Modal'),(2,111,'Kas'),(3,611,'Biaya Ongkos Kirim'),(4,333,'Biaya Kemasan'),(5,511,'Biaya Setup Mesin'),(6,111,'Saldo Awal'),(7,444,'Pembelian Bahan Baku'),(8,411,'Pendapatan'),(9,123,'BDP Bahan Baku'),(10,456,'Persediaan Bahan Baku'),(11,789,'Beban Gaji'),(12,234,'Main Overhead'),(13,567,'BDP BOP'),(14,987,'BDP BTKL'),(15,654,'Persediaan Barang Jadi'),(16,613,'Biaya PDAM'),(17,987,'Biaya Listrik'),(18,234,'Preawatan Mesin');

UNLOCK TABLES;

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` varchar(30) NOT NULL,
  `nama_pelanggan` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `tlp` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

LOCK TABLES `customer` WRITE;

insert  into `customer`(`id`,`nama_pelanggan`,`alamat`,`email`,`tlp`) values ('PEL-0001','PT. SOLUSI','Bandung','solusi@gmail.com','098276453'),('PEL-0002','PT. TOKAI TEXPRINT INDONESIA','Jl. Jababeka Raya Kav. B4-10, ','-','021-8934142'),('PEL-0003','PT CONDONG GARUT','Jl.Pameungpeuk Garut','-','0813203456');

UNLOCK TABLES;

/*Table structure for table `detail_bbb` */

DROP TABLE IF EXISTS `detail_bbb`;

CREATE TABLE `detail_bbb` (
  `kd_produksi` varchar(20) NOT NULL,
  `kd_bb` varchar(20) NOT NULL,
  `kd_produk` varchar(30) NOT NULL,
  `qty_bbb` float NOT NULL,
  `hrg_bbb` float NOT NULL,
  `sub_bbb` float NOT NULL,
  PRIMARY KEY (`kd_produksi`,`kd_bb`,`kd_produk`),
  KEY `kd_bb` (`kd_bb`),
  KEY `kd_produk` (`kd_produk`),
  CONSTRAINT `detail_bbb_ibfk_1` FOREIGN KEY (`kd_produksi`) REFERENCES `produksi` (`kd_produksi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_bbb_ibfk_2` FOREIGN KEY (`kd_bb`) REFERENCES `bahan_baku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_bbb_ibfk_3` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_bbb` */

LOCK TABLES `detail_bbb` WRITE;

UNLOCK TABLES;

/*Table structure for table `detail_bbb_produk` */

DROP TABLE IF EXISTS `detail_bbb_produk`;

CREATE TABLE `detail_bbb_produk` (
  `kd_produk` varchar(30) NOT NULL,
  `kd_bbaku` varchar(30) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_produk`,`kd_bbaku`),
  KEY `kd_bbaku` (`kd_bbaku`),
  CONSTRAINT `detail_bbb_produk_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_bbb_produk_ibfk_2` FOREIGN KEY (`kd_bbaku`) REFERENCES `bahan_baku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_bbb_produk` */

LOCK TABLES `detail_bbb_produk` WRITE;

insert  into `detail_bbb_produk`(`kd_produk`,`kd_bbaku`,`qty`) values ('IDP-0004','B-0012',10),('IDP-0004','B-0013',10),('IDP-0005','B-0009',1),('IDP-0005','B-0010',1),('IDP-0006','B-0008',2),('IDP-0006','B-0010',2),('IDP-0006','B-0012',10);

UNLOCK TABLES;

/*Table structure for table `detail_bop` */

DROP TABLE IF EXISTS `detail_bop`;

CREATE TABLE `detail_bop` (
  `kd_produksi` varchar(20) NOT NULL,
  `kd_bop` varchar(20) NOT NULL,
  `kd_produk` varchar(30) NOT NULL,
  `qty_bop` int(11) DEFAULT NULL,
  `nominal_bop` int(11) DEFAULT NULL,
  `subtotal_bop` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_produksi`,`kd_bop`,`kd_produk`),
  KEY `kd_bop` (`kd_bop`),
  KEY `kd_produk` (`kd_produk`),
  CONSTRAINT `detail_bop_ibfk_1` FOREIGN KEY (`kd_produksi`) REFERENCES `produksi` (`kd_produksi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_bop_ibfk_2` FOREIGN KEY (`kd_bop`) REFERENCES `bop` (`kd_bop`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_bop_ibfk_3` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_bop` */

LOCK TABLES `detail_bop` WRITE;

UNLOCK TABLES;

/*Table structure for table `detail_bop_produk` */

DROP TABLE IF EXISTS `detail_bop_produk`;

CREATE TABLE `detail_bop_produk` (
  `kd_produk` varchar(30) NOT NULL,
  `kd_bop` varchar(20) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_produk`,`kd_bop`),
  KEY `kd_bop` (`kd_bop`),
  CONSTRAINT `detail_bop_produk_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_bop_produk_ibfk_2` FOREIGN KEY (`kd_bop`) REFERENCES `bop` (`kd_bop`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_bop_produk` */

LOCK TABLES `detail_bop_produk` WRITE;

insert  into `detail_bop_produk`(`kd_produk`,`kd_bop`,`qty`,`nominal`) values ('IDP-0004','AKTV-0001',1,2000),('IDP-0005','AKTV-0001',1,2000),('IDP-0006','AKTV-0001',1,2000);

UNLOCK TABLES;

/*Table structure for table `detail_btkl` */

DROP TABLE IF EXISTS `detail_btkl`;

CREATE TABLE `detail_btkl` (
  `kd_produksi` varchar(20) NOT NULL,
  `kd_pekerjaan` varchar(20) NOT NULL,
  `kd_produk` varchar(20) NOT NULL,
  `id_karyawan` varchar(15) NOT NULL,
  `jumlah_jam` int(11) DEFAULT NULL,
  `tarif_btkl` int(11) DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  PRIMARY KEY (`kd_produksi`,`kd_pekerjaan`,`kd_produk`,`id_karyawan`),
  KEY `kd_pekerjaan` (`kd_pekerjaan`),
  KEY `kd_produk` (`kd_produk`),
  KEY `id_karyawan` (`id_karyawan`),
  CONSTRAINT `detail_btkl_ibfk_1` FOREIGN KEY (`kd_pekerjaan`) REFERENCES `pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_btkl_ibfk_3` FOREIGN KEY (`kd_produksi`) REFERENCES `produksi` (`kd_produksi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_btkl_ibfk_4` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_btkl_ibfk_5` FOREIGN KEY (`id_karyawan`) REFERENCES `karyawan` (`id_karyawan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_btkl` */

LOCK TABLES `detail_btkl` WRITE;

UNLOCK TABLES;

/*Table structure for table `detail_btkl_produk` */

DROP TABLE IF EXISTS `detail_btkl_produk`;

CREATE TABLE `detail_btkl_produk` (
  `kd_produk` varchar(30) NOT NULL,
  `kd_pekerjaan` varchar(30) NOT NULL,
  `jumlah_hari` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_produk`,`kd_pekerjaan`),
  KEY `kd_pekerjaan` (`kd_pekerjaan`),
  CONSTRAINT `detail_btkl_produk_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_btkl_produk_ibfk_2` FOREIGN KEY (`kd_pekerjaan`) REFERENCES `pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_btkl_produk` */

LOCK TABLES `detail_btkl_produk` WRITE;

insert  into `detail_btkl_produk`(`kd_produk`,`kd_pekerjaan`,`jumlah_hari`) values ('IDP-0004','PKRJ-0002',10),('IDP-0004','PKRJ-0003',5),('IDP-0004','PKRJ-0004',2),('IDP-0005','PKRJ-0001',2),('IDP-0005','PKRJ-0002',1),('IDP-0005','PKRJ-0004',1),('IDP-0006','PKRJ-0001',3),('IDP-0006','PKRJ-0003',1),('IDP-0006','PKRJ-0004',1);

UNLOCK TABLES;

/*Table structure for table `detail_pembelian` */

DROP TABLE IF EXISTS `detail_pembelian`;

CREATE TABLE `detail_pembelian` (
  `kd_pembelian` varchar(20) NOT NULL,
  `kd_bb` varchar(20) NOT NULL,
  `kd_supplier` varchar(25) NOT NULL,
  `qty_beli` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `total_beli` int(11) NOT NULL,
  PRIMARY KEY (`kd_pembelian`,`kd_bb`,`kd_supplier`,`qty_beli`,`harga_beli`,`total_beli`),
  KEY `kd_bb` (`kd_bb`),
  KEY `kd_supplier` (`kd_supplier`),
  CONSTRAINT `detail_pembelian_ibfk_1` FOREIGN KEY (`kd_bb`) REFERENCES `bahan_baku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_pembelian_ibfk_2` FOREIGN KEY (`kd_supplier`) REFERENCES `supplier` (`kd_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_pembelian` */

LOCK TABLES `detail_pembelian` WRITE;

UNLOCK TABLES;

/*Table structure for table `detail_pengeluaran` */

DROP TABLE IF EXISTS `detail_pengeluaran`;

CREATE TABLE `detail_pengeluaran` (
  `kd_pengeluaran` varchar(30) NOT NULL,
  `kd_bop` varchar(20) NOT NULL,
  `kebutuhan` int(11) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_pengeluaran`,`kd_bop`),
  KEY `kd_bop` (`kd_bop`),
  CONSTRAINT `detail_pengeluaran_ibfk_1` FOREIGN KEY (`kd_pengeluaran`) REFERENCES `pengeluaran` (`kd_pengeluaran`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_pengeluaran_ibfk_2` FOREIGN KEY (`kd_bop`) REFERENCES `bop` (`kd_bop`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_pengeluaran` */

LOCK TABLES `detail_pengeluaran` WRITE;

UNLOCK TABLES;

/*Table structure for table `detail_pesanan` */

DROP TABLE IF EXISTS `detail_pesanan`;

CREATE TABLE `detail_pesanan` (
  `kd_pesanan` varchar(20) NOT NULL,
  `kd_produk` varchar(20) NOT NULL,
  `qty_pesanan` int(11) NOT NULL,
  `harga_prd` int(11) NOT NULL,
  `sub_pesanan` int(11) NOT NULL,
  PRIMARY KEY (`kd_pesanan`,`kd_produk`,`qty_pesanan`,`harga_prd`,`sub_pesanan`),
  KEY `kd_produk` (`kd_produk`),
  CONSTRAINT `detail_pesanan_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_pesanan_ibfk_2` FOREIGN KEY (`kd_pesanan`) REFERENCES `pesanan` (`kd_pesanan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_pesanan` */

LOCK TABLES `detail_pesanan` WRITE;

UNLOCK TABLES;

/*Table structure for table `detail_produksi` */

DROP TABLE IF EXISTS `detail_produksi`;

CREATE TABLE `detail_produksi` (
  `kd_produksi` varchar(20) NOT NULL,
  `kd_produk` varchar(20) NOT NULL,
  `qty_produk` int(11) DEFAULT NULL,
  `harga_produk` int(11) DEFAULT NULL,
  `subtotal_produksi` int(11) DEFAULT '0',
  PRIMARY KEY (`kd_produksi`,`kd_produk`),
  KEY `kd_produk` (`kd_produk`),
  CONSTRAINT `detail_produksi_ibfk_1` FOREIGN KEY (`kd_produksi`) REFERENCES `produksi` (`kd_produksi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detail_produksi_ibfk_2` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_produksi` */

LOCK TABLES `detail_produksi` WRITE;

UNLOCK TABLES;

/*Table structure for table `jurnal` */

DROP TABLE IF EXISTS `jurnal`;

CREATE TABLE `jurnal` (
  `id_jurnal` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_jurnal` date DEFAULT NULL,
  `kd_coa` int(11) DEFAULT NULL,
  `type` enum('Kredit','Debit') DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_jurnal`),
  KEY `kd_coa` (`kd_coa`),
  CONSTRAINT `jurnal_ibfk_1` FOREIGN KEY (`kd_coa`) REFERENCES `coa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

/*Data for the table `jurnal` */

LOCK TABLES `jurnal` WRITE;

UNLOCK TABLES;

/*Table structure for table `karyawan` */

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `id_karyawan` varchar(15) NOT NULL,
  `nm_karyawan` varchar(25) DEFAULT NULL,
  `id_pekerjaan` varchar(30) DEFAULT NULL,
  `tarif` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_karyawan`),
  KEY `id_pekerjaan` (`id_pekerjaan`),
  CONSTRAINT `karyawan_ibfk_1` FOREIGN KEY (`id_pekerjaan`) REFERENCES `pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `karyawan` */

LOCK TABLES `karyawan` WRITE;

insert  into `karyawan`(`id_karyawan`,`nm_karyawan`,`id_pekerjaan`,`tarif`) values ('K-0001','Dyas Nurhakim','PKRJ-0003',20000),('K-0002','Dyas NS','PKRJ-0001',70000),('K-0003','Tubagus','PKRJ-0001',80000),('K-0004','Ihya','PKRJ-0001',50000),('K-0005','Tebe','PKRJ-0002',33000),('K-0006','Musyaffa','PKRJ-0002',30000),('K-0007','Adi','PKRJ-0004',20000),('K-0008','Danny','PKRJ-0003',35000),('K-0009','Bicky','PKRJ-0002',75000),('K-0010','Bicky','PKRJ-0001',75000);

UNLOCK TABLES;

/*Table structure for table `kelompok_bop` */

DROP TABLE IF EXISTS `kelompok_bop`;

CREATE TABLE `kelompok_bop` (
  `kd_kelompok_bop` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kelompok_bop` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kd_kelompok_bop`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `kelompok_bop` */

LOCK TABLES `kelompok_bop` WRITE;

insert  into `kelompok_bop`(`kd_kelompok_bop`,`nm_kelompok_bop`) values (1,'Utilitas'),(2,'Perawatan Mesin');

UNLOCK TABLES;

/*Table structure for table `pekerjaan` */

DROP TABLE IF EXISTS `pekerjaan`;

CREATE TABLE `pekerjaan` (
  `id` varchar(30) NOT NULL,
  `nama_pekerjaan` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pekerjaan` */

LOCK TABLES `pekerjaan` WRITE;

insert  into `pekerjaan`(`id`,`nama_pekerjaan`) values ('PKRJ-0001','Membubut'),('PKRJ-0002','Press'),('PKRJ-0003','Borpriss'),('PKRJ-0004','Finishing');

UNLOCK TABLES;

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `kd_pembelian` varchar(20) NOT NULL,
  `tgl_beli` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_pembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pembelian` */

LOCK TABLES `pembelian` WRITE;

UNLOCK TABLES;

/*Table structure for table `pengeluaran` */

DROP TABLE IF EXISTS `pengeluaran`;

CREATE TABLE `pengeluaran` (
  `kd_pengeluaran` varchar(30) NOT NULL,
  `tgl_pengeluaran` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keterangan` text,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengeluaran` */

LOCK TABLES `pengeluaran` WRITE;

UNLOCK TABLES;

/*Table structure for table `pesanan` */

DROP TABLE IF EXISTS `pesanan`;

CREATE TABLE `pesanan` (
  `kd_pesanan` varchar(20) NOT NULL,
  `kd_pelanggan` varchar(25) DEFAULT NULL,
  `tgl_pesan` date DEFAULT NULL,
  `total` int(11) DEFAULT '0',
  `status_pesanan` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`kd_pesanan`),
  KEY `kd_pelanggan` (`kd_pelanggan`),
  CONSTRAINT `pesanan_ibfk_1` FOREIGN KEY (`kd_pelanggan`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pesanan` */

LOCK TABLES `pesanan` WRITE;

UNLOCK TABLES;

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `kd_produk` varchar(30) NOT NULL,
  `nm_produk` varchar(30) NOT NULL,
  `harga` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

LOCK TABLES `produk` WRITE;

insert  into `produk`(`kd_produk`,`nm_produk`,`harga`) values ('IDP-0004','Rubber Stripe','2500000'),('IDP-0005','Pillow Block Stainless','500000'),('IDP-0006','Guide Roll Teflon','2500000');

UNLOCK TABLES;

/*Table structure for table `produksi` */

DROP TABLE IF EXISTS `produksi`;

CREATE TABLE `produksi` (
  `kd_produksi` varchar(20) NOT NULL,
  `kd_pesanan` varchar(50) NOT NULL,
  `tgl_produksi` date DEFAULT NULL,
  `total` int(11) DEFAULT '0',
  PRIMARY KEY (`kd_produksi`,`kd_pesanan`),
  KEY `kd_pesanan` (`kd_pesanan`),
  CONSTRAINT `produksi_ibfk_1` FOREIGN KEY (`kd_pesanan`) REFERENCES `pesanan` (`kd_pesanan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `produksi` */

LOCK TABLES `produksi` WRITE;

UNLOCK TABLES;

/*Table structure for table `supplier` */

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `kd_supplier` varchar(20) NOT NULL,
  `nm_supplier` varchar(20) DEFAULT NULL,
  `alamat_supplier` varchar(100) DEFAULT NULL,
  `email_supplier` varchar(30) DEFAULT NULL,
  `tlp_supplier` varbinary(15) DEFAULT NULL,
  PRIMARY KEY (`kd_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `supplier` */

LOCK TABLES `supplier` WRITE;

insert  into `supplier`(`kd_supplier`,`nm_supplier`,`alamat_supplier`,`email_supplier`,`tlp_supplier`) values ('S-0001','PT.Somad','Bandung                  \r\n                ','somad@gmail.com','0987654321'),('S-0002','SINAR JAYA TEKNIK','Jl. Batubaru Tegalrejo Ceper,Klaten             \r\n                ','-','0817447555'),('S-0003','RAKAN PRATAMA MANDIR','Jl. Arjuna No.52 Blok J 22 Bandung','-','085100267949');

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(2) NOT NULL,
  `status` int(11) NOT NULL,
  `kd_karyawan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`username`),
  KEY `level` (`level`),
  KEY `kd_karyawan` (`kd_karyawan`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`level`) REFERENCES `user_level` (`id_level`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`kd_karyawan`) REFERENCES `karyawan` (`id_karyawan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`username`,`password`,`level`,`status`,`kd_karyawan`) values ('adi','ee11cbb19052e40b07aac0ca060c23ee',2,1,'K-0007'),('bicky','21232f297a57a5a743894a0e4a801fc3',1,1,'K-0010'),('danny','ee11cbb19052e40b07aac0ca060c23ee',2,1,'K-0008'),('dyasns','ee11cbb19052e40b07aac0ca060c23ee',1,1,'K-0002'),('dyasnurhakim','21232f297a57a5a743894a0e4a801fc3',1,1,'K-0001'),('ihya','ee11cbb19052e40b07aac0ca060c23ee',2,1,'K-0004'),('musyaffa','ee11cbb19052e40b07aac0ca060c23ee',2,1,'K-0006'),('tebe','ee11cbb19052e40b07aac0ca060c23ee',2,0,'K-0005'),('tubagus','ee11cbb19052e40b07aac0ca060c23ee',2,1,'K-0003');

UNLOCK TABLES;

/*Table structure for table `user_level` */

DROP TABLE IF EXISTS `user_level`;

CREATE TABLE `user_level` (
  `id_level` int(2) NOT NULL AUTO_INCREMENT,
  `level` varchar(100) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `user_level` */

LOCK TABLES `user_level` WRITE;

insert  into `user_level`(`id_level`,`level`) values (1,'Admin'),(2,'User');

UNLOCK TABLES;

/* Trigger structure for table `detail_bbb` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `update_detail_produksi_bahanbaku` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `update_detail_produksi_bahanbaku` AFTER INSERT ON `detail_bbb` FOR EACH ROW BEGIN
	SET @total = (SELECT subtotal_produksi FROM detail_produksi WHERE kd_produksi = new.kd_produksi AND kd_produk = new.kd_produk);
	UPDATE detail_produksi SET subtotal_produksi = @total + (new.sub_bbb) WHERE kd_produksi = new.kd_produksi AND kd_produk = new.kd_produk;
	SET @total = (SELECT total FROM produksi WHERE kd_produksi = new.kd_produksi);
	UPDATE produksi SET total = @total + (new.sub_bbb) WHERE kd_produksi = new.kd_produksi;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_bop` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `update_detail_produksi_bop` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `update_detail_produksi_bop` AFTER INSERT ON `detail_bop` FOR EACH ROW BEGIN
	SET @totalproduksi = (SELECT subtotal_produksi FROM detail_produksi WHERE kd_produksi = new.kd_produksi AND kd_produk = new.kd_produk);
	UPDATE detail_produksi SET subtotal_produksi = @totalproduksi + (new.subtotal_bop) WHERE kd_produksi = new.kd_produksi AND kd_produk = new.kd_produk;
	SET @total = (SELECT total FROM produksi WHERE kd_produksi = new.kd_produksi);
	UPDATE produksi SET total = @total + (new.subtotal_bop) WHERE kd_produksi = new.kd_produksi;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_btkl` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `update_detail_produksi_btkl` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `update_detail_produksi_btkl` AFTER INSERT ON `detail_btkl` FOR EACH ROW BEGIN
	SET @totalproduksi = (SELECT subtotal_produksi FROM detail_produksi WHERE kd_produksi = new.kd_produksi AND kd_produk = new.kd_produk);
	UPDATE detail_produksi SET subtotal_produksi = @totalproduksi + (new.subtotal) WHERE kd_produksi = new.kd_produksi AND kd_produk = new.kd_produk;
	SET @total = (SELECT total FROM produksi WHERE kd_produksi = new.kd_produksi);
	UPDATE produksi SET total = @total + (new.subtotal) WHERE kd_produksi = new.kd_produksi;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_pembelian` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `Update_BBAKU` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `Update_BBAKU` AFTER INSERT ON `detail_pembelian` FOR EACH ROW BEGIN
	UPDATE bahan_baku SET harga = new.harga_beli WHERE id = new.kd_bb;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_pesanan` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `update_detail_pesanan` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `update_detail_pesanan` AFTER INSERT ON `detail_pesanan` FOR EACH ROW BEGIN
	SEt @total = (SELECT total FROm pesanan WHERE kd_pesanan = new.kd_pesanan);
	Update pesanan set total = @total + (new.qty_pesanan * new.harga_prd) WHERE kd_pesanan = new.kd_pesanan;
    END */$$


DELIMITER ;

/* Trigger structure for table `produksi` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `Update_Pesanan` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `Update_Pesanan` AFTER INSERT ON `produksi` FOR EACH ROW BEGIN
	Update pesanan set status_pesanan=true where pesanan.`kd_pesanan` = new.kd_pesanan;
    END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
