<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_trx_bbaku extends CI_Model{

	public function __construct(){
	
		parent::__construct();

		$this->table_bbaku	= 'bahan_baku';
		$this->table_supplier = 'supplier';
		$this->table_pembelian = 'pembelian';
		$this->table_detail_pembelian = 'detail_pembelian';
	
	}

	public function get_allPembelian(){
        $query=$this->db->get($this->table_pembelian);

        return $query->result();
    }
 
 
    public function get_onePembelian($where){
        
        $query=$this->db->get_where($this->table_pembelian,$where);
 
        return $query->row();
    }

    public function get_DetailPembelian($where){
        
        $query=$this->db->join($this->table_supplier,$this->table_supplier.'.kd_supplier = '.$this->table_detail_pembelian.'.kd_supplier')->join($this->table_bbaku,$this->table_bbaku.'.id = '.$this->table_detail_pembelian.'.kd_bb')->get_where($this->table_detail_pembelian,$where);
 
        return $query->result();
    }

	public function get_bbaku(){
		return $this->db->get($this->table_bbaku)->result();
	
	}

	public function get_supplier(){
		return $this->db->get($this->table_supplier)->result();
	
	}

	public function getOne_bbaku($id){
		return $this->db->get_where($this->table_bbaku,'id = "'. $id.'"')->result();
	}

	public function getOne_supplier($id){
		return $this->db->get_where($this->table_supplier,'kd_supplier = "'. $id.'"')->result();
	}

	public function add_pembelian($data){
		return $this->db->insert($this->table_pembelian,$data);
	}

	public function add_detail_pembelian($data){
		return $this->db->insert($this->table_detail_pembelian,$data);
	}	

	public function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_pembelian,4)) AS idmax FROM $this->table_pembelian ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "PBB-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
}



