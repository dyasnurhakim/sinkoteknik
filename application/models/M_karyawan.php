<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_karyawan extends CI_Model
{
 
	var $table = 'user';
	var $table_karyawan = 'karyawan';
 
 
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
 
 
	public function get_all_user(){
		$query=$this->db->join($this->table_karyawan, $this->table_karyawan.".id_karyawan = ".$this->table.".kd_karyawan")->get($this->table);
		return $query->result();
	}
 
 
	public function get_by_id($where) {
		$query = $this->db->join($this->table_karyawan, $this->table_karyawan.".id_karyawan = ".$this->table.".kd_karyawan")->get_where($this->table,$where);
 
		return $query->row();
	}

	public function get_by($where) {
		$query = $this->db->join($this->table_karyawan, $this->table_karyawan.".id_karyawan = ".$this->table.".kd_karyawan")->get_where($this->table,$where);
 
		return $query->result();
	}
 
	public function add($karyawan, $user){
		$this->db->insert($this->table_karyawan, $karyawan);
		$this->db->insert($this->table, $user);
		return $this->db->insert_id();
	}
 
	public function update($where, $karyawan, $user)
	{
		$this->db->update($this->table, $user, 'kd_karyawan = "'.$where.'"');
		return $this->db->update($this->table_karyawan, $karyawan, 'id_karyawan = "'.$where.'"');
	}
 
	public function delete_by_id($id)
	{
		$this->db->where('kd_karyawan', $id);
		$this->db->delete($this->table);
		$this->db->where('id_karyawan', $id);
		$this->db->delete($this->table_karyawan);
	}

	function cek_username($username){
		return $this->db->query("select username from user where username='$username'")->num_rows();
	}
 	
 	function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(id_karyawan,4)) AS idmax FROM $this->table_karyawan ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "K-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
 
}