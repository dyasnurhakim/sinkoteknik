<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_oh extends CI_Model
{
 
    var $table = 'bop';
    var $table_kelompok_bop = 'kelompok_bop';
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
 
    public function get_all_oh()
    {
    $this->db->from('bop');
    $query=$this->db->get();
    return $query->result();
    }
 
 
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('kd_bop',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
    
    public function get_one_kelompok_bop($id){
        return $this->db->get_where($this->table_kelompok_bop,'kd_kelompok_bop = '.$id)->result();
    }
    
    public function oh_add($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function oh_update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
        $this->db->where('kd_bop', $id);
        $this->db->delete($this->table);
    }
    function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_bop,4)) AS idmax FROM bop ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "AKTV-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
      function data_kelompok_bop(){
        $query=$this->db->query("SELECT * FROM kelompok_bop");
        return $query->result();
    }
    
    function data_coa(){
        $query=$this->db->query("SELECT * FROM coa");
        return $query->result();
    }
 
}