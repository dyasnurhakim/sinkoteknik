<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_trx_produksi extends CI_Model{

	public function __construct(){
	
		parent::__construct();

		$this->table_pesanan= 'pesanan';
		$this->table_bop= 'bop';
		$this->table_pekerjaan = 'pekerjaan';
		$this->table_bbaku = 'bahan_baku';
		$this->table_produksi = 'produksi';
		$this->table_produk = 'produk';
		$this->table_customer = 'customer';
		$this->table_detail_bbb = 'detail_bbb';
		$this->table_detail_btkl = 'detail_btkl';
		$this->table_detail_bop = 'detail_bop';
		$this->table_detail_produksi = 'detail_produksi';
		$this->table_detail_bbb_produk = 'detail_bbb_produk';
		$this->table_detail_btkl_produk = 'detail_btkl_produk';
		$this->table_detail_bop_produk = 'detail_bop_produk';
		$this->table_detail_pesanan = 'detail_pesanan';
	
	}

	public function get_pesanan(){
		return $this->db->join($this->table_customer,$this->table_customer.".id = ".$this->table_pesanan.".kd_pelanggan")->get_where($this->table_pesanan,$this->table_pesanan.'.status_pesanan = 0')->result();
	
	}

	public function get_pekerjaan($id){
		return $this->db->join($this->table_detail_btkl_produk,$this->table_detail_btkl_produk.'.kd_pekerjaan = '.$this->table_pekerjaan.'.id')->get_where($this->table_pekerjaan,$this->table_detail_btkl_produk.'.kd_produk = "'.$id.'"')->result();
	
	}

	public function get_bop($id){
		return $this->db->join($this->table_detail_bop_produk,$this->table_detail_bop_produk.'.kd_bop = '.$this->table_bop.'.kd_bop')->get_where($this->table_bop,$this->table_detail_bop_produk.'.kd_produk = "'.$id.'"')->result();
	
	}

	public function get_bbaku($id){
		return $this->db->join($this->table_detail_bbb_produk,$this->table_detail_bbb_produk.'.kd_bbaku = '.$this->table_bbaku.'.id')->get_where($this->table_bbaku,$this->table_detail_bbb_produk.'.kd_produk = "'.$id.'"')->result();
	
	}

	public function getOne_detail_pesanan($pesanan,$produk){
		return $this->db->join($this->table_produk,$this->table_produk.'.kd_produk ='.$this->table_detail_pesanan.'.kd_produk')->get_where($this->table_detail_pesanan,'kd_pesanan = "'. $pesanan.'"'.' AND '.$this->table_detail_pesanan.'.kd_produk = "'.$produk.'"')->result();
	}

	public function get_detail_pesanan($where){
		return $this->db->join($this->table_produk,$this->table_produk.'.kd_produk ='.$this->table_detail_pesanan.'.kd_produk')->get_where($this->table_detail_pesanan,$where)->result();
	}

	public function getOne_Pesanan($where){
		return $this->db->get_where($this->table_pesanan,$where)->result();
	}

	public function getProduk_customer($id){
		return $this->db->join($this->table_produk,$this->table_produk.".kd_produk = ".$this->table_detail_pesanan.'.kd_produk')->get_where($this->table_detail_pesanan,$this->table_detail_pesanan.'.kd_pesanan = "'. $id.'"')->result();
	}

	public function getOne_customer_join_pesanan($id){
		return $this->db->join($this->table_customer,$this->table_customer.".id = ".$this->table_pesanan.".kd_pelanggan")->get_where($this->table_pesanan,$this->table_pesanan.'.kd_pesanan = "'. $id.'"')->result();
	}
	
	public function getOne_customer($id){
		return $this->db->get_where($this->table_customer,'id = "'. $id.'"')->result();
	}

	public function add_produksi($data){
		return $this->db->insert($this->table_produksi,$data);
	}

	public function add_detail_produksi($data){
		return $this->db->insert($this->table_detail_produksi,$data);
	}

	public function add_detail_bbb($data){
		return $this->db->insert($this->table_detail_bbb,$data);
	}	

	public function add_detail_btkl($data){
		return $this->db->insert($this->table_detail_btkl,$data);
	}	

	public function add_detail_bop($data){
		return $this->db->insert($this->table_detail_bop,$data);
	}	

	public function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_produksi,4)) AS idmax FROM $this->table_produksi ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "P-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
}