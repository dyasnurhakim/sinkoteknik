<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_barang extends CI_Model{

	private $primary_key = 'id';
	private $table_name	= 'produk';

	public function __construct()
	{
	
		parent::__construct();
	
	}

	public function get() 
	{
	  	
	  	$this->db->select('id,nama_produk');

		return $this->db->get($this->table_name)->result();
	
	}

	public function get_by_id($id)
	{
	  
	  	$this->db->where($this->primary_key,$id); 
	  
	  	return $this->db->get($this->table_name)->row();
	
	}
	function add_transaksi($data){
			$this->db->insert('transaksi',$data);
	}
	public function delete_by_id($id)
	{
		$this->db->where('kd_transaksi', $id);
		$this->db->delete($this->table);
	}
	function process(){
		$invoice = array(
			 'date'		=> date('Y-m-d H:i:s'),
			 'due_date'		=> date('Y-m-d H:i:s', mktime(date('H'),date('i'),date('s'),date('m'),date('d') +1,date('Y'))),
			 );
		$this->db->insert('invoice',$invoice);
		$invoice_id = $this->db->insert_id();

		foreach ($this->cart->contents() as $item){
			$data = array(
				'invoice_id'	=> $invoice_id,
				'produk_id'		=> $item['id'],
				'nama_produk'	=> $item['name'],
				'qty'			=> $item['qty'],
				'price'			=> $item['price'],
				'customer_id'	=> $item['customer_id'],
				);
			$this->db->insert('transaksis',$data);
		}
		return TRUE;
	}
	function data_pekerjaan(){
        $query=$this->db->query("SELECT * FROM pekerjaan ORDER BY id ASC");
        return $query->result();
    }
    function data_bahanbaku(){
        $query=$this->db->query("SELECT * FROM bahan_baku ORDER BY id ASC");
        return $query->result();
    }
    function data_oh(){
        $query=$this->db->query("SELECT * FROM biaya_oh ORDER BY id ASC");
        return $query->result();
    }   
 
    public function add_tes($data)
	{
		$this->db->insert('tes',$data);
	}
	function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(id,4)) AS idmax FROM transaksi ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "TRX-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }

}



