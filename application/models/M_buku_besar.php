<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_buku_besar extends CI_Model
{
 
    public function __construct() {
        parent::__construct();
        $this->bop  = 'bop';
        $this->coa = 'coa';
        $this->jurnal = 'jurnal';
    }
    
    public function getAll_Jurnal($bulan, $tahun, $kd_coa){

        return $this->db->join($this->coa, $this->coa.".id = ".$this->jurnal.".kd_coa")->get_where($this->jurnal,'MONTH(tgl_jurnal) = "'.$bulan.'" AND YEAR(tgl_jurnal) = "'.$tahun.'" AND '.$this->coa.'.id = "'.$kd_coa.'"')->result();
    }

    public function getAllCoa(){
        return $this->db->get($this->coa)->result();
    }
}