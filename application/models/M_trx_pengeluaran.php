<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_trx_pengeluaran extends CI_Model{

	public function __construct(){
	
		parent::__construct();

		$this->table_bop	= 'bop';
		$this->table_customer = 'customer';
		$this->table_pengeluaran= 'pengeluaran';
		$this->table_detail_pengeluaran = 'detail_pengeluaran';
	
	}

	public function get_bop(){
		return $this->db->get($this->table_bop)->result();
	
	}

	public function getOne_bop($id){
		return $this->db->get_where($this->table_bop,'kd_bop = "'. $id.'"')->result();
	}

	public function add_pengeluaran($data){
		return $this->db->insert($this->table_pengeluaran,$data);
	}

	public function add_detail_pengeluaran($data){
		return $this->db->insert($this->table_detail_pengeluaran,$data);
	}	

	public function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_pengeluaran,4)) AS idmax FROM $this->table_pengeluaran ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "PNRL-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
}



