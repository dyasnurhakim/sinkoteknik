<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_produk extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->table_bop= 'bop';
        $this->table_pekerjaan = 'pekerjaan';
        $this->table_bbaku = 'bahan_baku';
        $this->table_produk = 'produk';
        $this->table_detail_bbb_produk = 'detail_bbb_produk';
        $this->table_detail_btkl_produk = 'detail_btkl_produk';
        $this->table_detail_bop_produk = 'detail_bop_produk';
    }
 
 
    public function get_all_produk() {
        $query=$this->db->get($this->table_produk);

        return $query->result();
    }
 
    public function get_pekerjaan(){
        return $this->db->get($this->table_pekerjaan)->result();
    
    }

    public function get_bop(){
        return $this->db->get($this->table_bop)->result();
    
    }

    public function get_bbaku(){
        return $this->db->get($this->table_bbaku)->result();
    
    }

    public function get_pekerjaan_produk($where){
        return $this->db->get_where($this->table_detail_btkl_produk,$where)->result();
    
    }

    public function get_bop_produk($where){
        return $this->db->get_where($this->table_detail_bop_produk,$where)->result();
    
    }

    public function get_bbaku_produk($where){
        return $this->db->get_where($this->table_detail_bbb_produk,$where)->result();
    
    }

    public function getOne_produk($id){
        return $this->db->get_where($this->table_produk,'kd_produk = "'. $id.'"')->result();
    }

    public function getOne_BOP($id){
        return $this->db->get_where($this->table_bop,'kd_bop = "'. $id.'"')->result();
    }

    public function getOne_pekerjaan($id){
        return $this->db->get_where($this->table_pekerjaan,'id = "'. $id.'"')->result();
    }

    public function getOne_bbaku($id){
        return $this->db->get_where($this->table_bbaku,'id = "'. $id.'"')->result();
    }

    public function get_by_id($id){
        $query = $this->db->get_where($this->table_produk,'kd_produk = "'.$id.'"');
 
        return $query->result();
    }
 
    public function add_produk($data){
        return $this->db->insert($this->table_produk, $data);
    }
    
    public function add_bbaku($data){
        return $this->db->insert($this->table_detail_bbb_produk,$data);
    }

    public function add_pekerjaan($data){
        return $this->db->insert($this->table_detail_btkl_produk,$data);
    }

    public function add_bop($data){
        return $this->db->insert($this->table_detail_bop_produk,$data);
    }

    public function update_produk($where, $data) {
        return $this->db->update($this->table_produk, $data, $where);
    }

    public function update_bop($where, $data) {
        return $this->db->update($this->table_detail_bop_produk, $data, $where);
    }

    public function update_pekerjaan($where, $data) {
        return $this->db->update($this->table_detail_btkl_produk, $data, $where);
    }

    public function update_bbaku($where, $data) {
        return $this->db->update($this->table_detail_bbb_produk, $data, $where);
    }
 
    public function delete_by_id($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->table_produk);
    }

    public function delete_bbaku($where) {
        return $this->db->delete($this->table_detail_bbb_produk,$where);
    }

    public function delete_pekerjaan($where) {
        return $this->db->delete($this->table_detail_btkl_produk,$where);
    }

    public function delete_bop($where) {
        return $this->db->delete($this->table_detail_bop_produk, $where);
    }

    function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_produk,4)) AS idmax FROM produk ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "IDP-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
 
 
}