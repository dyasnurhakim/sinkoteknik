<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_trx_pemesanan extends CI_Model{

	public function __construct(){
	
		parent::__construct();

		$this->table_produk	= 'produk';
		$this->table_customer = 'customer';
		$this->table_pesanan = 'pesanan';
		$this->table_detail_pesanan = 'detail_pesanan';
	
	}

	 public function get_allPesanan(){
        $query=$this->db->join($this->table_customer,$this->table_customer.'.id = '.$this->table_pesanan.'.kd_pelanggan')->get($this->table_pesanan);

        return $query->result();
    }
 
 
    public function get_onePesanan($where){
        
        $query=$this->db->join($this->table_customer,$this->table_customer.'.id = '.$this->table_pesanan.'.kd_pelanggan')->get_where($this->table_pesanan,$where);
 
        return $query->row();
    }
	
    public function get_DetailPesanan($where){
    	 $query=$this->db->join($this->table_produk,$this->table_produk.'.kd_produk = '.$this->table_detail_pesanan.'.kd_produk')->get_where($this->table_detail_pesanan,$where);
    	 return $query->result();
    }

	public function get_produk(){
		return $this->db->get($this->table_produk)->result();
	
	}

	public function get_customer(){
		return $this->db->get($this->table_customer)->result();
	
	}

	public function getOne_produk($id){
		return $this->db->get_where($this->table_produk,'kd_produk = "'. $id.'"')->result();
	}

	public function getOne_customer($id){
		return $this->db->get_where($this->table_customer,'id = "'. $id.'"')->result();
	}

	public function add_pesanan($data){
		return $this->db->insert($this->table_pesanan,$data);
	}

	public function add_detail_pesanan($data){
		return $this->db->insert($this->table_detail_pesanan,$data);
	}	

	public function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_pesanan,4)) AS idmax FROM $this->table_pesanan ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "TRX-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
}



