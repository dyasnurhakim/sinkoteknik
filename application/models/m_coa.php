<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_coa extends CI_Model
{
 
    var $table = 'coa';
    var $jurnal = 'jurnal';
 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
 
    public function get_all_coa()
    {
    $this->db->from('coa');
    $query=$this->db->get();
    return $query->result();
    }
 
 
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function coa_add($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function coa_update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
    
    public function save_coa($data){;
        //$this->cek_saldo();
        $saldo = $this->get_saldo();
        if($data['type'] == "Kredit" && $data['kd_coa'] == 2){
            $data['saldo'] = $saldo[0]->saldo - $data['nominal'];
        } else if($data['type'] == "Debit" && $data['kd_coa'] == 2){
            $data['saldo'] = $saldo[0]->saldo + $data['nominal'];
        } else {
            $data['saldo'] = $saldo[0]->saldo;   
        }
        return $this->db->insert($this->jurnal, $data);
    }

    public function save_kas($data){
        $data['kd_coa'] = '2';
        $data['type'] = $data['type'];
        $saldo = $this->get_saldo();
        $data['saldo'] = $saldo[0]->saldo - $data['nominal'];
         $this->db->insert($this->jurnal, $data);
        return $this->db->insert_id();
    }

    public function cek_saldo(){
        $this->db->order_by('tgl_jurnal', "desc");
        $this->db->limit(1);
        $temp = $this->db->get_where($this->jurnal, array('kd_coa' => '6'))->result();
        $tgl = explode('-', $temp[0]->tgl_jurnal);
        if($tgl[1] != date('m')){
            $saldo = $this->get_saldo();
            $data = array(
                        'tgl_jurnal'    => date("Y-m-d"),
                        'kd_coa'        => '6',
                        'type'          => 'Kredit',
                        'nominal'       => $saldo[0]->saldo,
                        'saldo'         => $saldo[0]->saldo
                );
            $this->db->insert($this->jurnal, $data);
            $this->db->insert_id();
        }

        return true;

    }

    public function get_saldo(){
        $this->db->select('saldo');
        $this->db->order_by('id_jurnal', "desc");
        $this->db->limit(1);
        return $this->db->get($this->jurnal)->result();
    }
 
}