<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_lap_labarugi extends CI_Model
{
 
    public function __construct() {
        parent::__construct();
        $this->produksi = 'produksi';
        $this->bop = 'bop';
        $this->pesanan = 'pesanan';
        $this->pengeluaran = 'pengeluaran';
        $this->pembelian = 'pembelian';
        $this->detail_pengeluaran = 'detail_pengeluaran';
    }
    
    public function get_totalProduksi_by($month, $year){
        $this->db->select_sum('total');
        $query = $this->db->get_where($this->produksi,'MONTH(tgl_produksi) = "'.$month.'" AND YEAR(tgl_produksi) = "'.$year.'"')->result();
        return $query[0]->total;
    }

    public function get_totalPesanan_by($month, $year){
        $this->db->select_sum('total');
        $query = $this->db->get_where($this->pesanan,'MONTH(tgl_pesan) = "'.$month.'" AND YEAR(tgl_pesan) = "'.$year.'"')->result();
        return $query[0]->total;
    }

    public function get_totalPembelian_by($month, $year){
        $this->db->select_sum('total');
        $query = $this->db->get_where($this->pembelian,'MONTH(tgl_beli) = "'.$month.'" AND YEAR(tgl_beli) = "'.$year.'"')->result();
        return $query[0]->total;
    }

    public function get_AllPengeluaran_by($month, $year){
        return $this->db->get_where($this->pengeluaran,'MONTH(tgl_pengeluaran) = "'.$month.'" AND YEAR(tgl_pengeluaran) = "'.$year.'"')->result();
    }

    public function get_DetailPengeluaran($kode){
        return $this->db->get_where($this->detail_pengeluaran,'kd_pengeluaran = "'.$kode.'"')->result();
    }

}