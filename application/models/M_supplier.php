<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_supplier extends CI_Model
{
 
    public function __construct() {
        parent::__construct();
        $this->table = 'supplier';
    }
 
 
    public function getAll(){
        $this->db->from($this->table);
        $query=$this->db->get();

        return $query->result();
    }
 
 
    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('kd_supplier',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function add($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function bb_update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id){
        $this->db->where('kd_supplier', $id);
        $this->db->delete($this->table);
    }
    function getkodeunik() { 
        $q = $this->db->query("SELECT MAX(RIGHT(kd_supplier,4)) AS idmax FROM $this->table ");
        $kd = ""; //kode awal
        if($q->num_rows()>0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "S-"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kar.$kd;
   }
 
 
}