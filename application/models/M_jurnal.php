<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_jurnal extends CI_Model
{
 
    public function __construct() {
        parent::__construct();
        $this->bop  = 'bop';
        $this->coa = 'coa';
        $this->jurnal = 'jurnal';
    }
    
    public function getAll_Jurnal($awal, $akhir){

        return $this->db->join($this->coa, $this->coa.".id = ".$this->jurnal.".kd_coa")->order_by($this->jurnal.'.id_jurnal')->get_where($this->jurnal,'tgl_jurnal >= "'.$awal.'" AND tgl_jurnal <= "'.$akhir.'"')->result();
    }

}