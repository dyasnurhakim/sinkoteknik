<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class M_lap_produksi extends CI_Model
{
 
    public function __construct() {
        parent::__construct();
        $this->produksi = 'produksi';
        $this->detail_produksi  = 'detail_produksi';
        $this->detail_bbb  = 'detail_bbb';
        $this->detail_bop = 'detail_bop';
        $this->detail_btkl = 'detail_btkl';
    }
    
    public function get_AllProduksi_byDate($awal, $akhir){
        return $this->db->get_where($this->produksi,'tgl_produksi >= "'.$awal.'" AND tgl_produksi <= "'.$akhir.'"')->result();
    }

    public function get_OneProduksi($where){
        return $this->db->get_where($this->produksi,$where)->result();
    }

    public function get_totalbbb($where){
        $this->db->select_sum('sub_bbb');
        $query =  $this->db->get_where($this->detail_bbb,$where)->result();
        return $query[0]->sub_bbb;
    }
    
    public function get_totalbtkl($where){
        $this->db->select_sum('subtotal');
        $query = $this->db->get_where($this->detail_btkl,$where)->result();
        return $query[0]->subtotal;
    }

    public function get_totalbop($where){
        $this->db->select_sum('nominal_bop');
        $query = $this->db->get_where($this->detail_bop,$where)->result();
        return $query[0]->nominal_bop;
    }
}