<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Coa extends Main {
	function __construct(){
		parent::__construct();
	}

	function index(){
			$data['active_menu'] = 'coa';
            $data['coa']=$this->m_coa->get_all_coa();
            $this->global_data = $data;
	        $this->tampilan('admin/coa/coa');
	}
	 
	public function coa_add(){
			$data = array(
					'kd_akun' => $this->input->post('kd_akun'),
					'nama_akun' => $this->input->post('nama_akun')
				);
			$insert = $this->m_coa->coa_add($data);
			echo json_encode(array("status" => TRUE));
		}
	public function ajax_edit($id){
			$data = $this->m_coa->get_by_id($id);
			echo json_encode($data);
		}
	
	public function coa_update(){
			$data = array(
					'kd_akun' => $this->input->post('kd_akun'),
					'nama_akun' => $this->input->post('nama_akun'),
				);
			$this->m_coa->coa_update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
		}	
	public function coa_delete($id){
			$this->m_coa->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}

}
