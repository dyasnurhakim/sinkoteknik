<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Customer extends Main {
	function __construct(){
		parent::__construct();
	}
	public function index(){
			$data['active_menu'] = 'customer';
            $data['customer']=$this->m_customer->get_all_customer();
            $data['kodeunik'] = $this->m_customer->getkodeunik();

			$this->global_data = $data;
	        $this->tampilan('admin/customer/customer');
         
		
	}
	public function customer_add()
		{
			$data = array(
					'id' => $this->input->post('id'),
					'nama_pelanggan' => $this->input->post('nama_pelanggan'),
					'alamat' => $this->input->post('alamat'),
					'email' => $this->input->post('email'),
					'tlp' => $this->input->post('tlp'),
				);
			$insert = $this->m_customer->customer_add($data);
			echo json_encode(array("status" => TRUE));
		}
	public function ajax_edit($id)
		{
			$data = $this->m_customer->get_by_id($id);
 
 
 
			echo json_encode($data);
		}
	
	public function customer_update()
		{
			$data = array(
					'id' => $this->input->post('id'),
					'nama_pelanggan' => $this->input->post('nama_pelanggan'),
					'alamat' => $this->input->post('alamat'),
					'email' => $this->input->post('email'),
					'tlp' => $this->input->post('tlp'),
				);
			$this->m_customer->customer_update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
		}	

	public function customer_delete($id)
		{
			$this->m_customer->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}
}	