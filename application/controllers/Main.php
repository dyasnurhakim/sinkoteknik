<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->getSession();
		if($this->sessions['user']){
			$user = $this->sessions['user'];
	        if(!$user['isLogin']){
	            redirect('auth');
	        }	
		} else {
			redirect('auth');
		}
        
		$this->global_data = array();
	}

	protected function saveSession(){
		$this->session->set_userdata('session',$this->sessions);
	}

	protected function getSession(){
		$this->sessions = $this->session->userdata('session');
	}

	public function save_coa( $kd_coa, $type, $nominal ){
		$data = array(
					'tgl_jurnal'	=> date('Y-m-d'),
					'kd_coa'		=> $kd_coa,
					'type'			=> $type,
					'nominal'		=> $nominal
				);
		return $this->m_coa->save_coa($data);
	}

	protected function tampilan($view_name){
		$this->load->view('header',$this->global_data);
        $this->load->view('side_menu',$this->global_data);
        $this->load->view($view_name,$this->global_data);
        $this->load->view('footer',$this->global_data);
	}

	protected function outputJson($response=array(),$status=200){
		$this->output
		->set_status_header($status)
		->set_content_type('application/json', 'utf-8')
		->set_output(json_encode($response, JSON_PRETTY_PRINT))
		->_display();
		exit();
	}
}

?>