<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Karyawan extends Main {
	function __construct(){
		parent::__construct();
	}

	public function index(){
        	$data['active_menu'] = 'karyawan';
            $data['users']=$this->m_karyawan->get_all_user();
            $data['pekerjaan'] = $this->m_pekerjaan->get_all_pekerjaan();
			$this->global_data = $data;
	        $this->tampilan('admin/karyawan/karyawan');	
	}

	function registration(){
		$this->form_validation->set_rules('username','Username','required|trim|xss_clean'); 
		$this->form_validation->set_rules('password','Password','required|trim|xss_clean');
		$this->form_validation->set_rules('fullname','Fullname','required|trim|xss_clean');
		$this->form_validation->set_rules('level','Level','required|trim|xss_clean');	
		$this->form_validation->set_rules('status','Status','required|trim|xss_clean');
		$this->form_validation->set_rules('pekerjaan','Pekerjaan','required|trim|xss_clean');

		if($this->form_validation->run()==FALSE){
        	$data['active_menu'] = 'karyawan';
            
            $data['users']=$this->m_karyawan->get_all_user();
		
			$this->global_data = $data;
	        $this->tampilan('admin/karyawan');
		}else{		
			$username 	= $this->input->post('username');
			$password 	= md5($this->input->post('password'));
			$nama 		= $this->input->post('fullname');
			$email 		= $this->input->post('email');
			$level 		= $this->input->post('level');
			$status 	= $this->input->post('status');
			$pekerjaan 	= $this->input->post('pekerjaan');
			$tarif 		= $this->input->post('tarif');

			$count = $this->m_karyawan->cek_username($username);
			if($count==0){
				$id_karyawan = $this->m_karyawan->getkodeunik();
				$user = array(
						'username' 		=> $username,
						'password' 		=> $password,
						'level' 		=> $level,
						'status' 		=> $status,
						'kd_karyawan' 	=> $id_karyawan
					);
				$karyawan = array(
						'id_karyawan' 	=> $id_karyawan,
						'nm_karyawan' 	=> $nama,
						'id_pekerjaan'	=> $pekerjaan,
						'tarif'			=> $tarif
					);
				$this->m_karyawan->add($karyawan, $user);
				redirect('karyawan');
			}else{
				echo"<script>alert('Username Sudah ada');history.go(-1);</script>";
			}
		}	
	}
	public function ajax_edit($id)
		{
			$where = array(
					'user.kd_karyawan' => $id
				);
			$data = $this->m_karyawan->get_by_id($where);
			echo json_encode($data);
		}
	
	public function update()
		{
			$user = array(
					'username' 		=> $this->input->post('edit_username'),
					'password' 		=> (!empty($this->input->post('edit_password'))) ? md5($this->input->post('edit_password')) : $this->input->post('old_password'),
					'level' 		=> $this->input->post('edit_level'),
					'status' 		=> $this->input->post('edit_status'),
					'kd_karyawan' 	=> $this->input->post('edit_id')
				);
			$karyawan = array(
					'id_karyawan' 	=> $this->input->post('edit_id'),
					'nm_karyawan' 	=> $this->input->post('edit_nama'),
					'id_pekerjaan' 	=> $this->input->post('edit_pekerjaan'),
					'tarif' 		=> $this->input->post('edit_tarif')
				);
			$this->m_karyawan->update($this->input->post('edit_id'), $karyawan, $user);
			echo json_encode(array("status" => TRUE));
		}
	public function delete($id)
		{
			$this->m_karyawan->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}
}	