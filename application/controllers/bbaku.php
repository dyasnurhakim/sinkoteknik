<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Bbaku extends Main {

	function __construct(){
		parent::__construct();
	}

	function index(){
		
 			$data['active_menu'] = "bbaku";
            
            $data['bb']=$this->m_bahanbaku->get_all_bb();
			$data['kodeunik'] = $this->m_bahanbaku->getkodeunik();
			$this->global_data = $data;
	        $this->tampilan('admin/bbaku/bahan_baku');
		
	}
	 
	public function bb_add(){
			$data = array(
					'id' => $this->input->post('id'),
					'nama_bb' => $this->input->post('nama_bb'),
					'harga' => $this->input->post('harga'),
					'satuan' => $this->input->post('satuan')
				);

			$insert = $this->m_bahanbaku->bb_add($data);
			
			echo json_encode(array("status" => TRUE));
		}
	public function ajax_edit($id){
			$data = $this->m_bahanbaku->get_by_id($id);
			echo json_encode($data);
		}
	
	public function bb_update(){
			$data = array(
					'id' => $this->input->post('id'),
					'nama_bb' => $this->input->post('nama_bb'),
					'harga' => $this->input->post('harga'),
					'satuan' => $this->input->post('satuan')
				);
			$this->m_bahanbaku->bb_update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
		}	
	public function bb_delete($id){
			$this->m_bahanbaku->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}

}
