<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Biaya_oh extends Main {
	function __construct(){
		parent::__construct();
	
	}

	function index(){
			$data['active_menu'] = 'oh';
            $data['oh']=$this->m_oh->get_all_oh();
			$data['kodeunik'] = $this->m_oh->getkodeunik();
			$data['kelompok_bop'] = $this->m_oh->data_kelompok_bop();
			$data['coa'] = $this->m_oh->data_coa();
			$this->global_data = $data;
	        $this->tampilan('admin/oh/oh');
		
	}
	 
	public function oh_add(){
			$data = array(
					'kd_bop' => $this->input->post('kd_bop'),
					'nm_bop' => $this->input->post('nm_bop'),
					'satuan_bop' => $this->input->post('satuan_bop'),
					'kd_coa' => $this->input->post('kd_coa'),
				);
			$insert = $this->m_oh->oh_add($data);
			
			echo json_encode(array("status" => TRUE));
		}
	public function ajax_edit($id){
			$data = $this->m_oh->get_by_id($id);
			echo json_encode($data);
		}
	
	public function oh_update(){
			$data = array(
					'kd_bop' => $this->input->post('kd_bop'),
					'nm_bop' => $this->input->post('nm_bop'),
					'satuan_bop' => $this->input->post('satuan_bop'),
					'kd_coa' => $this->input->post('kd_coa'),
				);
			$this->m_oh->oh_update(array('kd_bop' => $this->input->post('kd_bop')), $data);
			echo json_encode(array("status" => TRUE));
		}	
	public function oh_delete($id){
			$this->m_oh->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}

}
