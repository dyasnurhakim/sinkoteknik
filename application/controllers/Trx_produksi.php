<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Trx_produksi extends Main {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->getSession();
		$data['active_menu'] = "trx_produksi";
		$data['pesanan'] = $this->m_trx_produksi->get_pesanan();
		$data['kodeunik'] = $this->m_trx_produksi->getkodeunik();
		$data['produksi_produk'] = $this->sessions['produksi_produk'];
		if(isset($data['produksi_produk'])){
			$data['produk'] = $this->m_trx_produksi->getProduk_customer($data['produksi_produk']['kd_pesanan']);

		}
		$this->global_data = $data;
        $this->tampilan('admin/trx_produksi/trx_produksi');
	}

	public function getOneCustomer($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_trx_produksi->getOne_customer_join_pesanan($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function getOneDetailPesanan($pesanan,$produk){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_trx_produksi->getOne_detail_pesanan($pesanan,$produk);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function produksi_produk(){
		$this->getSession();
		$session['kd_produksi'] = $this->input->post('kd_produksi');
		$session['id_pelanggan'] = $this->input->post('id_pelanggan');
		$session['kd_pesanan'] = $this->input->post('kd_pesanan');
		$session['nm_pelanggan'] = $this->input->post('nm_pelanggan');
		$session['tgl_transaksi'] = $this->input->post('tgl_transaksi');
		$this->sessions['produksi_produk'] = $session;
		$this->sessions['karyawan_produk'] = array();
		$this->saveSession();
		redirect('trx_produksi');
	}

	public function cancel_produksi_produk(){
		$this->sessions['produksi_produk'] = FALSE;
		$this->sessions['karyawan_produk'] = FALSE;
		$this->saveSession();
		redirect('trx_produksi');
	}

	public function detail_produk(){
		$this->getSession();
		$data['active_menu'] = 'trx_produksi';
		$kd_produk = $this->input->post('kd_produk');
		$karyawan = array();
		$pekerjaan = $this->m_trx_produksi->get_pekerjaan($kd_produk);
		foreach ($pekerjaan as $row) {
			$karyawan[$row->id] = $this->m_karyawan->get_by(array('id_pekerjaan' => $row->id));
		}

		$data['produksi_produk'] = $this->sessions['produksi_produk'];
		$data['karyawan_produk'] = $this->sessions['karyawan_produk'];
		$data['kd_produk'] = $kd_produk;
		$data['produk'] = $this->m_trx_produksi->getOne_detail_pesanan($data['produksi_produk']['kd_pesanan'],$kd_produk)[0];
		$data['pekerjaan'] = $pekerjaan;
		$data['karyawan'] = $karyawan;
		$data['bbaku'] = $this->m_trx_produksi->get_bbaku($kd_produk);
		$data['bop'] = $this->m_trx_produksi->get_bop($kd_produk);
		$data['customer'] = $this->m_trx_produksi->getOne_customer($data['produksi_produk']['id_pelanggan'])[0];
		$this->global_data = $data;
		$this->tampilan('admin/trx_produksi/trx_produksi_detail');
	}

	public function tambah_pekerjaan_produk(){
		$this->getSession();
		$post = $this->input->post();
		$kd_produk = $post['kd_produk'];
		$karyawan = $this->sessions['karyawan_produk'];
		$data_karyawan = $this->input->post();
		$pekerjaan = $this->m_trx_produksi->get_pekerjaan($kd_produk);
		foreach ($pekerjaan as $row) {
			$pekerja = array();
			foreach ($post[$row->id] as $value) {
				$temp = $this->m_karyawan->get_by_id(array('id_karyawan' => $value));

				$temp2['id_karyawan'] = $temp->id_karyawan;
				$temp2['nm_karyawan'] = $temp->nm_karyawan;
				$temp2['tarif'] = $temp->tarif;
				array_push($pekerja, $temp2);
			}
			$karyawan[$kd_produk][$row->id]['karyawan'] = $pekerja;
			
		}

		$this->sessions['karyawan_produk'] = $karyawan;
		$this->saveSession();
		redirect('trx_produksi');

	}

	public function check_produksi($kd_pesanan){
		$this->getSession();
		$data['active_menu'] = 'trx_produksi';
		$data['karyawan_produk'] = $this->sessions['karyawan_produk'];
		$data['produksi_produk'] = $this->sessions['produksi_produk'];
		$data['listproduk'] = $this->m_trx_produksi->get_detail_pesanan(array('kd_pesanan' => $data['produksi_produk']['kd_pesanan']));
		foreach ($data['listproduk'] as $produk) {
			$pekerjaan[$produk->kd_produk] = $this->m_trx_produksi->get_pekerjaan($produk->kd_produk);
			$bbaku[$produk->kd_produk] = $this->m_trx_produksi->get_bbaku($produk->kd_produk);
			$bop[$produk->kd_produk] = $this->m_trx_produksi->get_bop($produk->kd_produk);
		}
		$data['pekerjaan'] = $pekerjaan;
		$data['bbaku'] = $bbaku;
		$data['bop'] = $bop;
		$data['customer'] = $this->m_trx_produksi->getOne_customer($data['produksi_produk']['id_pelanggan'])[0];
		$this->global_data = $data;
		$this->tampilan('admin/trx_produksi/trx_produksi_checkout');

	}

	public function simpan_produksi(){
		$this->getSession();
		$session = $this->sessions['produksi_produk'];
		$karyawan_produk = $this->sessions['karyawan_produk'];

			$where = array(
						'kd_pesanan' => $session['kd_pesanan']
					);
			$pesanan = $this->m_trx_produksi->get_detail_pesanan($where);

			$field = array(
				'kd_produksi' 	=> $session['kd_produksi'],
				'kd_pesanan' 	=> $session['kd_pesanan'],
				'tgl_produksi' 	=> $session['tgl_transaksi']
				);
			if($this->m_trx_produksi->add_produksi($field)){

				foreach ($pesanan as $data) {
					
					$field = array(
								'kd_produksi' 		=> $session['kd_produksi'],
								'kd_produk'			=> $data->kd_produk,
								'qty_produk'		=> $data->qty_pesanan,
								'harga_produk'		=> $data->harga_prd
							);
					$this->m_trx_produksi->add_detail_produksi($field);

					$pekerjaan = $this->m_trx_produksi->get_pekerjaan($data->kd_produk);
					$bbaku = $this->m_trx_produksi->get_bbaku($data->kd_produk);
					$bop = $this->m_trx_produksi->get_bop($data->kd_produk);
					$totalbtkl = 0;
					foreach ($pekerjaan as $row) {
						foreach ($karyawan_produk[$row->kd_produk][$row->kd_pekerjaan]['karyawan'] as $row_karyawan) {
							$field = array(
								"kd_produksi" 	=> $session['kd_produksi'],
								"kd_pekerjaan" 	=> $row->kd_pekerjaan,
								"kd_produk" 	=> $row->kd_produk,
								"id_karyawan" 	=> $row_karyawan['id_karyawan'],
								"jumlah_jam"	=> $row->jumlah_hari,
								"tarif_btkl"	=> $row_karyawan['tarif'],
								"subtotal"		=> $row->jumlah_hari * $row_karyawan['tarif']
								);
							$totalbtkl += $field['subtotal'];
							$this->m_trx_produksi->add_detail_btkl($field);
						}
					}
					$totalbb = 0;
					foreach ($bbaku as $row) {
						$field = array(
							"kd_produksi" 	=> $session['kd_produksi'],
							"kd_bb" 		=> $row->kd_bbaku,
							"kd_produk" 	=> $row->kd_produk,
							"qty_bbb"		=> $row->qty * $data->qty_pesanan,
							"hrg_bbb"		=> $row->harga,
							"sub_bbb"		=> ($row->qty * $data->qty_pesanan) * $row->harga
							);
						$totalbb += $field['sub_bbb'];
						$this->m_trx_produksi->add_detail_bbb($field);
					}
					$totalbop = 0;
					foreach ($bop as $row) {
						$field = array(
							"kd_produksi" 	=> $session['kd_produksi'],
							"kd_bop" 		=> $row->kd_bop,
							"kd_produk" 	=> $row->kd_produk,
							"qty_bop"		=> $row->qty * $data->qty_pesanan,
							"nominal_bop"	=> $row->nominal,
							"subtotal_bop"	=> ($row->qty * $data->qty_pesanan) * $row->nominal
							);
						$totalbop += $field['subtotal_bop'];
						$this->m_trx_produksi->add_detail_bop($field);
					}

				$this->save_coa(9, 'Debit', $totalbb);
				$this->save_coa(10, 'Kredit', $totalbb);


				$this->save_coa(14, 'Debit', $totalbtkl);
				$this->save_coa(11, 'Kredit', $totalbtkl);


				$this->save_coa(13, 'Debit', $totalbop);
				$this->save_coa(12, 'Kredit', $totalbop);


				$this->save_coa(15, 'Debit', $totalbop + $totalbb + $totalbtkl);
				$this->save_coa(9, 'Kredit', $totalbb);
				$this->save_coa(14, 'Kredit', $totalbtkl);
				$this->save_coa(13, 'Kredit', $totalbop);
				}
				$this->sessions['produksi_produk'] = FALSE;
				$this->sessions['karyawan_produk'] = FALSE;
				$this->saveSession();
			}

			redirect('trx_produksi');
	}

}