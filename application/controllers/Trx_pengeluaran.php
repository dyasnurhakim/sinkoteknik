<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Trx_pengeluaran extends Main {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->getSession();
		$data['active_menu'] = "trx_pengeluaran";
		$data['bop'] = $this->m_trx_pengeluaran->get_bop();
		$data['kodeunik'] = $this->m_trx_pengeluaran->getkodeunik();
		$data['detail_pengeluaran'] = $this->sessions['detail_pengeluaran'];
		$this->global_data = $data;
        $this->tampilan('admin/trx_pengeluaran/trx_pengeluaran');
	}

	public function getOneBOP($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_trx_pengeluaran->getOne_BOP($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function tambah_pengeluaran(){
		$this->getSession();
		$bop = $this->m_trx_pengeluaran->getOne_bop($this->input->post('kd_bop'));
		$session = array();
		$barang = array();
		if($this->sessions['detail_pengeluaran']){
			$session = $this->sessions['detail_pengeluaran'];
			$index = $session['last'];
			$barang = $session['data'];
		} else {
			$index = 0;
			$session['tgl'] = $this->input->post('tgl_transaksi');
			$session['kodeunik'] = $this->input->post('kode_transaksi');
			$session['keterangan'] = $this->input->post('keterangan');
			$session['total'] = 0;
		}

		$barang[$index] = 	array(
						'kd_bop' 		=> $bop[0]->kd_bop,
						'nm_bop' 		=> $bop[0]->nm_bop,
						'satuan_bop'	=> $bop[0]->satuan_bop,
						'id_coa'		=> $bop[0]->kd_coa,
						'kebutuhan' 	=> $this->input->post('kebutuhan'),
						'nominal' 		=> $this->input->post('nominal'),
					);
		$session['total'] += $barang[$index]['kebutuhan'] * $barang[$index]['nominal'];
		$session['last'] = $index+1;
		$session['data'] = $barang;
		$this->sessions['detail_pengeluaran'] = $session;
		$this->saveSession();
		redirect('trx_pengeluaran');
	}

	public function remove_bop($no){
		$this->getSession();
		$session = $this->sessions['detail_pengeluaran'];
		$data = $session['data'];
		$session['total'] -= $data[$no]['kebutuhan'] * $data[$no]['nominal'];
		$data[$no] = NULL;
		for($i = $no ; $i < $session['last'] ; $i++){
			$data[$i] = $data[$i+1];
		}
		$session['last'] = $session['last'] - 1;
		$session['data'] = $data;
		$this->sessions['detail_pengeluaran'] = $session;
		$this->saveSession();
		redirect('trx_pengeluaran');
	}

	public function simpan_pengeluaran(){
		$this->getSession();
		$session = $this->sessions['detail_pengeluaran'];
		$data = $session['data'];
		$field = array(
			'kd_pengeluaran' 	=> $session['kodeunik'],
			'keterangan' 		=> $session['keterangan'],
			'tgl_pengeluaran' 	=> $session['tgl'],
			'total'				=> $session['total']
			);
		if($this->m_trx_pengeluaran->add_pengeluaran($field)){
			for($i = 0;$i < $session['last'];$i++){
				$field = array(
					"kd_pengeluaran" 	=> $session['kodeunik'],
					"kd_bop" 			=> $data[$i]['kd_bop'],
					"kebutuhan"			=> $data[$i]['kebutuhan'],
					"nominal"			=> $data[$i]['nominal'],
					"subtotal"			=> $data[$i]['kebutuhan'] * $data[$i]['nominal']
					);
				$this->m_trx_pengeluaran->add_detail_pengeluaran($field);
				$this->save_coa($data[$i]['id_coa'], 'Debit', $field['subtotal']);
				$this->save_coa(2, 'Kredit', $field['subtotal']);
			}
			$this->sessions['detail_pengeluaran'] = FALSE;
			$this->saveSession();
		}

		redirect('trx_pengeluaran');

	}

	public function check_pengeluaran(){
		$this->getSession();
		$data['active_menu'] = 'trx_pengeluaran';
		$data['detail_pengeluaran'] = $this->sessions['detail_pengeluaran'];
		$data['kodeunik'] = $data['detail_pengeluaran']['kodeunik'];
		$data['tgl_transaksi'] = $data['detail_pengeluaran']['tgl'];
		$this->global_data = $data;
		$this->tampilan('admin/trx_pengeluaran/trx_pengeluaran_checkout');
	}

	public function cancel_pengeluaran(){
		$this->sessions['detail_pengeluaran'] = FALSE;
		$this->saveSession();
		redirect('trx_pengeluaran');
	}
	
}