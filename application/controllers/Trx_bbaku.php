<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Trx_bbaku extends Main {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->getSession();
		$data['active_menu'] = "trx_bbaku";
		$data['bbaku'] = $this->m_trx_bbaku->get_bbaku();
		$data['supplier'] = $this->m_trx_bbaku->get_supplier();
		$data['detail_pembelian'] = $this->sessions['pembelian_bbaku'];
		$data['supplier_pembelian'] = $this->sessions['supplier_pembelian'];
		$this->global_data = $data;
        $this->tampilan('admin/trx_bbaku/trx_bbaku');
	}

	public function tambah_supplier(){
		$this->getSession();
		$supplier = $this->m_trx_bbaku->getOne_supplier($this->input->post('kd_supplier'));
		$this->sessions['supplier_pembelian'] = $supplier[0];
		$this->saveSession();
		redirect('trx_bbaku');
	}

	public function tambah_bbaku(){
		$this->getSession();
		$supplier = $this->m_trx_bbaku->getOne_supplier($this->input->post('kd_supplier'));
		$bbaku = $this->m_trx_bbaku->getOne_bbaku($this->input->post('id_bb'));
		$session = array();
		$barang = array();
		if($this->sessions['pembelian_bbaku']){
			$session = $this->sessions['pembelian_bbaku'];
			$index = $session['last'];
			$barang = $session['data'];
		} else {
			$index = 0;
			$session['tgl'] = $this->input->post('tgl_transaksi');
			$session['kodeunik'] = $this->m_trx_bbaku->getkodeunik();
			$session['total'] = 0;
		}

		$barang[$index] = 	array(
						'kd_supplier' 	=> $supplier[0]->kd_supplier,
						'nm_supplier' 	=> $supplier[0]->nm_supplier,
						'kd_bb' 		=> $bbaku[0]->id,
						'nama_bb' 		=> $bbaku[0]->nama_bb,
						'qty' 			=> $this->input->post('qty'),
						'harga' 		=> $this->input->post('harga')
					);
		$session['total'] += $barang[$index]['qty'] * $barang[$index]['harga'];
		$session['last'] = $index+1;
		$session['data'] = $barang;
		$this->sessions['pembelian_bbaku'] = $session;
		$this->saveSession();
		redirect('trx_bbaku');
	}

	public function remove_bbaku($no){
		$this->getSession();
		$session = $this->sessions['pembelian_bbaku'];
		$data = $session['data'];
		$session['total'] -= $data[$no]['qty'] * $data[$no]['harga'];
		$data[$no] = NULL;
		for($i = $no ; $i < $session['last'] ; $i++){
			$data[$i] = $data[$i+1];
		}
		$session['last'] = $session['last'] - 1;
		$session['data'] = $data;
		$this->sessions['pembelian_bbaku'] = $session;
		$this->saveSession();
		redirect('trx_bbaku');
	}

	public function tambah_pembelian(){
		$this->getSession();
		$session = $this->sessions['pembelian_bbaku'];
		$data = $session['data'];
		$field = array(
			'kd_pembelian' 	=> $session['kodeunik'],
			'tgl_beli' 		=> $session['tgl'],
			'total' 		=> $session['total']
			);
		if($this->m_trx_bbaku->add_pembelian($field)){
			for($i = 0;$i < $session['last'];$i++){
				$field = array(
					"kd_pembelian" 	=> $session['kodeunik'],
					"kd_bb" 		=> $data[$i]['kd_bb'],
					"kd_supplier" 	=> $data[$i]['kd_supplier'],
					"qty_beli"		=> $data[$i]['qty'],
					"harga_beli"	=> $data[$i]['harga'],
					"total_beli"	=> $data[$i]['qty'] * $data[$i]['harga']
					);
				$this->m_trx_bbaku->add_detail_pembelian($field);
			}
			$this->save_coa(7, 'Debit', $session['total']);
			$this->save_coa(2, 'Kredit', $session['total']);
			$this->sessions['pembelian_bbaku'] = FALSE;
			$this->sessions['supplier_pembelian'] = FALSE;
			$this->saveSession();
		}

		redirect('trx_bbaku');

	}
	
	public function check_pembelian(){
		$this->getSession();
		$data['active_menu'] = 'trx_bbaku';
		$data['detail_pembelian'] = $this->sessions['pembelian_bbaku'];
		$data['kodeunik'] = $data['detail_pembelian']['kodeunik'];
		$data['tgl_transaksi'] = $data['detail_pembelian']['tgl'];
		$this->global_data = $data;
		$this->tampilan('admin/trx_bbaku/trx_bbaku_checkout');
	}

	public function cancel_pembelian(){
		$this->sessions['supplier_pembelian'] = FALSE;
		$this->sessions['pembelian_bbaku'] = FALSE;
		$this->saveSession();
		redirect('trx_bbaku');
	}

	public function change_supplier(){
		$this->sessions['supplier_pembelian'] = FALSE;
		$this->saveSession();
		redirect('trx_bbaku');
	}
	
	public function list_pembelian(){
		$data['active_menu'] = "data_trx_bbaku";
            
        $data['data_pembelian']=$this->m_trx_bbaku->get_allPembelian();
		$this->global_data = $data;
        $this->tampilan('admin/trx_bbaku/list_trx_bbaku');
	}

	public function detail_pembelian($id){
		$data['active_menu'] = "data_trx_bbaku";
        $where = array(
        			'kd_pembelian' => $id
        	);
        $data['pembelian'] = $this->m_trx_bbaku->get_onePembelian($where);
        $data['datadetail_pembelian'] = $this->m_trx_bbaku->get_DetailPembelian($where);
		$this->global_data = $data;
        $this->tampilan('admin/trx_bbaku/detail_trx_bbaku');
	}
}