<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Buku_besar extends Main {

	function __construct(){
		parent::__construct();
	}

	function index(){
		
 			$data['active_menu'] 	= "buku_besar";
 			$data['data_coa']		= $this->m_buku_besar->getAllCoa();
 			$this->global_data = $data;
	        $this->tampilan('admin/buku_besar/buku_besar');
		
	}

	public function get_buku_besar(){
		$data['active_menu'] = "buku_besar";
		$kd_akun = $this->input->post('kd_akun');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');


		$listbuku = $this->m_buku_besar->getAll_Jurnal($bulan, $tahun, $kd_akun);

		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['data'] = $listbuku;
		$this->global_data = $data;
		$this->tampilan('admin/buku_besar/list_buku_besar');
	}

}