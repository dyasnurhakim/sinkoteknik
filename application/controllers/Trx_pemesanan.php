<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Trx_pemesanan extends Main {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->getSession();
		$data['active_menu'] = "trx_pemesanan";
		$data['customer'] = $this->m_trx_pemesanan->get_customer();
		$data['produk'] = $this->m_trx_pemesanan->get_produk();
		$data['kodeunik'] = $this->m_trx_pemesanan->getkodeunik();
		$data['detail_pembelian'] = $this->sessions['pembelian_produk'];
		$this->global_data = $data;
        $this->tampilan('admin/trx_pemesanan/trx_pemesanan');
	}

	public function getOneCustomer($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_trx_pemesanan->getOne_customer($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function getOneProduk($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_trx_pemesanan->getOne_produk($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function tambah_produk(){
		$this->getSession();
		$produk = $this->m_trx_pemesanan->getOne_produk($this->input->post('id_produk'));
		$session = array();
		$barang = array();
		if($this->sessions['pembelian_produk']){
			$session = $this->sessions['pembelian_produk'];
			$index = $session['last'];
			$barang = $session['data'];
		} else {
			$index = 0;
			$session['tgl'] = $this->input->post('tgl_transaksi');
			$session['kodeunik'] = $this->input->post('kode_transaksi');
			$session['nama_pelanggan'] = $this->input->post('nama_pelanggan');
			$session['id_pelanggan'] = $this->input->post('kd_customer');
			$session['total'] = 0;
		}

		$barang[$index] = 	array(
						'id_produk' 	=> $produk[0]->kd_produk,
						'nm_produk' 	=> $produk[0]->nm_produk,
						'qty' 			=> $this->input->post('qty'),
						'harga' 		=> $produk[0]->harga
					);
		$session['total'] += $barang[$index]['qty'] * $barang[$index]['harga'];
		$session['last'] = $index+1;
		$session['data'] = $barang;
		$this->sessions['pembelian_produk'] = $session;
		$this->saveSession();
		redirect('trx_pemesanan');
	}

	public function remove_produk($no){
		$this->getSession();
		$session = $this->sessions['pembelian_produk'];
		$data = $session['data'];
		$session['total'] -= $data[$no]['qty'] * $data[$no]['harga'];
		$data[$no] = NULL;
		for($i = $no ; $i < $session['last'] ; $i++){
			$data[$i] = $data[$i+1];
		}
		$session['last'] = $session['last'] - 1;
		$session['data'] = $data;
		$this->sessions['pembelian_produk'] = $session;
		$this->saveSession();
		redirect('trx_pemesanan');
	}

	public function tambah_pemesanan(){
		$this->getSession();
		$session = $this->sessions['pembelian_produk'];
		$data = $session['data'];
		$field = array(
			'kd_pesanan' 	=> $session['kodeunik'],
			'kd_pelanggan' 	=> $session['id_pelanggan'],
			'tgl_pesan' 	=> $session['tgl']
			);
		if($this->m_trx_pemesanan->add_pesanan($field)){
			for($i = 0;$i < $session['last'];$i++){
				$field = array(
					"kd_pesanan" 	=> $session['kodeunik'],
					"kd_produk" 	=> $data[$i]['id_produk'],
					"qty_pesanan"	=> $data[$i]['qty'],
					"harga_prd"		=> $data[$i]['harga'],
					"sub_pesanan"	=> $data[$i]['harga'] * $data[$i]['qty']
					);
				$this->m_trx_pemesanan->add_detail_pesanan($field);
			}
			$this->save_coa(2, 'Debit', $session['total']);
			$this->save_coa(8, 'Kredit', $session['total']);
			$this->sessions['pembelian_produk'] = FALSE;
			$this->saveSession();
		}

		redirect('trx_pemesanan');

	}

	public function check_pemesanan(){
		$this->getSession();
		$data['active_menu'] = 'trx_pemesanan';
		$data['detail_pembelian'] = $this->sessions['pembelian_produk'];
		$data['kodeunik'] = $data['detail_pembelian']['kodeunik'];
		$data['tgl_transaksi'] = $data['detail_pembelian']['tgl'];
		$this->global_data = $data;
		$this->tampilan('admin/trx_pemesanan/trx_pemesanan_checkout');
	}

	public function cancel_pemesanan(){
		$this->sessions['pembelian_produk'] = FALSE;
		$this->saveSession();
		redirect('trx_pemesanan');
	}
	
	public function list_pesanan(){
		$data['active_menu'] = "data_trx_pemesanan";
            
        $data['data_pemesanan']=$this->m_trx_pemesanan->get_allPesanan();
		$this->global_data = $data;
        $this->tampilan('admin/trx_pemesanan/list_trx_pemesanan');
	}

	public function detail_pesanan($id){
		$data['active_menu'] = "data_trx_pemesanan";
        $where = array(
        			'kd_pesanan' => $id
        	);
        $data['pesanan'] = $this->m_trx_pemesanan->get_onePesanan($where);
        $data['datadetail_pesanan'] = $this->m_trx_pemesanan->get_DetailPesanan($where);
		$this->global_data = $data;
        $this->tampilan('admin/trx_pemesanan/detail_trx_pemesanan');
	}
}