<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->sessions = $this->session->userdata('session');
		if($this->sessions['user']){
			$user = $this->sessions['user'];
	        if($user['isLogin']){
	            redirect('dashboard');
	        }	
		}
		
	}

	function index(){
		
		$this->load->view('login');

	}
	

	function aksi_login(){
			$this->form_validation->set_rules('username','Username','required|trim|xss_clean'); 
			$this->form_validation->set_rules('password','Password','required|trim|xss_clean');
			
			if($this->form_validation->run()==FALSE){

				// $this->load->view('main/header');
				$this->load->view('login');
				// $this->load->view('main/footer');
			}else{	
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$bersih_username = $this->security->xss_clean($username);
				$bersih_password = $this->security->xss_clean($password);

				$hitung_user = $this->m_auth->hitung_user($username,$password); 
				if($hitung_user==1){
						$cek_user= $this->m_auth->get_user($username,$password); 
						$cek_level = $this->m_auth->cek_level($username,$password);
						$level = $cek_level->level;
						$id= $cek_user->kd_karyawan;
						$user['isLogin'] = TRUE;
						$user['user_id'] = $id;
						$user['username'] = $username;
						$user['level'] = $level;
						$this->sessions['user'] = $user;
						$this->sessions['produk_detail'] = FALSE;
						$this->sessions['produk'] = FALSE;
						$this->sessions['pembelian_bbaku'] = FALSE;
						$this->sessions['supplier_pembelian'] = FALSE;
						$this->sessions['pembelian_produk'] = FALSE;
						$this->sessions['detail_pengeluaran'] = FALSE;
						$this->sessions['produksi_produk'] = FALSE;
						$this->sessions['karyawan_produk'] = FALSE;
						$this->session->set_userdata('session',$this->sessions);
						redirect('auth');
					
				}else{		
							echo"<script>
							alert('Gagal Login cek username');
							history.go(-1);
							</script>";
				}

			}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */