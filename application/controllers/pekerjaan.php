<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Pekerjaan extends Main {

	function __construct(){
		parent::__construct();
	}

	function index(){
            $data['active_menu'] = 'pekerjaan';
            $data['pekerjaan']=$this->m_pekerjaan->get_all_pekerjaan();
			$data['kodeunik'] = $this->m_pekerjaan->getkodeunik();
			$this->global_data = $data;
	        $this->tampilan('admin/pekerjaan/pekerjaan');
		
	}
	 
	public function pekerjaan_add(){
			$data = array(
					'id' => $this->input->post('id'),
					'nama_pekerjaan' => $this->input->post('nama_pekerjaan')
					// 'tanggal' => $this->input->post('tanggal'),
				);
			$insert = $this->m_pekerjaan->pekerjaan_add($data);
			echo json_encode(array("status" => TRUE));
		}
	public function ajax_edit($id){
			$data = $this->m_pekerjaan->get_by_id($id);
			echo json_encode($data);
		}
	
	public function pekerjaan_update(){
			$data = array(
					'nama_pekerjaan' => $this->input->post('nama_pekerjaan')
					// 'tanggal' => $this->input->post('tanggal'),
				);
			$this->m_pekerjaan->pekerjaan_update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
		}	
	public function pekerjaan_delete($id){
			$this->m_pekerjaan->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}

}
