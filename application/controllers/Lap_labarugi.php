<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Lap_labarugi extends Main {

	function __construct(){
		parent::__construct();
	}

	function index(){
		
 			$data['active_menu'] = "lap_labarugi";
 			$this->global_data = $data;
	        $this->tampilan('admin/lap_labarugi/lap_labarugi');
		
	}

	public function get_labarugi(){
		$data['active_menu'] = "lap_labarugi";
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');


		$data['totalpesanan'] = $this->m_lap_labarugi->get_totalPesanan_by($bulan, $tahun);
		$data['totalproduksi'] = $this->m_lap_labarugi->get_totalProduksi_by($bulan, $tahun);
		$data['totalpembelian'] = $this->m_lap_labarugi->get_totalPembelian_by($bulan, $tahun);
		$listpengeluaran = $this->m_lap_labarugi->get_AllPengeluaran_by($bulan, $tahun);
		$data['listbop'] = $this->m_oh->get_all_oh();

		$total_bop = array();
		foreach ($listpengeluaran as $row) {
			$detail = $this->m_lap_labarugi->get_DetailPengeluaran($row->kd_pengeluaran);

			foreach ($detail as $rowDetail) {
				if(empty($total_bop[$rowDetail->kd_bop])){
					$total_bop[$rowDetail->kd_bop] = $rowDetail->subtotal;
				} else {
					$total_bop[$rowDetail->kd_bop] += $rowDetail->subtotal;
				}
			}
		}

		$data['totalbop'] = $total_bop; 
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$this->global_data = $data;
		$this->tampilan('admin/lap_labarugi/list_laporan');
	}

	public function get_laporan(){
		$data['active_menu'] = "lap_produksi";
		$start_date = $this->input->post('start_date');
		$finish_date = $this->input->post('finish_date');

		if($start_date > $finish_date ){
			echo "<script>alert('Tanggal Awal tidak Boleh lebih besar dari Tanggal AKhir')</script>";
			echo "<meta http-equiv='refresh' content='0;url=".base_url()."lap_produksi'>";
		} else {

			$listproduksi = $this->m_lap_produksi->get_AllProduksi_byDate($start_date, $finish_date);
			$listpekerjaan = $this->m_pekerjaan->get_all_pekerjaan();
			$listbiayaOH = $this->m_oh->get_all_oh();

			if(empty($listproduksi)){
				echo "<script>alert('Tidak ada transaksi yang terjadi')</script>";
				echo "<meta http-equiv='refresh' content='0;url=".base_url()."lap_produksi'>";
			} else {
				$total_bbaku = array();
				$total_bop = array();
				$total_btkl = array();
				$total_lap = array();
				foreach ($listproduksi as $produksi) {

					$listproduk = $this->m_trx_pemesanan->get_DetailPesanan(array('kd_pesanan' => $produksi->kd_pesanan));

					foreach ($listproduk as $produk) {
						if(empty($produks[$produk->kd_produk])){
							$temp['kd_produk'] = $produk->kd_produk;
							$temp['nm_produk'] = $produk->nm_produk;
							$produks[$produk->kd_produk] = $temp;	
						}
						foreach ($listpekerjaan as $pekerjaan) {

							$where = array(
								'kd_produksi' 	=> $produksi->kd_produksi,
								'kd_produk'		=> $produk->kd_produk,
								'kd_pekerjaan'	=> $pekerjaan->id
							);

							$total = $this->m_lap_produksi->get_totalbtkl($where);

							if(empty($total)){
								$total = 0;
							}

							if(empty($total_btkl[$produk->kd_produk])){
								$total_btkl[$produk->kd_produk][$pekerjaan->id] = $total;
							} else {
								$total_btkl[$produk->kd_produk][$pekerjaan->id] += $total;
							}
						}

						foreach ($listbiayaOH as $biayaOH) {

							$where = array(
								'kd_produksi' 	=> $produksi->kd_produksi,
								'kd_produk'	=> $produk->kd_produk,
								'kd_bop'		=> $biayaOH->kd_bop
							);

							$total = $this->m_lap_produksi->get_totalbop($where);

							if(empty($total)){
								$total = 0;
							}

							if(empty($total_bop[$produk->kd_produk])){
								$total_bop[$produk->kd_produk][$biayaOH->kd_bop] = $total;
							} else {
								$total_bop[$produk->kd_produk][$biayaOH->kd_bop] += $total;
							}
						}

						$where = array(
							'kd_produksi' 	=> $produksi->kd_produksi,
							'kd_produk'	=> $produk->kd_produk
						);

						$total += $this->m_lap_produksi->get_totalbbb($where);

						if(empty($total)){
							$total = 0;
						}

						if(empty($total_bbaku[$produk->kd_produk])){
							$total_bbaku[$produk->kd_produk] = $total;
						} else {
							$total_bbaku[$produk->kd_produk] += $total;
						}

					}
				}
				$data['listproduk'] = $produks;
				$data['listpekerjaan'] = $listpekerjaan;
				$data['listbop'] = $listbiayaOH;
				$data['total_bbaku'] = $total_bbaku;
				$data['total_bop'] = $total_bop;
				$data['total_btkl'] = $total_btkl;
				$data['tgl_awal'] = $start_date;
				$data['tgl_akhir'] = $finish_date;
				$this->global_data = $data;
				$this->tampilan('admin/lap_produksi/list_laporan');
			}
		}
	}

}