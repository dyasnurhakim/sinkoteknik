<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Jurnal extends Main {

	function __construct(){
		parent::__construct();
	}

	function index(){
		
 			$data['active_menu'] = "jurnal";
 			$this->global_data = $data;
	        $this->tampilan('admin/jurnal/jurnal');
		
	}

	public function get_jurnal(){
		$data['active_menu'] = "jurnal";
		$start_date = $this->input->post('start_date');
		$finish_date = $this->input->post('finish_date');

		if($start_date > $finish_date ){
			echo "<script>alert('Tanggal Awal tidak Boleh lebih besar dari Tanggal AKhir')</script>";
			echo "<meta http-equiv='refresh' content='0;url=".base_url()."jurnal'>";
		} else {

			$listJurnal = $this->m_jurnal->getAll_Jurnal($start_date, $finish_date);

			if(empty($listJurnal)){
				echo "<script>alert('Tidak ada transaksi yang terjadi')</script>";
				echo "<meta http-equiv='refresh' content='0;url=".base_url()."jurnal'>";
			} else {
				$data['tgl_awal'] = $start_date;
				$data['tgl_akhir'] = $finish_date;
				$data['data'] = $listJurnal;
				$this->global_data = $data;
				$this->tampilan('admin/jurnal/list_jurnal');
			}
		}
	}

}