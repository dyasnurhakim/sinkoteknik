<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Supplier extends Main {

	function __construct(){
		parent::__construct();
	}

	function index(){
		
 			$data['active_menu'] = "supplier";
            
            $data['supplier']=$this->m_supplier->getAll();
			$data['kodeunik'] = $this->m_supplier->getkodeunik();
			$this->global_data = $data;
	        $this->tampilan('admin/supplier/supplier');
		
	}
	 
	public function add(){
			$data = array(
					'kd_supplier' => $this->input->post('kd_supplier'),
					'nm_supplier' => $this->input->post('nm_supplier'),
					'alamat_supplier' => $this->input->post('alamat_supplier'),
					'email_supplier' => $this->input->post('email_supplier'),
					'tlp_supplier' => $this->input->post('tlp_supplier')
				);

			$insert = $this->m_supplier->add($data);
			
			echo json_encode(array("status" => TRUE));
		}
	public function edit($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_supplier->get_by_id($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}
	
	public function update(){
			$data = array(
					'id' => $this->input->post('kd_supplier'),
					'nama_bb' => $this->input->post('nama_bb'),
					'harga' => $this->input->post('harga'),
					
				);
			$this->m_bahanbaku->bb_update(array('id' => $this->input->post('kd_supplier')), $data);
			echo json_encode(array("status" => TRUE));
		}	
	public function delete($id){
			$this->m_supplier->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}

}

