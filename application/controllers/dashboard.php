<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Dashboard extends Main {

	function __construct(){

		parent::__construct();
	}

	public function index(){
		$data['active_menu'] = "produk";
        $data['produk']=$this->m_produk->get_all_produk();
		$this->global_data = $data;
        $this->tampilan('admin/produk/produk');
		
	}
	 
	public function tambah_produk(){
		$this->getSession();
		$data['active_menu'] = "produk";
		$data['state'] = 'add';
		$data['pekerjaan'] = $this->m_produk->get_pekerjaan();
		$data['bbaku'] = $this->m_produk->get_bbaku();
		$data['bop'] = $this->m_produk->get_bop();
		$data['kodeunik'] = $this->m_produk->getkodeunik();
		$data['produk_detail'] = $this->sessions['produk_detail'];
		$data['produk'] = $this->sessions['produk'];
		$this->global_data = $data;
		$this->tampilan('admin/produk/produk_detail');
	}

	public function add_produk(){
		$this->getSession();
		$session['kd_produk'] = $this->input->post('kd_produk');
		$session['nm_produk'] = $this->input->post('nm_produk');
		$session['harga'] = $this->input->post('harga_produk');
		$session['tgl_transaksi'] = $this->input->post('tgl_transaksi');
		$this->sessions['produk'] = $session;
		$this->saveSession();
		redirect('dashboard/tambah_produk');
	}

	public function cancel_tambah_produk(){
		$this->getSession();
		$this->sessions['produk_detail'] = FALSE;
		$this->sessions['produk'] = FALSE;
		$this->saveSession();
		redirect('dashboard');
	}

	public function cancel_detail_produk(){
		$this->getSession();
		$this->sessions['produk_detail'] = FALSE;
		$this->saveSession();
		redirect('dashboard/tambah_produk');
	}

	public function tambah_pekerjaan($kode){
		$this->getSession();
		if(empty($kode)){
			$id = $this->input->post('id_pekerjaan');
			$jml_hari = $this->input->post('jml_hari');
		} else {
			$id = $kode['kd_pekerjaan'];
			$pekerjaan_produk = $this->m_produk->get_pekerjaan_produk($kode);
			$jml_hari = $pekerjaan_produk[0]->jumlah_hari;
		}

		$pekerjaan = $this->m_produk->getOne_pekerjaan($id);
		$session = $this->sessions['produk_detail'];
		$barang = array();
		if($session['pekerjaan']){
			$data_pekerjaan = $session['pekerjaan'];
			$index = $data_pekerjaan['last'];
			$barang = $data_pekerjaan['data'];
		} else {
			$index = 0;
			$session['pekerjaan']['total'] = 0;
		}

		$barang[$index] = 	array(
						'kd_pekerjaan' 	=> $pekerjaan[0]->id,
						'nm_pekerjaan' 	=> $pekerjaan[0]->nama_pekerjaan,
						'jml_hari' 		=> $jml_hari,
						'harga' 		=> $pekerjaan[0]->tarif
					);
		$session['pekerjaan']['total'] += $barang[$index]['jml_hari'] * $barang[$index]['harga'];
		$session['pekerjaan']['last'] = $index+1;
		$session['pekerjaan']['data'] = $barang;
		$this->sessions['produk_detail'] = $session;
		$this->saveSession();
		$state = $this->input->post('state');
		if($state == "edit"){
			redirect('dashboard/edit_produk');
		} else if($state == "add"){
			redirect('dashboard/tambah_produk');
		}
	}

	public function tambah_bbaku($kode){
		$this->getSession();
		if(empty($kode)){
			$id = $this->input->post('id_bbaku');
			$qty = $this->input->post('qty');
		} else {
			$id = $kode['kd_bbaku'];
			$bbaku_produk = $this->m_produk->get_bbaku_produk($kode);
			$qty = $bbaku_produk[0]->qty;
		}
		$bbaku = $this->m_produk->getOne_bbaku($id);
		$session = $this->sessions['produk_detail'];
		$barang = array();
		if($session['bbaku']){
			$data_bbaku = $session['bbaku'];
			$index = $data_bbaku['last'];
			$barang = $data_bbaku['data'];
		} else {
			$index = 0;
			$session['bbaku']['total'] = 0;
		}

		$barang[$index] = 	array(
						'kd_bbaku' 	=> $bbaku[0]->id,
						'nm_bbaku' 	=> $bbaku[0]->nama_bb,
						'satuan' 	=> $bbaku[0]->satuan,
						'qty' 		=> $qty
					);
		$session['bbaku']['total'] += $barang[$index]['qty'] * $barang[$index]['harga'];
		$session['bbaku']['last'] = $index+1;
		$session['bbaku']['data'] = $barang;
		$this->sessions['produk_detail'] = $session;
		$this->saveSession();
		$state = $this->input->post('state');
		if($state == "edit"){
			redirect('dashboard/edit_produk');
		} else if($state == "add"){
			redirect('dashboard/tambah_produk');
		}
	}

	public function tambah_bop($kode){
		$this->getSession();
		if(empty($kode)){
			$id = $this->input->post('kd_bop');
			$kebutuhan = $this->input->post('kebutuhan');
			$nominal = $this->input->post('nominal');
		} else {
			$id = $kode['kd_bop'];
			$bop_produk = $this->m_produk->get_bop_produk($kode);
			$kebutuhan = $bop_produk[0]->qty;
			$nominal = $bop_produk[0]->nominal;
		}
		$bop = $this->m_produk->getOne_BOP($id);
		$session = $this->sessions['produk_detail'];
		$barang = array();
		if($session['bop']){
			$data_bop = $session['bop'];
			$index = $data_bop['last'];
			$barang = $data_bop['data'];
		} else {
			$index = 0;
			$session['bop']['total'] = 0;
		}

		$barang[$index] = 	array(
						'kd_bop' 	=> $bop[0]->kd_bop,
						'nm_bop' 	=> $bop[0]->nm_bop,
						'satuan' 	=> $bop[0]->satuan_bop,
						'kebutuhan' => $kebutuhan,
						'nominal' 	=> $nominal
					);
		$session['bop']['total'] += $barang[$index]['kebutuhan'] * $barang[$index]['nominal'];
		$session['bop']['last'] = $index+1;
		$session['bop']['data'] = $barang;
		$this->sessions['produk_detail'] = $session;
		$this->saveSession();
		$state = $this->input->post('state');
		if($state == "edit"){
			redirect('dashboard/edit_produk');
		} else if($state == "add"){
			redirect('dashboard/tambah_produk');
		}
	}

	public function remove_pekerjaan($state,$no){
		$this->getSession();
		$session = $this->sessions['produk_detail'];
		$pekerjaan = $session['pekerjaan'];
		$data = $pekerjaan['data'];
		$data[$no] = NULL;
		for($i = $no ; $i < $pekerjaan['last'] ; $i++){
			$data[$i] = $data[$i+1];
		}
		$pekerjaan['last'] = $pekerjaan['last'] - 1;
		$pekerjaan['data'] = $data;
		$session['pekerjaan'] = $pekerjaan;
		$this->sessions['produk_detail'] = $session;
		$this->saveSession();
		if($state == "edit"){
			redirect('dashboard/edit_produk');
		} else if($state == "add"){
			redirect('dashboard/tambah_produk');
		}
	}

	public function remove_bbaku($state,$no){
		$this->getSession();
		$session = $this->sessions['produk_detail'];
		$bbaku = $session['bbaku'];
		$data = $bbaku['data'];
		$data[$no] = NULL;
		for($i = $no ; $i < $bbaku['last'] ; $i++){
			$data[$i] = $data[$i+1];
		}
		$bbaku['last'] = $bbaku['last'] - 1;
		$bbaku['data'] = $data;
		$session['bbaku'] = $bbaku;
		$this->sessions['produk_detail'] = $session;
		$this->saveSession();
		if($state == "edit"){
			redirect('dashboard/edit_produk');
		} else if($state == "add"){
			redirect('dashboard/tambah_produk');
		}
	}

	public function remove_bop($state,$no){
		$this->getSession();
		$session = $this->sessions['produk_detail'];
		$bbaku = $session['bop'];
		$data = $bbaku['data'];
		$data[$no] = NULL;
		for($i = $no ; $i < $bbaku['last'] ; $i++){
			$data[$i] = $data[$i+1];
		}
		$bbaku['last'] = $bbaku['last'] - 1;
		$bbaku['data'] = $data;
		$session['bop'] = $bbaku;
		$this->sessions['produk_detail'] = $session;
		$this->saveSession();
		if($state == "edit"){
			redirect('dashboard/edit_produk');
		} else if($state == "add"){
			redirect('dashboard/tambah_produk');
		}
	}

	public function check_produk($state){
		$this->getSession();
		$data['active_menu'] = 'produk';
		$data['state'] = $state;
		$data['produk_detail'] = $this->sessions['produk_detail'];
		$data['produk'] = $this->sessions['produk'];
		$this->global_data = $data;
		$this->tampilan('admin/produk/produk_detail_checkout');
	}

	public function edit(){
		$where = array(
					'kd_produk'	=> $this->input->post('kd_produk')
				);
		$field = array( 
					'nm_produk'		=> $this->input->post('nm_produk'),
					'harga' 		=> $this->input->post('harga')
				);
		$this->m_produk->update_produk($where,$field);
		$edit_detail = $this->input->post('detail_produk');
		echo $edit_detail;
		if($edit_detail == "true"){
			redirect('dashboard/edit_detail'."/".$this->input->post('kd_produk'));
		} else {
			redirect('dashboard');
		}
	}

	public function edit_produk(){
		$this->getSession();
		$data['active_menu'] = "produk";
		$data['state'] = "edit";
		$data['pekerjaan'] = $this->m_produk->get_pekerjaan();
		$data['bbaku'] = $this->m_produk->get_bbaku();
		$data['bop'] = $this->m_produk->get_bop();
		$data['kodeunik'] = $this->m_produk->getkodeunik();
		$data['produk_detail'] = $this->sessions['produk_detail'];
		$data['produk'] = $this->sessions['produk'];
		$this->global_data = $data;
		$this->tampilan('admin/produk/produk_detail');
	}
	public function edit_detail($id){
		$this->getSession();
		$data = $this->m_produk->get_by_id($id);
		$session['kd_produk'] = $data[0]->kd_produk;
		$session['nm_produk'] = $data[0]->nm_produk;
		$session['harga'] = $data[0]->harga;
		$session['tgl_transaksi'] = date('Y-m-d');
		$this->sessions['produk'] = $session;
		$this->saveSession();
		$where = array(
					'kd_produk' => $id
				);
		$pekerjaan = $this->m_produk->get_pekerjaan_produk($where);
		foreach ($pekerjaan as $row) {
			$kode = array(
						'kd_produk'		=> $id,
						'kd_pekerjaan'	=> $row->kd_pekerjaan
					);
			$this->tambah_pekerjaan($kode);
		}
		$bbaku = $this->m_produk->get_bbaku_produk($where);
		foreach ($bbaku as $row) {
			$kode = array(
						'kd_produk'	=> $id,
						'kd_bbaku'	=> $row->kd_bbaku
					);
			$this->tambah_bbaku($kode);
		}
		$bop = $this->m_produk->get_bop_produk($where);
		foreach ($bop as $row) {
			$kode = array(
						'kd_produk'	=> $id,
						'kd_bop'	=> $row->kd_bop
					);
			$this->tambah_bop($kode);
		}
		redirect('dashboard/edit_produk');
	}

	public function save_detail(){
		$this->getSession();
		$data_produk = $this->sessions['produk'];
		$data_detail_produk = $this->sessions['produk_detail'];
		$pekerjaan = $data_detail_produk['pekerjaan'];
		$bbaku = $data_detail_produk['bbaku'];
		$bop = $data_detail_produk['bop'];
		$where = array(
					'kd_produk'	=> $data_produk['kd_produk']
				);
		$this->m_produk->delete_bbaku($where);
		$this->m_produk->delete_pekerjaan($where);
		$this->m_produk->delete_bop($where);
		$data = $pekerjaan['data'];
		for($i = 0 ; $i < $pekerjaan['last']; $i++) {
			$field = array(
						'kd_produk'		=> $data_produk['kd_produk'],
						'kd_pekerjaan'	=> $data[$i]['kd_pekerjaan'],
						'jumlah_hari'	=> $data[$i]['jml_hari']
					);
			if(!$this->m_produk->add_pekerjaan($field)){
				echo "<script>alert('Tambah Produk Gagal ')</script>";
				echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
			}
		}
		$data = $bbaku['data'];
		for($i = 0 ; $i < $bbaku['last']; $i++) {
			$field = array(
						'kd_produk'	=> $data_produk['kd_produk'],
						'kd_bbaku'	=> $data[$i]['kd_bbaku'],
						'qty'		=> $data[$i]['qty']
					);
			if(!$this->m_produk->add_bbaku($field)){
				echo "<script>alert('Tambah Produk Gagal !')</script>";
				echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
			}
		}
		$data = $bop['data'];
		for($i = 0 ; $i < $bop['last']; $i++) {
			$field = array(
						'kd_produk'	=> $data_produk['kd_produk'],
						'kd_bop'	=> $data[$i]['kd_bop'],
						'qty'		=> $data[$i]['kebutuhan'],
						'nominal'	=> $data[$i]['nominal']
					);
			if(!$this->m_produk->add_bop($field)){
				echo "<script>alert('Tambah Produk Gagal !')</script>";
				echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
			}
		}

		$this->sessions['produk_detail'] = FALSE;
		$this->sessions['produk'] = FALSE;
		$this->saveSession();
		redirect('dashboard');
	}

	public function simpan_produk(){
		$this->getSession();
		$data_produk = $this->sessions['produk'];
		$data_detail_produk = $this->sessions['produk_detail'];
		$pekerjaan = $data_detail_produk['pekerjaan'];
		$bbaku = $data_detail_produk['bbaku'];
		$bop = $data_detail_produk['bop'];
		$field = array(
					'kd_produk'	=> $data_produk['kd_produk'],
					'nm_produk'	=> $data_produk['nm_produk'],
					'harga'		=> $data_produk['harga']
				);
		if($this->m_produk->add_produk($field)){
			foreach ($pekerjaan['data'] as $data) {
				$field = array(
							'kd_produk'		=> $data_produk['kd_produk'],
							'kd_pekerjaan'	=> $data['kd_pekerjaan'],
							'jumlah_hari'	=> $data['jml_hari']
						);
				if(!$this->m_produk->add_pekerjaan($field)){
					echo "<script>alert('Tambah Produk Gagal ')</script>";
					echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
				}
			}
			foreach ($bbaku['data'] as $data) {
				$field = array(
							'kd_produk'	=> $data_produk['kd_produk'],
							'kd_bbaku'	=> $data['kd_bbaku'],
							'qty'		=> $data['qty']
						);
				if(!$this->m_produk->add_bbaku($field)){
					echo "<script>alert('Tambah Produk Gagal !')</script>";
					echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
				}
			}
			foreach ($bop['data'] as $data) {
				$field = array(
							'kd_produk'	=> $data_produk['kd_produk'],
							'kd_bop'	=> $data['kd_bop'],
							'qty'		=> $data['kebutuhan'],
							'nominal'	=> $data['nominal']
						);
				if(!$this->m_produk->add_bop($field)){
					echo "<script>alert('Tambah Produk Gagal !')</script>";
					echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
				}
			}
		} else {
			echo "<script>alert('Tambah Produk Gagal !')</script>";
			echo "<meta http-equiv='refresh' content='0;url=".base_url()."index.php/dashboard/check_produk'>";
		}

		$this->sessions['produk_detail'] = FALSE;
		$this->sessions['produk'] = FALSE;
		$this->saveSession();
		redirect('dashboard');
	}

	public function getOneProduk($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $this->m_produk->getOne_produk($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function getOnePekerjaan($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $pekerjaan = $this->m_produk->getOne_pekerjaan($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function getOneBOP($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $pekerjaan = $this->m_produk->getOne_BOP($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function getOneBbaku($id){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);

		$data = $pekerjaan = $this->m_produk->getOne_bbaku($id);
			
		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data[0],
				'status'	=> true
			);
		}

		$this->outputJson($json);

	}

	public function logout(){
		$this->sessions = FALSE;
		$this->saveSession();
		$this->session->sess_destroy();
		redirect('dashboard','refresh');
	}
	
}
