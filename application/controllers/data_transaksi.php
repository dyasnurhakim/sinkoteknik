<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Data_transaksi extends Main {
	function __construct(){
		parent::__construct();
	}

	function index(){
			$data['active_menu'] = "";
			$data['data_transaksi'] = $this->m_datatransaksi->data_transaksi();
			$this->global_data = $data;
	        $this->tampilan('admin/daftar_transaksi');
		
	}
	public function trx_delete($id){
			$this->m_datatransaksi->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}
	function detail_trx	($id){
			$data['active_menu'] = "";
			$data['data_transaksi'] = $this->m_datatransaksi->datas_transaksi($id);
			$data['trx_pekerjaan'] = $this->m_datatransaksi->trx_pekerjaan($id);
			$data['trx_bahanbaku'] = $this->m_datatransaksi->trx_bahanbaku($id);
			$data['trx_biayaoh'] = $this->m_datatransaksi->trx_biayaoh($id);
			$data['detail'] = $this->m_datatransaksi->data_transaksi_detail($id);
			$this->global_data = $data;
			$this->tampilan("admin/detail_transaksi");
		
	}

}
