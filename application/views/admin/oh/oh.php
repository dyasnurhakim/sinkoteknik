
    <button class="btn btn-success" onclick="add_oh()"></i> Tambah Biaya OH</button>
    <br />
    <br />
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Aktivitas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
                    <th>NO</th>
                    <th>KODE AKTIVITAS</th>
                    <th>NAMA AKTIVITAS</th>
                    <th>SATUAN</th>
                    <th style="width:125px;">ACTION</p></th>
        </tr>
      </thead>
      <tbody>
                <?php 
                $i=1;
                foreach($oh as $oh){?>
                     <tr>
                         <td><?php echo $i++;?></td>
                         <td><?php echo $oh->kd_bop;?></td>
                         <td><?php echo $oh->nm_bop;?></td>
                         <td><?php echo $oh->satuan_bop;?></td>
                        <td>
                            <button class="btn btn-warning" onclick="edit_oh('<?php echo $oh->kd_bop;?>')"><i class="glyphicon glyphicon-pencil"></i></button>
                        </td>
                      </tr>
                     <?php }?>
 
 
 
      </tbody>
    </table>
 </div>
  </div>
  
<script src="<?php echo base_url('assets/jquery/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>

  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable({
        "aLengthMenu": [[5, 10, 20, 25, -1], [5, 10, 20, 25, "All"]],
        "iDisplayLength": 5
      });
      //datepicker
    // $('.datepicker').datepicker({
    //     autoclose: true,
    //     format: "yyyy-mm-dd",
    //     todayHighlight: true,
    //     orientation: "top auto",
    //     todayBtn: true,
    //     todayHighlight: true,  
    // });
  } );
    var save_method; //for save method string
    var table;
 
 
    function add_oh()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    function edit_oh(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('biaya_oh/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="kd_bop"]').val(data.kd_bop);
            $('[name="nm_bop"]').val(data.nm_bop);
            $('[name="satuan_bop"]').val(data.satuan_bop);
            $('[name="kelompok_bop"]').val(data.kd_kelompok_bop);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit BOP'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
 
 
 
    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('biaya_oh/oh_add')?>";
      }
      else
      {
        url = "<?php echo site_url('biaya_oh/oh_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
 
    function delete_oh(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('biaya_oh/oh_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
      }
    }
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah BOP</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Kode Aktivitas</label>
              <div class="col-md-9">
               <input readonly="" name="kd_bop" placeholder="Kode Aktivitas" class="form-control" type="text" required="" value=<?php echo "$kodeunik"; ?>>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama Aktivitas</label>
              <div class="col-md-9">
                <input name="nm_bop" placeholder="Nama Aktivitas" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Satuan</label>
              <div class="col-md-9">
                <input name="satuan_bop" placeholder="Satuan" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Kode COA</label>
                  <div class="col-md-9">
                  <select name="kelompok_bop" class="form-control" id="kelompok_aktivitas">
                    <option value="">Pilih COA</option>
                    <?php foreach ($coa as $row): ?>
                    <option value="<?php echo $row->id?>"><?php echo $row->nama_akun?></option>
                    <?php endforeach ?>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Kelompok Aktivitas</label>
                  <div class="col-md-9">
                  <select name="kelompok_bop" class="form-control" id="kelompok_aktivitas">
                    <option value="">Pilih Kelompok Aktivitas</option>
                    <?php foreach ($kelompok_bop as $row): ?>
                    <option value="<?php echo $row->kd_kelompok_bop?>"><?php echo $row->nm_kelompok_bop?></option>
                    <?php endforeach ?>
                  </select>
                  <span style="color: RED;">*Kosongkan jika tidak termasuk Kelompok Aktivitas</span>
                </div>
            </div>
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    