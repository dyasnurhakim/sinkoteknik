
    <button class="btn btn-success" onclick="add()"> Tambah Supplier</button>
    <br />
    <br />
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Supplier</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
                    <th>NO</th>
                    <th>ID</th>
                    <th>NAMA</th>
                    <th>ALAMAT</th>
                    <th>EMAIL</th>
                    <th>TELP</th>
                    <th style="width:125px;">ACTION
                    </p></th>
        </tr>
      </thead>
      <tbody>
                <?php $i=1;
                foreach($supplier as $row){?>
                    <tr>
                      <td><?php echo $i++;?></td>
                      <td><?php echo $row->kd_supplier;?></td>
                      <td><?php echo $row->nm_supplier;?></td>
                      <td><?php echo $row->alamat_supplier;?></td>
                      <td><?php echo $row->email_supplier;?></td>
                      <td><?php echo $row->tlp_supplier;?></td>
                      <td>
                        <button class="btn btn-warning" onclick="edit_supplier('<?php echo $row->kd_supplier;?>')"><i class="glyphicon glyphicon-pencil"></i></button>
                      </td>
                    </tr>
                <?php }?>
 
 
 
      </tbody>
    </table>
 </div>
  </div>
  
<script src="<?php echo base_url('assets/jquery/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>

  <script type="text/javascript">
    $(document).ready( function () {
      $('#table_id').DataTable({
        "aLengthMenu": [[5, 10, 20, 25, -1], [5, 10, 20, 25, "All"]],
        "iDisplayLength": 5
      });
    });

    var save_method; //for save method string
    var table;
 
 
    function add(){
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Supplier'); // Set Title to Bootstrap modal title
    }
 
    function edit_supplier(id){
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('supplier/edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(response){
 
            $('[name="kd_supplier"]').val(response.data.kd_supplier);
            $('[name="nm_supplier"]').val(response.data.nm_supplier);
            $('[name="alamat_supplier"]').val(response.data.alamat_supplier);
            $('[name="email_supplier"]').val(response.data.email_suppliar);
            $('[name="tlp_supplier"]').val(response.data.tlp_supplier);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Supplier'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
      });
    }
 
 
 
    function save(){
      var url;
      if(save_method == 'add'){
          url = "<?php echo site_url('supplier/add')?>";
      } else {
          url = "<?php echo site_url('supplier/update')?>";
      }
 
       // ajax adding data to database
      $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
           //if success close modal and reload ajax table
           $('#modal_form').modal('hide');
          location.reload();// for reload a page
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error adding / update data');
        }
      });
    }
 
    function delete_supplier(id){
      if(confirm('Are you sure delete this data?')){
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('supplier/delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data){
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error deleting data');
            }
          });
 
      }
    }
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Supplier</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">ID Supplier</label>
              <div class="col-md-9">
               <input readonly="" name="kd_supplier" placeholder="Id Supplier" class="form-control" type="text" required="" value=<?php echo "$kodeunik"; ?>>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama Supplier</label>
              <div class="col-md-9">
                <input name="nm_supplier" placeholder="Nama Supplier" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Alamat Supplier</label>
              <div class="col-md-9">
                <textarea class="form-control" name="alamat_supplier">
                  
                </textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Email Supplier</label>
              <div class="col-md-9">
                <input name="email_supplier" placeholder="Email Supplier" class="form-control" type="email" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Telp Produk</label>
              <div class="col-md-9">
                <input name="tlp_supplier" placeholder="Telp Supplier" class="form-control" type="text" required="">
              </div>
            </div>
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    
