
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>Laporan Biaya Produksi
            <small class="pull-right">Tanggal <?php echo $tgl_awal;?> s/d <?php echo $tgl_akhir;?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-header -->
    <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12" style="text-align: center">
              <H2>PD. Sinko Teknik Bandung</H2>
              <H3>Laporan Produksi</H3>
              <H3>per <?php $date = date("j M Y", strtotime($tgl_akhir));echo $date;?></H3>
            </div>
          </div>
          
        </div>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered" >
				      <tr>
                  <th rowspan="2" colspan="2"></th>
                  <th colspan="<?php echo count($listproduk);?>" style="text-align: center;">Produk</th>    
              </tr>
              <tr>
                <?php foreach($listproduk as $produk) {?>
                <td style="text-align: center;"><?php echo $produk['nm_produk'];?></td>
                <?php }?>
              </tr>
              <tr>
                <td colspan="2"><b>Total Unit</b></td>
                <?php foreach($listproduk as $produk) {?>
                <td style="text-align: center;"><?php echo number_format($total_unit[$produk['kd_produk']]);?> Unit</td>
                <?php }?>
              </tr>
              <tr>
                <td colspan="2"><b>Biaya Bahan Baku</b></td>
                <?php ;foreach($listproduk as $produk) {?>
                <td style="text-align: center;">Rp <?php $total[$produk['kd_produk']] = $total_bbaku[$produk['kd_produk']]; echo number_format($total_bbaku[$produk['kd_produk']]);?>,-</td>
                <?php }?>
              </tr>
              <tr>
                <td colspan="2"><b>Pekerjaan</b></td>
                <td colspan="<?php echo count($listproduk);?>"></td>
              </tr>
              <?php foreach($listpekerjaan as $pekerjaan){ ?>
              <tr>
                <td style="width: 30px">&nbsp;</td>
                <td><?php echo $pekerjaan->nama_pekerjaan;?></td>
                <?php foreach($listproduk as $produk) {?>
                <td style="text-align: center;">Rp <?php $total[$produk['kd_produk']] += $total_btkl[$produk['kd_produk']][$pekerjaan->id]; echo number_format($total_btkl[$produk['kd_produk']][$pekerjaan->id]);?>,-</td>
                <?php }?>
              </tr>
              <?php }?>
             	<tr>
              <tr>
                <td colspan="2"><b>Biaya OH</b></td>
                <td colspan="<?php echo count($listproduk);?>"></td>
              </tr>
              <?php foreach($listbop as $bop){ ?>
              <tr>
                <td style="width: 30px">&nbsp;</td>
                <td><?php echo $bop->nm_bop;?></td>
                <?php foreach($listproduk as $produk) {?>
                <td style="text-align: center;">Rp <?php $total[$produk['kd_produk']] += $total_bop[$produk['kd_produk']][$bop->kd_bop] * $total_unit[$produk['kd_produk']];echo number_format($total_bop[$produk['kd_produk']][$bop->kd_bop] * $total_unit[$produk['kd_produk']]);?>,-</td>
                <?php }?>
              </tr>
              <?php }?>
              <tr>
                <td colspan="2"><b>Subtotal Biaya Produksi</b></td>
                <?php $totalproduksi = 0;foreach($listproduk as $produk) {?>
                <td style="text-align: center;">Rp <?php $totalproduksi += $total[$produk['kd_produk']]; echo number_format($total[$produk['kd_produk']]);?>,-</td>
                <?php }?>
              </tr>
              <tr>
                <td colspan="2"><b>Total Biaya Produksi</b></td>
                 <td colspan="<?php echo count($listproduk);?>" style="text-align: center;">Rp <?php echo number_format($totalproduksi);?>,-</td>
              </tr>
              
				    </table>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->

</section>