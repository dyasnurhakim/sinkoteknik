
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>Jurnal PD. Sinko Teknik Bandung
            <small class="pull-right">Tanggal <?php echo $tgl_awal;?> s/d <?php echo $tgl_akhir;?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-header -->

      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12" style="text-align: center">
              <H2>PD. Sinko Teknik Bandung</H2>
              <H3>Laporan Jurnal</H3>
              <H3>per <?php $date = date("j M Y", strtotime($tgl_akhir));echo $date;?></H3>
            </div>
          </div>
          
        </div>
          
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered" >
				      <tr>
                <th>Tanggal</th>
                <th colspan="2">Nama Akun</th>
                <th>No. Akun</th>
                <th>Debit</th>
                <th>Kredit</th>
              </tr>
              <?php
                    foreach($data as $row){
                      if($row->type == 'Kredit' || $row->type == 'Debit'){
                        
              ?>

              <tr>
                <td><?php echo $row->tgl_jurnal;?></td>
                <td><?php echo ($row->type == "Debit") ? $row->nama_akun : "";?></td>
                <td><?php echo ($row->type == "Kredit") ? $row->nama_akun : "";?></td>
                <td><?php echo $row->kd_akun;?></td>
                <td><?php echo ($row->type == "Debit") ? "Rp ".number_format($row->nominal).",-" : "";?></td>
                <td><?php echo ($row->type == "Kredit") ? "Rp ".number_format($row->nominal).",-" : "";?></td>
              </tr>
              <?php 
                      }
                    }
              ?>
                      
              
				    </table>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->

</section>