<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Jurnal</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('jurnal/get_jurnal');?>" method="POST">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                <label for="id">Tanggal Awal : </label>
                <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-autoclose="true"  data-date-orientation="bottom" >
                    <input type="text" class="form-control" name="start_date">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="id">Tanggal Akhir : </label>
                <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-orientation="bottom" >
                    <input type="text" class="form-control" name="finish_date">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
          </div>
        </div>
      <input type="submit" class="btn btn-primary" name="Finish" value="Submit">
    </form>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
      Master Data PD. Sinko Teknik
    </div> -->
</div>
<script type="text/javascript">


  function convertToRupiah(angka){

      var rupiah = '';    
      var angkarev = angka.toString().split('').reverse().join('');
      
      for(var i = 0; i < angkarev.length; i++) 
        if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      
      return rupiah.split('',rupiah.length-1).reverse().join('');

  }
</script>
