<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> PD.Sinko Teknik / Produk
            <small class="pull-right"><b> Tanggal Transaksi : <?php echo $tgl_transaksi ?> </b></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
            <div class="row">
            <div class="col-xs-7 ">
              <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th>Kode Pengeluaran</th>
                        <td><?php echo $kodeunik ?></td>
                      </tr>
                      <tr>
                        <th>Keterangan</th>
                        <td><?php echo $detail_pengeluaran['keterangan'] ?></td>
                      </tr>
                    </tbody>
                  </table>
              </div>
            </div>
              <div class="col-md-12">
              <legend>Detail Transaksi Pengeluaran</legend>
                    <table class="table">
                      <thead>
                        <tr>
                            <th>NO</th>
                            <th>ID AKTIVITAS</th>
                            <th>NAMA AKTIVITAS</th>
                            <th>SATUAN</th>
                            <th>KEBUTUHAN</th>
                            <th>NOMINAL</th>
                            <th>SUBTOTAL</th>
                        </tr>
                      </thead>
                      <tbody>
                            <?php
                            if(!empty($detail_pengeluaran)){
                              $total = 0;
                              $data = $detail_pengeluaran['data'];
                              for($i = 0 ; $i < $detail_pengeluaran['last']; $i++){?>
                              <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $data[$i]['kd_bop'];?></td>
                                <td><?php echo $data[$i]['nm_bop'];?></td>
                                <td><?php echo $data[$i]['satuan_bop'];?></td>
                                <td><?php echo $data[$i]['kebutuhan'];?></td>
                                <td>Rp. <?php echo number_format($data[$i]['nominal']);?>,00</td>
                                <td>Rp. <?php echo number_format($data[$i]['kebutuhan'] * $data[$i]['nominal']);?>,00</td> 
                              </tr>
                             <?php } ?>
                             <tr>
                              <td colspan="5"></td>
                                <td><h4><b>Total :</b></h4></td>
                                <td><h4><b>Rp. <?php echo number_format($detail_pengeluaran['total']);?>,00</b></h4></td>
                             </tr>
                             <?php
                              }
                             ?>

                      </tbody>
                    </table>
              </div>
                <!-- /.col -->
            </div>
      <!-- info row -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="<?php echo base_url('trx_pengeluaran/simpan_pengeluaran');?>"><button type="button" class="btn btn-success pull-right"><i class="fa fa-check"></i> Simpan </button></a>
          <a href="<?php echo base_url('trx_pengeluaran');?>">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              Kembali
            </button>
          </a>
          
        </div>
      </div>
    </section>

<script type="text/javascript">

  function subTotal(){

    var harga = $('#harga').val().replace(/Rp|,|-/g, "").replace(".","");
    var qty = $('#qty').val();
    console.log(harga);

    $('#sub_total').val(convertToRupiah(harga * qty));
  }

  function convertToRupiah(angka){

      var rupiah = '';    
      var angkarev = angka.toString().split('').reverse().join('');
      
      for(var i = 0; i < angkarev.length; i++) 
        if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
      
      return "Rp. " + rupiah.split('',rupiah.length-1).reverse().join('') + ",-";

  }
</script>
