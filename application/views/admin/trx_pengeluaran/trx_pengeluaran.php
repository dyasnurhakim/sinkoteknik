<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Transaksi Pengeluaran</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('trx_pengeluaran/tambah_pengeluaran');?>" method="POST">
        <div class="row">
        	<div class="col-md-6">
			    <div class="form-group">
			      <label for="id">Aktivitas :</label>
			        <input list="list_bop" class="form-control reset" 
			        	placeholder="Isi id..." name="kd_bop" id="id" 
			        	autocomplete="off" required="" onblur="showBOP(this.value)" onchange="showBOP(this.value)" onkeyup="showBOP(this.value)">
	                  <datalist id="list_bop" name="kd_bop">
	                  	<?php foreach ($bop as $row): ?>
	                  		<option value="<?= $row->kd_bop ?>"><?= $row->nm_bop ?></option>
	                  	<?php endforeach ?>
	                  </datalist>
			    </div>
			   <div class="form-group">
            		<label for="harga_produk">Satuan </label>
            		<input type="text" class="form-control" id="satuan" min="0" name="sataun" id="satuan" readonly="true" >
            	</div>
            	<div class="form-group">
            		<label for="qty">Kebutuhan </label>
            		<input type="number" class="form-control reset" autocomplete="off" id="kebutuhan" min="0" name="kebutuhan" placeholder="Isi qty..." onchange="subTotal()" onkeyup="subTotal()">
            	</div>
            	<div class="form-group">
            		<label for="qty">Nominal </label>
            		<input type="number" class="form-control reset" autocomplete="off" id="nominal" min="0" name="nominal" placeholder="Isi Nominal..." onchange="subTotal()" onkeyup="subTotal()">
            	</div>
            	<div class="form-group">
				     	<label for="sub_total">Keterangan :</label>
				        <textarea name="keterangan" class="form-control" <?php echo (!empty($detail_pengeluaran)) ? "readonly" : "";?>><?php echo (!empty($detail_pengeluaran)) ? $detail_pengeluaran['keterangan'] : ""; ?></textarea>
			    </div>
            </div>
            <div class="col-md-4">
            	<div class="form-group">
                		<label for="kode_transaksi">Kode Pengeluaran</label>
                		<input type="text" class="form-control reset" id="kode_transaksi" readonly="" name="kode_transaksi" value="<?php echo (!empty($detail_pengeluaran)) ? $detail_pengeluaran['kodeunik'] : $kodeunik; ?>" >
                </div>
                <div class="form-group">
                		<label for="tgl_transaksi">Tanggal</label>
                		<input type="text" class="form-control reset" id="tgl_transaksi" readonly="" name="tgl_transaksi" value="<?php echo date('Y-m-d'); ?>" >
                </div>
                <div class="form-group">
				     	<label for="sub_total">Sub-Total (Rp):</label>
				        <input type="text" class="form-control reset" name="sub_total" id="sub_total" readonly="readonly">
			    </div>
            </div>
            <!-- /.col -->
        </div>
		<input type="submit" class="btn btn-success" name="Finish" value="Tambah">
    </form>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div>
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Detail Transaksi Pengeluaran</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
        <div class="row">
        	<div class="col-md-12">
              	<table id="table_id" class="table table-striped table-bordered">
				      <thead>
				        <tr>
		                    <th>NO</th>
		                    <th>ID AKTIVITAS</th>
		                    <th>NAMA AKTIVITAS</th>
		                    <th>SATUAN</th>
		                    <th>KEBUTUHAN</th>
		                    <th>NOMINAL</th>
		                    <th>SUBTOTAL</th>
		                    <th style="width:125px;">ACTION</p></th>
				        </tr>
				      </thead>
				      <tbody>
		                <?php
		                if(!empty($detail_pengeluaran)){
		                	$data = $detail_pengeluaran['data'];
			                for($i = 0 ; $i < $detail_pengeluaran['last']; $i++){?>
	                    <tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]['kd_bop'];?></td>
							<td><?php echo $data[$i]['nm_bop'];?></td>
							<td><?php echo $data[$i]['satuan_bop'];?></td>
							<td><?php echo $data[$i]['kebutuhan'];?></td>
							<td><?php echo $data[$i]['nominal'];?></td>
							<td><?php echo ($data[$i]['kebutuhan'] * $data[$i]['nominal']);?></td>
                            <td>
                                <a href="<?php echo base_url('trx_pengeluaran/remove_bop').'/'.$i;?>"><button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></a>
                            </td>
	                    </tr>
                     <?php } ?>
                     	<tr>
                     		<td colspan="8">
                     			<a href="<?php echo base_url('trx_pengeluaran/check_pengeluaran');?>"><button class="btn btn-warning">Finish</button></a>
                     			<a href="<?php echo base_url('trx_pengeluaran/cancel_pengeluaran');?>"><button class="btn btn-danger">Cancel</button></a>
                     		</td>
                     	</tr>
                     <?php
                     	}
                     ?>

				      </tbody>
				    </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div>
</div>
<script type="text/javascript">

	function subTotal(){

		var harga = $('#kebutuhan').val().replace(/Rp|,|-/g, "").replace(".","");
		var qty = $('#nominal').val();
		console.log(harga);

		$('#sub_total').val(convertToRupiah(harga * qty));
	}

	function convertToRupiah(angka){

	    var rupiah = '';    
	    var angkarev = angka.toString().split('').reverse().join('');
	    
	    for(var i = 0; i < angkarev.length; i++) 
	      if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
	    
	    return "Rp. " + rupiah.split('',rupiah.length-1).reverse().join('') + ",-";

	}

	function showBOP(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('trx_pengeluaran/getOneBOP')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				$("#satuan").val(res.data.satuan_bop);
			}
		});
	}
</script>
