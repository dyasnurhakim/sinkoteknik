<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Produk</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('dashboard/add_produk');?>" method="POST">
        <div class="row">
        	<div class="col-md-6">
				<div class="form-group">
            		<label for="nm_produk">Nama Produk </label>
            		<input type="text" class="form-control" id="nm_produk" placeholder="Nama Produk" name="nm_produk" <?php echo (empty($produk)) ? '' : 'readonly value="'.$produk['nm_produk'].'"';?> required>
            	</div>
				<div class="form-group">
            		<label for="harga_produk">Harga Jual</label>
            		<input type="text" class="form-control" autocomplete="off" id="harga_produk" min="0" name="harga_produk" <?php echo (empty($produk)) ? '' : 'readonly value="Rp. '.number_format($produk['harga']).',-"';?> required>
            	</div>
            </div>
            <div class="col-md-4">
            	<div class="form-group">
                		<label for="kode_transaksi">Kode Produk</label>
                		<input type="text" class="form-control" id="kode_transaksi" readonly="" name="kd_produk" value="<?php echo (!empty($produk)) ? $produk['kd_produk'] : $kodeunik; ?>">
                </div>     
                <div class="form-group">
                		<label for="tgl_transaksi">Tanggal</label>
                		<input type="text" class="form-control" id="tgl_transaksi" readonly="" name="tgl_transaksi" value="<?php echo date('Y-m-d'); ?>" >
                </div>
            </div>
            <!-- /.col -->
        </div>
        <?php
        	if(empty($produk)){
        ?>
		<input type="submit" class="btn btn-success" name="Finish" value="Tambah Produk">
    </form>
    	<?php
        	} else {
        ?>
  	</form>
  		<a href="<?php echo base_url('dashboard/cancel_tambah_produk');?>"><button class="btn btn-danger"><?php echo ($state == 'add') ? "Cancel Tambah Produk" : "Cancel Edit Produk"?></button></a>
        <?php
        	}
    	?>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>

<?php if(!empty($produk)){ ?>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Pekerjaan</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('dashboard/tambah_pekerjaan');?>" method="POST">
        <div class="row">
        	<div class="col-md-12">
              	<table class="table">
			                <tbody>
					        <tr>
					            <td>
					            	<label for="id">Id Pekerjaan :</label>
							        <input list="list_pekerjaan" class="form-control reset" 
							        	placeholder="Isi id..." name="id_pekerjaan" id="id" 
							        	autocomplete="off" required="" onblur="showPekerjaan(this.value)" onchange="showPekerjaan(this.value)" onkeyup="showPekerjaan(this.value)">
					                  <datalist id="list_pekerjaan" name="id_pekerjaan">
					                  	<?php foreach ($pekerjaan as $row): ?>
					                  		<option value="<?= $row->id ?>"><?= $row->nama_pekerjaan ?></option>
					                  	<?php endforeach ?>
					                  </datalist>
					            </td>
					            <td>
					            	<label for="id">Nama Pekerjaan :</label>
					            	<input class="form-control" value="" readonly="" id="nm_pekerjaan">
					            </td>					            
					            <td>
					            	<label for="id">Jumlah dibutuhkan :</label>
					            	<input class="form-control" name="jml_hari" placeholder="Jumlah Hari" type="number" required>
					            </td>					           

					        </tr>
					        <tr>
					        	<td colspan="2"><input type="hidden" name="state" value="<?php echo $state;?>"></td>
					        	<td align="right">
					        		<input type="submit" class="btn btn-success" name="Finish" value="Tambah Pekerjaan">
					        	</td>
					        </tr>
			              </tbody>

			    </table>
            </div>
        </div>
           
        </div>
        <!-- /.row -->
    </form>
    <!-- /.box-body -->
   <!--  <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Bahan Baku</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('dashboard/tambah_bbaku');?>" method="POST">
        <div class="row">
        	<div class="col-md-12">
              	<table class="table">
			                <tbody>
					        <tr>
					            <td>
					            	<label for="id">Id Bahan Baku :</label>
							        <input list="list_bbaku" class="form-control reset" 
							        	placeholder="Isi id..." name="id_bbaku" id="id" 
							        	autocomplete="off" required="" onblur="showBbaku(this.value)" onchange="showBbaku(this.value)" onkeyup="showBbaku(this.value)">
					                  <datalist id="list_bbaku" name="id_bbaku">
					                  	<?php foreach ($bbaku as $row): ?>
					                  		<option value="<?= $row->id ?>"><?= $row->nama_bb ?></option>
					                  	<?php endforeach ?>
					                  </datalist>
					            </td>
					            <td>
					            	<label for="id">Nama Bahan Baku :</label>
					            	<input class="form-control" name="nama_bb" value="" readonly="" id="nama_bb">
					            </td>
					            <td>
					            	<label for="id">Satuan Bahan Baku :</label>
					            	<input class="form-control" name="satuan_bbaku" value="" readonly="" id="satuan_bbaku">
					            </td>
					            <td>
					            	<label for="id">Jumlah dibutuhkan :</label>
					            	<input class="form-control" name="qty" placeholder="Jumlah" id="qty" required>
					            </td>


					        </tr>
					        <tr>
					        	<td colspan="2"><input type="hidden" name="state" value="<?php echo $state;?>"></td>
					        	<td align="right">
					        		<input type="submit" class="btn btn-success" name="Finish" value="Tambah Bahan Baku">
					        	</td>
					        </tr>
			              </tbody>

			    </table>
            </div>
        </div>
           
        </div>
        <!-- /.row -->
    </form>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Biaya Overhead</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('dashboard/tambah_bop');?>" method="POST">
        <div class="row">
        	<div class="col-md-12">
              	<table class="table">
			                <tbody>
					        <tr>
					            <td>
					            	<label for="id">Aktivitas :</label>
							        <input list="list_bop" class="form-control reset" 
							        	placeholder="Isi id..." name="kd_bop" id="id" 
							        	autocomplete="off" required="" onblur="showBOP(this.value)" onchange="showBOP(this.value)" onkeyup="showBOP(this.value)">
					                  <datalist id="list_bop" name="kd_bop">
					                  	<?php foreach ($bop as $row): ?>
					                  		<option value="<?= $row->kd_bop ?>"><?= $row->nm_bop ?></option>
					                  	<?php endforeach ?>
					                  </datalist>
					            </td>
					            <td>
					            	<label for="id">Nama Aktivitas :</label>
					            	<input class="form-control" name="nm_bop" id="nm_bop" value="" readonly="">
					            </td>			            
					            <td>
					            	<label for="id">Satuan : </label>
					            	<input class="form-control" name="satuan" id="satuan" value="" readonly="">
					            </td>					            
					            <td>
					            	<label for="id">Kebutuhan :</label>
					            	<input type="number" class="form-control" name="kebutuhan" placeholder="Kebutuhan" required>
					            </td>					           
					            <td>
					            	<label for="id">Nominal :</label>
					            	<input type="number" class="form-control" name="nominal" placeholder="Nominal" required>
					            </td>					           
					        </tr>
					        <tr>
					        	<td colspan="4"><input type="hidden" name="state" value="<?php echo $state;?>"></td>
					        	<td align="right">
					        		<input type="submit" class="btn btn-success" name="Finish" value="Tambah Biaya Overhead">
					        	</td>
					        </tr>
			              </tbody>

			    </table>
            </div>
        </div>
           
        </div>
        <!-- /.row -->
    </form>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Detail Pembelian</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
        <div class="row">
        	<div class="col-md-12">
        		<h4>Pekerjaan</h4>
              	<table id="table_id" class="table table-striped table-bordered">
				      <thead>
				        <tr>
		                    <th>NO</th>
		                    <th>ID PEKERJAAN</th>
		                    <th>NAMA PEKERJAAN</th>
		                    <th>JUMLAH HARI</th>
		                    <th style="width:125px;">ACTION</th>
				        </tr>
				      </thead>
				      <tbody>
		                <?php
		                if(!empty($produk_detail['pekerjaan'])){
		                	$pekerjaan = $produk_detail['pekerjaan'];
		                	$data = $pekerjaan['data'];
			                for($i = 0 ; $i < $pekerjaan['last']; $i++){?>
	                    <tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]['kd_pekerjaan'];?></td>
							<td><?php echo $data[$i]['nm_pekerjaan'];?></td>
							<td><?php echo $data[$i]['jml_hari'];?></td>
                            <td>
                                <a href="<?php echo base_url('dashboard/remove_pekerjaan').'/'.$state.'/'.$i;?>"><button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></a>
                            </td>
	                    </tr>
                     <?php } 
                     	}
                     ?>

				      </tbody>
				    </table>
            </div>
            <div class="col-md-12">
        		<h4>Bahan Baku</h4>
              	<table id="table_id" class="table table-striped table-bordered">
				      <thead>
				        <tr>
		                    <th>NO</th>
		                    <th>ID BAHAN BAKU</th>
		                    <th>NAMA BAHAN BAKU</th>
		                    <th>SATUAN BAHAN BAKU</th>
		                    <th>QTY</th>
		                    <th style="width:125px;">ACTION</th>
				        </tr>
				      </thead>
				      <tbody>
		                <?php
		                if(!empty($produk_detail['bbaku'])){
		                	$bbaku = $produk_detail['bbaku'];
		                	$data = $bbaku['data'];
			                for($i = 0 ; $i < $bbaku['last']; $i++){?>
	                    <tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]['kd_bbaku'];?></td>
							<td><?php echo $data[$i]['nm_bbaku'];?></td>
							<td><?php echo $data[$i]['satuan'];?></td>
							<td><?php echo $data[$i]['qty'];?></td>
                            <td>
                                <a href="<?php echo base_url('dashboard/remove_bbaku').'/'.$state.'/'.$i;?>"><button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></a>
                            </td>
	                    </tr>
                     <?php }
                     	}
                     ?>

				      </tbody>
				    </table>
            </div>
            <div class="col-md-12">
        		<h4>Biaya Overhead</h4>
              	<table id="table_id" class="table table-striped table-bordered">
				      <thead>
				        <tr>
		                    <th>NO</th>
		                    <th>ID AKTIVITAS</th>
		                    <th>NAMA AKTIVITAS</th>
		                    <th>SATUAN</th>
		                    <th>KEBUTUHAN</th>
		                    <th>NOMINAL</th>
		                    <th>SUBTOTAL</th>
		                    <th style="width:125px;">ACTION</th>
				        </tr>
				      </thead>
				      <tbody>
		                <?php
		                if(!empty($produk_detail['bop'])){
		                	$biaya_oh = $produk_detail['bop'];
		                	$data = $biaya_oh['data'];
			                for($i = 0 ; $i < $biaya_oh['last']; $i++){?>
	                    <tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]['kd_bop'];?></td>
							<td><?php echo $data[$i]['nm_bop'];?></td>
							<td><?php echo $data[$i]['satuan'];?></td>
							<td><?php echo $data[$i]['kebutuhan'];?></td>
							<td><?php echo $data[$i]['nominal'];?></td>
							<td><?php echo ($data[$i]['kebutuhan'] * $data[$i]['nominal']);?></td>
                            <td>
                                <a href="<?php echo base_url('dashboard/remove_bop').'/'.$state.'/'.$i;?>"><button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></a>
                            </td>
	                    </tr>
                     <?php }
                 		}

                     	if(!empty($produk_detail)){
                      ?>
                     	<tr>
                     		<td colspan="8">
                     			<a href="<?php echo base_url('dashboard/check_produk')."/".$state;?>"><button class="btn btn-warning">Finish</button></a>
                     			<?php if($state =='add') {?>
                     				<a href="<?php echo base_url('dashboard/cancel_detail_produk');?>"><button class="btn btn-danger">Cancel</button></a>
                     			<?php }?>
                     		</td>
                     	</tr>
                     <?php
                     	}
                     ?>

				      </tbody>
				    </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<?php }?>
<script type="text/javascript">

	function subTotal(){

		var harga = $('#harga_produk').val().replace(".", "").replace(".", "");
		var qty = $('#qty').val();

		$('#sub_total').val(convertToRupiah(harga * qty));
	}

	function convertToRupiah(angka){

	    var rupiah = '';    
	    var angkarev = angka.toString().split('').reverse().join('');
	    
	    for(var i = 0; i < angkarev.length; i++) 
	      if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	    
	    return rupiah.split('',rupiah.length-1).reverse().join('');

	}

	function showCustomer(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('dashboard/getOneCustomer')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				$("#nama_pelanggan").val(res.data.nama_pelanggan);
				$("#id_pelanggan").val(res.data.kd_pelanggan);
				showProduk(id);
			}
		});
	}

	function showProduk(obj){
		var id = obj;
		$.ajax({
			url:"<?php echo site_url('dashboard/getProdukCustomer')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				 $("#produk").html(res.data);
			}
		});
	}

	function showPekerjaan(obj){
		var id = obj;
		$.ajax({
			url:"<?php echo site_url('dashboard/getOnePekerjaan')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				 $("#nm_pekerjaan").val(res.data.nama_pekerjaan);
			}
		});
	}

	function showBbaku(obj){
		var id = obj;
		$.ajax({
			url:"<?php echo site_url('dashboard/getOneBbaku')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				 $("#nama_bb").val(res.data.nama_bb);
				 $("#harga_bbaku").val(res.data.harga);
				 $("#satuan_bbaku").val(res.data.satuan);
			}
		});
	}

	function showDetailProduk(obj){
		var produk = obj;
		var pesanan = $("#id_pesanan").val();
		$.ajax({
			url:"<?php echo site_url('dashboard/getOneDetailPesanan')?>/"+pesanan+"/"+produk,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				 $("#harga_produk").val(convertToRupiah(res.data.harga_prd));
				 $("#qty").val(res.data.qty_pesanan);
				 $("#nm_produk").val(res.data.nama_produk);
				 subTotal();
			}
		});
	}

	function showBOP(obj){
		var id = obj;
		$.ajax({
			url:"<?php echo site_url('dashboard/getOneBOP')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				 $("#nm_bop").val(res.data.nm_bop);
				 $("#satuan").val(res.data.satuan_bop);
			}
		});
	}
</script>


