
    <a href="<?php echo base_url('dashboard/tambah_produk');?>"><button class="btn btn-success"> Tambah produk</button></a>
    <br />
    <br />
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
                    <th>NO</th>
                    <th>ID</th>
                    <th>NAMA PRODUK</th>
                    <th>HARGA JUAL</th>
                    <th style="width:125px;">ACTION
                    </p></th>
        </tr>
      </thead>
      <tbody>
                <?php $i=1;
                foreach($produk as $produk){?>
                     <tr>
                         <td><?php echo $i++;?></td>
                         <td><?php echo $produk->kd_produk;?></td>
                         <td><?php echo $produk->nm_produk;?></td>
                         <td>Rp. <?php echo number_format($produk->harga);?>,-</td>
                          <td>
                          <button class="btn btn-warning" onclick="edit_produk('<?php echo $produk->kd_produk;?>')"><i class="glyphicon glyphicon-pencil"></i></button>
                          </td>
                      </tr>
                     <?php }?>
 
 
 
      </tbody>
    </table>
 </div>
  </div>
  
<script src="<?php echo base_url('assets/jquery/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>

  <script type="text/javascript">
    $(document).ready( function () {
      $('#table_id').DataTable({
        "aLengthMenu": [[5, 10, 20, 25, -1], [5, 10, 20, 25, "All"]],
        "iDisplayLength": 5
      });
    });
 
    function edit_produk(id){
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('dashboard/getOneproduk')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(response){
 
            $('[name="kd_produk"]').val(response.data.kd_produk);
            $('[name="nm_produk"]').val(response.data.nm_produk);
            $('[name="harga"]').val(response.data.harga);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Produk'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
      });
    }
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Produk</h3>
      </div>
      <div class="modal-body form">
        <form action="<?php echo base_url('dashboard/edit')?>" id="form" class="form-horizontal" method="post">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">ID Produk</label>
              <div class="col-md-9">
               <input readonly="" name="kd_produk" placeholder="Id Produk" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama Produk</label>
              <div class="col-md-9">
                <input name="nm_produk" placeholder="Nama Produk" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Harga Jual</label>
              <div class="col-md-9">
                <input name="harga" placeholder="Harga Produk" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Edit Detail Produk</label>
              <div class="col-md-9 checkbox">
                <label><input type="checkbox" name="detail_produk" value="true">Yes</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="btnSave" class="btn btn-primary">Save</button>
          <button type="submit" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
        </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
</div>
    