<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> PD.Sinko Teknik
            <small class="pull-right">Tanggal: <?php echo date('Y-m-d'); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="col-xs-7 ">
        <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <th>Kode Produk</th>
                  <td><?php echo $produk['kd_produk'] ?></td>
                </tr>
                <tr>
                  <th>Nama Produk</th>
                  <td><?php echo $produk['nm_produk'] ?></td>
                </tr>
                <tr>
                  <th>Harga Jual</th>
                  <td>Rp. <?php echo $produk['harga'] ?>,-</td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Pekerjaan</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Pekerjaan</th>
              <th>Nama Pekerjaan</th>
              <th>Jumlah Jam</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($produk_detail['pekerjaan'])){
              $pekerjaan = $produk_detail['pekerjaan'];
              $data = $pekerjaan['data'];
              for($i = 0 ; $i < $pekerjaan['last']; $i++): ?>
            <tr>
              <td><?php echo $i+1;?></td>
              <td><?php echo $data[$i]['kd_pekerjaan'];?></td>
              <td><?php echo $data[$i]['nm_pekerjaan'];?></td>
              <td><?php echo $data[$i]['jml_hari'];?></td>
            </tr>
            <?php endfor ?>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Bahan Baku</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Bahan Baku</th>
              <th>Nama Bahan Baku</th>
              <th>Satuan Bahan Baku</th>
              <th>Jumalah BB</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($produk_detail['bbaku'])){
              $bbaku = $produk_detail['bbaku'];
              $data = $bbaku['data'];
             for($i = 0 ; $i < $bbaku['last']; $i++): ?>
            <tr>
               <td><?php echo $i+1;?></td>
              <td><?php echo $data[$i]['kd_bbaku'];?></td>
              <td><?php echo $data[$i]['nm_bbaku'];?></td>
              <td><?php echo $data[$i]['satuan'];?></td>
              <td><?php echo $data[$i]['qty'];?></td>
            </tr>
            <?php endfor ?>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Kelompok Aktivitas</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Aktivitas</th>
              <th>Nama Aktivitas</th>
              <th>Kebutuhan Produk</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($produk_detail['bop'])){
              $bop = $produk_detail['bop'];
              $data = $bop['data'];
              for($i = 0 ; $i < $bop['last']; $i++): ?>
            <tr>
               <td><?php echo $i+1;?></td>
              <td><?php echo $data[$i]['kd_bop'];?></td>
              <td><?php echo $data[$i]['nm_bop'];?></td>
              <td><?php echo $data[$i]['kebutuhan']." ".$data[$i]['satuan'];?></td>
            </tr>
            <?php endfor ?>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-9">
        </div>
        <div class="col-xs-3">
          <div class="col-xs-6">
            <a href="<?php echo ($state == "add") ? base_url('dashboard/tambah_produk') : base_url('dashboard/edit_produk');?>">
              <button type="button" class="btn btn-info pull-right"><i></i> Kembali </button>
            </a>
          </div>
          <div class="col-xs-5">
          <a href="<?php echo ($state == "add") ? base_url('dashboard/simpan_produk') : base_url('dashboard/save_detail');?>">
            <button type="button" class="btn btn-success pull-right"><i></i> Simpan </button>
          </a>
          </div>
        </div>
      </div>
    </section>
