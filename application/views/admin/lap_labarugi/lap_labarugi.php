<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Laporan Laba Rugi</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('lap_labarugi/get_labarugi');?>" method="POST">
        <div class="row">
          <div class="col-md-4">
           <div class="form-group">
              <label>Bulan</label>
              <select name="bulan" class="form-control" data-toggle="tooltip" data-original-title="Bulan" data-placement="right">
                <option value="">Pilih Bulan</option>
                <?php for($i = 1; $i <= 12; $i ++){?>
                <option value="<?php echo $i;?>"><?php echo date("F", mktime(0, 0, 0, $i, 10));?></option>
                <?php }?>
              </select>
          </div>
          <div class="form-group">
              <label>Tahun</label>
              <select name="tahun" class="form-control" data-toggle="tooltip" data-original-title="Tahun" data-placement="right">
                <option value="">Pilih tahun</option>
                <?php $start = 2014; for($i = $start; $i <= $start + 10; $i ++){?>
                <option value="<?php echo $i;?>"><?php echo $i?></option>
                <?php }?>
              </select>
          </div>
          </div>
        </div>
      <input type="submit" class="btn btn-primary" name="Finish" value="Submit">
    </form>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
      Master Data PD. Sinko Teknik
    </div> -->
</div>
<script type="text/javascript">


  function convertToRupiah(angka){

      var rupiah = '';    
      var angkarev = angka.toString().split('').reverse().join('');
      
      for(var i = 0; i < angkarev.length; i++) 
        if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      
      return rupiah.split('',rupiah.length-1).reverse().join('');

  }
</script>
