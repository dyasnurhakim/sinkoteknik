
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>Laporan Laba Rugi
            <small class="pull-right">Bulan <?php echo date("F", mktime(0, 0, 0, $bulan, 10));?> Tahun <?php echo $tahun;?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-header -->
    <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12" style="text-align: center">
              <H2>PD. Sinko Teknik Bandung</H2>
              <H3>Laporan Laba Rugi</H3>
              <H3>per <?php echo date("F", mktime(0, 0, 0, $bulan, 10))." ".$tahun;?></H3>
            </div>
          </div>
          
        </div>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <?php $total = $totalpesanan;?>
          <table class="table table-striped table-bordered">
              <tr>
                <th colspan="2">Penjualan</th>
                <td>Rp<?php echo number_format($totalpesanan);?>,-</td>
                <td></td>
              </tr>
              <tr>
                <th colspan="2">HPP / Laporan Biaya</th>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td width="1px"></td>
                <td>Produksi</td>
                <td>Rp <?php echo number_format($totalproduksi);?>,-</td>
                <td></td>
              </tr>
              <tr>
                <th colspan="2">Laba Kotor</th>
                <td></td>
                <td>Rp <?php $total -= $totalproduksi; echo number_format($total);?>,-</td>
              </tr>
               <tr>
                <th colspan="2">HPP / Laporan Biaya</th>
                <td></td>
                <td></td>
              </tr>
              <?php foreach($listbop as $bop){?>
              <?php   if(!empty($totalbop[$bop->kd_bop])){?>
              <tr>
                <td width="1px"></td>
                <td><?php echo $bop->nm_bop;?></td>
                <td>Rp <?php echo number_format($totalbop[$bop->kd_bop]);?>,-</td>
                <td><?php $total -= $totalbop[$bop->kd_bop];?></td>
              </tr>
              <?php   }?>
              <?php }?>
              <tr>
                <th colspan="2">Laba Bersih</th>
                <td></td>
                <td>Rp <?php echo number_format($total);?>,-</td>
              </tr>
            </table>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
      Master Data PD. Sinko Teknik
    </div> -->

</section>