<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Pembelian Bahan Baku</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo (!empty($supplier_pembelian)) ? base_url('trx_bbaku/tambah_bbaku') : base_url('trx_bbaku/tambah_supplier');?>" method="POST">
        <div class="row">
        	<div class="col-md-6">
			    <div class="form-group">
			      <label for="id">Supplier :</label>
			        <input list="list_supplier" class="form-control reset" 
			        	placeholder="Isi id..." name="kd_supplier" id="id" 
			        	autocomplete="off" required="" <?php echo (!empty($supplier_pembelian)) ? 'readonly="true" value="'.$supplier_pembelian->kd_supplier.'"' : '';?>>
	                  <datalist id="list_supplier" name="kd_supplier">
	                  	<?php foreach ($supplier as $row): ?>
	                  		<option value="<?= $row->kd_supplier ?>"><?= $row->nm_supplier ?></option>
	                  	<?php endforeach ?>
	                  </datalist>
			    </div>
		        <?php
		        	if(!empty($supplier_pembelian)){
		        ?>
			     <div class="form-group">
			      <label for="id">Bahan Baku :</label>
			        <input list="list_bbaku" class="form-control reset" 
			        	placeholder="Isi id..." name="id_bb" id="id" 
			        	autocomplete="off" required="">
	                  <datalist id="list_bbaku" name="id_bb">
	                  	<?php foreach ($bbaku as $row): ?>
	                  		<option value="<?= $row->id ?>"><?= $row->nama_bb ?></option>
	                  	<?php endforeach ?>
	                  </datalist>
			    </div>
			   <div class="form-group">
            		<label for="harga">Harga Beli</label>
            		<input type="number" class="form-control reset" autocomplete="off" id="harga" min="0" name="harga" placeholder="Isi harga..." onchange="subTotal(this.value)" onkeyup="subTotal(this.value)">
            	</div>
            	<div class="form-group">
            		<label for="qty">Quantity </label>
            		<input type="number" class="form-control reset" autocomplete="off" id="qty" min="0" name="qty" placeholder="Isi qty..." onchange="subTotal(this.value)" onkeyup="subTotal(this.value)">
            	</div>
		        <?php
		        	}
		        ?>
            </div>
            <div class="col-md-4">
            <?php
            	if(!empty($supplier_pembelian)){
            ?>
             	<div class="form-group">
                		<label for="nama_supplier">Nama Supplier</label>
                		<input type="text" class="form-control reset" id="nama_produk" readonly="" name="nm_supplier" value="<?php echo $supplier_pembelian->nm_supplier ?>" >
                </div>
            <?php
            	}
            ?>
                <div class="form-group">
                		<label for="nama_produk">Tanggal</label>
                		<input type="text" class="form-control reset" id="nama_produk" readonly="" name="tgl_transaksi" value="<?php echo date('Y-m-d'); ?>" >
                </div>
            <?php
            	if(!empty($supplier_pembelian)){
            ?>
                <div class="form-group">
				     	<label for="sub_total">Sub-Total (Rp):</label>
				        <input type="text" class="form-control reset" name="sub_total" id="sub_total" readonly="readonly">
			    </div>
			<?php
            	}
            ?>
            </div>
            <!-- /.col -->
        </div>
        <?php
        	if(!empty($supplier_pembelian)){
        ?>
        	<div class="form-group">
        	<input type="submit" class="btn btn-success" name="Finish" value="Tambah Bahan">
    </form>
        	<a href="<?php echo base_url('trx_bbaku/change_supplier')?>"><button class="btn btn-primary" type="button"> Ganti Supplier</button></a>
        	</div>
        <?php
        	} else {
        ?>
			<input type="submit" class="btn btn-success" name="Finish" value="Tambah">
    </form>
		<?php
        	}
        ?>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Detail Pembelian</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
        <div class="row">
        	<div class="col-md-12">
              	<table id="table_id" class="table table-striped table-bordered">
				      <thead>
				        <tr>
		                    <th>NO</th>
		                    <th>ID SUPPLIER</th>
		                    <th>NAMA SUPPLIER</th>
		                    <th>NAMA BAHAN BAKU</th>
		                    <th>QTY</th>
		                    <th>HARGA BELI</th>
		                    <th>SUBTOTAL</th>
		                    <th style="width:125px;">ACTION</p></th>
				        </tr>
				      </thead>
				      <tbody>
		                <?php
		                if(!empty($detail_pembelian)){
		                	$data = $detail_pembelian['data'];
			                for($i = 0 ; $i < $detail_pembelian['last']; $i++){?>
	                    <tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]['kd_supplier'];?></td>
							<td><?php echo $data[$i]['nm_supplier'];?></td>
							<td><?php echo $data[$i]['nama_bb'];?></td>
							<td><?php echo $data[$i]['qty'];?></td>
							<td><?php echo $data[$i]['harga'];?></td>
							<td><?php echo ($data[$i]['harga'] * $data[$i]['qty']);?></td>
                            <td>
                                <a href="<?php echo base_url('trx_bbaku/remove_bbaku').'/'.$i;?>"><button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></a>
                            </td>
	                    </tr>
                     <?php } ?>
                     	<tr>
                     		<td colspan="8">
                     			<a href="<?php echo base_url('trx_bbaku/check_pembelian');?>"><button class="btn btn-warning">Finish</button></a>
                     			<a href="<?php echo base_url('trx_bbaku/cancel_pembelian');?>"><button class="btn btn-danger">Cancel</button></a>
                     		</td>
                     	</tr>
                     <?php
                     	}
                     ?>

				      </tbody>
				    </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<script type="text/javascript">
    function subTotal(){

        var harga = $('#harga').val().replace(/Rp|,|-/g, "").replace(".","");
        var qty = $('#qty').val();
        console.log(harga);

        $('#sub_total').val(convertToRupiah(harga * qty));
    }

	function convertToRupiah(angka){

	    var rupiah = '';    
	    var angkarev = angka.toString().split('').reverse().join('');
	    
	    for(var i = 0; i < angkarev.length; i++) 
	      if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
	    
	    return "Rp. " + rupiah.split('',rupiah.length-1).reverse().join('') + ",-";

	}
</script>
