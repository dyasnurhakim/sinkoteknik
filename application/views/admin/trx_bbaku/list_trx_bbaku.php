
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Bahan Baku</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
              <tr>
                <th>NO</th>
                <th>KD PEMBELIAN</th>
                <th>TANGGAL BELI</th>
                <th>TOTAL</th>
                <th style="width:125px;">ACTION</p></th>
              </tr>
          </thead>
          <tbody>
          <?php  $i=1;foreach($data_pembelian as $row){?>
              <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $row->kd_pembelian;?></td>
                  <td><?php echo $row->tgl_beli;?></td>
                  <td><?php echo $row->total;?></td>
                  <td>
                      <a href="<?php echo base_url('trx_bbaku/detail_pembelian').'/'.$row->kd_pembelian;?>"><button class="btn btn-primary" ><i class="glyphicon glyphicon-list-alt"></i></button></a>
                  </td>
              </tr>
          <?php }?>
          </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
  
<script src="<?php echo base_url('assets/jquery/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>

  <script type="text/javascript">
  function convertToRupiah(angka){

      var rupiah = '';    
      var angkarev = angka.toString().split('').reverse().join('');
      
      for(var i = 0; i < angkarev.length; i++) 
        if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      
      return rupiah.split('',rupiah.length-1).reverse().join('');

  }
  $(document).ready( function () {
      $('#table_id').DataTable({
        "aLengthMenu": [[5, 10, 20, 25, -1], [5, 10, 20, 25, "All"]],
        "iDisplayLength": 5
      });
      //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });
  } );
    var save_method; //for save method string
    var table;
 
 
    function add_bb()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    function edit_bb(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('bbaku/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="id"]').val(data.id);
            $('[name="nama_bb"]').val(data.nama_bb);
            $('[name="harga"]').val(data.harga);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Bahan Baku'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
 
 
 
    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('bbaku/bb_add')?>";
      }
      else
      {
        url = "<?php echo site_url('bbaku/bb_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
 
    function delete_bb(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('bbaku/bb_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
      }
    }
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah Bahan Baku</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">ID BB</label>
              <div class="col-md-9">
               <input readonly="" name="id" placeholder="Id BB" class="form-control" type="text" required="" value=<?php echo "$kodeunik"; ?>>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama BB</label>
              <div class="col-md-9">
                <input name="nama_bb" placeholder="Nama BB" class="form-control" type="text" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Harga BB</label>
              <div class="col-md-9">
                <input name="harga" placeholder="Harga Produk" class="form-control" type="text" required="">
              </div>
            </div>
           
                       <!--  <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-9">
                                <input name="tanggal" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div> -->
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    