<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> PD.Sinko Teknik
            <small class="pull-right">Tanggal: <?php echo date('Y-m-d'); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
    
      <div class="col-xs-7 ">
        <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <th>Kode Pembelian</th>
                  <td><?php echo $pembelian->kd_pembelian; ?></td>
                </tr>
                <tr>
                  <th>Tanggal Beli</th>
                  <td><?php echo $pembelian->tgl_beli; ?></td>
                </tr>
                <tr>
                  <th>Total</th>
                  <td>Rp. <?php echo number_format($pembelian->total);?>,-</td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Pembelian Bahan Baku</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Bahan Baku</th>
              <th>Supplier</th>
              <th>Qty</th>
              <th>Harga Beli</th>
              <th>SubTotal</th>
            </tr>
            </thead>
            <tbody>

            <?php $total = 0; $i = 1; foreach($datadetail_pembelian as $row): ?>
            <tr>
              <td><?php echo $i++;?></td>
              <td><?php echo $row->kd_pembelian;?></td>
              <td><?php echo $row->nm_supplier;?></td>
              <td><?php echo $row->qty_beli;?></td>
              <td>Rp. <?php echo number_format($row->harga_beli)?>,-</td>
              <td>Rp. <?php echo number_format($row->total_beli)?>,-</td>
            </tr>
            <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="<?php echo base_url('trx_bbaku/list_pembelian');?>">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i></i> Kembali
            </button>
          </a>
        </div>
      </div>
    </section>
