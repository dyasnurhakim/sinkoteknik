<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> PD.Sinko Teknik
            <small class="pull-right">Tanggal: <?php echo date('Y-m-d'); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Admin.</strong><br>
            Bandung<br>
            Phone: (804) 123-5432<br>
            Email: Admin@gmail.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $customer->nama_pelanggan ?></strong><br>
            <?php echo $customer->alamat ?><br>
            Telp: <?php echo $customer->tlp ?><br>
            Email: <?php echo $customer->email ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Kode Transaksi : #<?php echo $produksi_produk['kd_produksi'] ?></b><br>
          <b>Tanggal Transaksi : <?php echo $produksi_produk['tgl_transaksi'] ?> </b><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="col-xs-7 ">
        <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <th>Nama Produk</th>
                  <td><?php echo $produk->nm_produk; ?></td>
                </tr>
                <tr>
                  <th>Harga Jual</th>
                  <td>Rp. <?php echo number_format($produk->harga_prd); ?>,-</td>
                </tr>
                <tr>
                  <th>Qty</th>
                  <td><?php echo $produk->qty_pesanan; ?></td>
                </tr>
                <tr>
                  <th>Total</th>
                  <td>Rp. <?php echo number_format($produk->qty_pesanan * $produk->harga_prd);?>,-</td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
        <?php if(!array_key_exists($kd_produk, $karyawan_produk)) {?>
        <form action="<?php echo base_url('trx_produksi/tambah_pekerjaan_produk');?>" method="POST">
        <input name="kd_produk" value="<?= $kd_produk;?>" type="hidden">
        <?php } ?>
          <legend>Detail Pekerjaan</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Pekerjaan</th>
              <th>Nama Pekerjaan</th>
              <th>Jumlah Hari</th>
              <th>Karyawan</th>
              <?php if(isset($karyawan_produk[$kd_produk])) {?>
              <th>Tarif / Jam</th>
              <th>Subtotal</th>
              <?php }?>
            </tr>
            </thead>
            <tbody>
            <?php $total = 0; $i = 1; foreach($pekerjaan as $row_pekerjaan): ?>

            <?php if(array_key_exists($kd_produk, $karyawan_produk)) {
                    foreach ($karyawan_produk[$kd_produk][$row_pekerjaan->kd_pekerjaan]['karyawan'] as $row_karyawan){
            ?>
              <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $row_pekerjaan->kd_pekerjaan;?></td>
                <td><?php echo $row_pekerjaan->nama_pekerjaan;?></td>
                <td><?php echo $row_pekerjaan->jumlah_hari;?></td>
                <td><?php echo $row_karyawan['nm_karyawan']?></td>
                <td><?php echo number_format($row_karyawan['tarif'])?></td>
                <td><?php echo number_format($row_karyawan['tarif'] * $row_pekerjaan->jumlah_hari);?></td>
                <?php $total += ($row_karyawan['tarif'] * $row_pekerjaan->jumlah_hari); ?>
              </tr>
            <?php        
                    }
                  } else {
            
            ?>
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $row_pekerjaan->kd_pekerjaan;?></td>
                <td><?php echo $row_pekerjaan->nama_pekerjaan;?></td>
                <td><?php echo $row_pekerjaan->jumlah_hari;?></td>
                 <td>
                  <select name="<?= $row_pekerjaan->kd_pekerjaan ?>[]" id="id" class="selectpicker form-control" multiple>
                     <?php foreach ($karyawan[$row_pekerjaan->kd_pekerjaan] as $row): ?>
                        <option value="<?= $row->id_karyawan ?>" title="<?= $row->nm_karyawan ?>"><?= $row->nm_karyawan." | Rp ".number_format($row->tarif).",-" ?></option>
                      <?php endforeach ?>
                  </select>
                </td>
              </tr>
            <?php
                  }
            ?>
            <?php endforeach ?>
            <?php if(array_key_exists($kd_produk, $karyawan_produk)) {?>
            <tr>
              <th colspan="6" style="text-align:center">Total</th>
              <th><?php echo number_format($total); ?></th>
            </tr>
            <?php }?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Bahan Baku</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Bahan Baku</th>
              <th>Nama Bahan Baku</th>
              <th>Satuan</th>
              <th>Harga Beli</th>
              <th>Jumalah BB</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; $total=0; foreach($bbaku as $row_bbaku): ?>
            <tr>
               <td><?php echo $i++;?></td>
              <td><?php echo $row_bbaku->kd_bbaku;?></td>
              <td><?php echo $row_bbaku->nama_bb;?></td>
              <td><?php echo $row_bbaku->satuan;?></td>
              <td><?php echo number_format($row_bbaku->harga)?></td>
              <td><?php echo ($row_bbaku->qty * $produk->qty_pesanan);?></td>
              <td><?php echo number_format(($row_bbaku->qty * $produk->qty_pesanan) * $row_bbaku->harga)?></td>
            </tr>
            <?php $total = $total + (($row_bbaku->qty * $produk->qty_pesanan) * $row_bbaku->harga); ?>
            <?php endforeach ?>
            <tr>
              <th colspan="5" style="text-align:center">Total</th>
              <th><?php echo number_format($total); ?></th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Kelompok Aktivitas</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Aktivitas</th>
              <th>Nama Aktivitas</th>
              <th>Satuan Aktivitas</th>
              <th>Kebutuhan Produk</th>
              <th>Nominal</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; $total=0; foreach($bop as $row_bop): ?>
            <tr>
               <td><?php echo $i++;?></td>
              <td><?php echo $row_bop->kd_bop;?></td>
              <td><?php echo $row_bop->nm_bop;?></td>
              <td><?php echo $row_bop->satuan_bop;?></td>
              <td><?php echo ($row_bop->qty * $produk->qty_pesanan)." ".$row_bop->satuan_bop;?></td>
              <td><?php echo number_format($row_bop->nominal)?></td>
              <td><?php echo number_format(($row_bop->qty * $produk->qty_pesanan) *$row_bop->nominal)?></td>
            </tr>
            <?php $total = $total + (($row_bop->qty * $produk->qty_pesanan) * $row_bop->nominal); ?>
            <?php endforeach ?>
            <tr>
              <th colspan="4" style="text-align:center">Total</th>
              <th><?php echo number_format($total); ?></th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
      <div class="row no-print">
        <div class="col-xs-12">
        <?php if(!array_key_exists($kd_produk, $karyawan_produk)) {?>
            <button type="submit" class="btn btn-warning pull-right" style="margin-right: 5px;">
              <i></i> Simpan
            </button>
        </form>
        <?php } ?>
          <a href="<?php echo base_url('trx_produksi');?>">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i></i> Kembali
            </button>
          </a>
        </div>
      </div>
    </section>
    <script type="text/javascript">
      $('#id').selectpicker({
        onChange: function(element, checked) {
            var brands = $('#id option:selected');
            var selected = [];
            $(brands).each(function(index, brand){
                selected.push([$(this).val()]);
            });

            console.log(selected);
        }
      });
      


    </script>
