<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Produksi</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo (empty($produksi_produk)) ? base_url('trx_produksi/produksi_produk') : base_url('trx_produksi/detail_produk');?>" method="POST">
        <div class="row">
        	<div class="col-md-6">
			    <div class="form-group">
			      <label for="id">Pesanan :</label>
			        <input list="list_customer" class="form-control reset" 
			        	placeholder="Isi id..." name="kd_pesanan" id="id_pesanan" 
			        	autocomplete="off" required="" onblur="showCustomer(this.value)" onchange="showCustomer(this.value)" onkeyup="showCustomer(this.value)" <?php echo (!empty($produksi_produk)) ? 'readonly="true" value="'.$produksi_produk['kd_pesanan'].'"' : '';?>>
	                  <datalist id="list_customer" name="kd_pesanan">
	                  	<?php foreach ($pesanan as $row): ?>
	                  		<option value="<?= $row->kd_pesanan ?>"><?= $row->nama_pelanggan ?></option>
	                  	<?php endforeach ?>
	                  </datalist>
			    </div>
			    <?php
			    	if(!empty($produksi_produk)){
			    ?>
			    <div class="form-group">
					<label for="id">Produk :</label>
			        <input list="list_produk" class="form-control reset" 
			        	placeholder="Isi id..." name="kd_produk" id="id" 
			        	autocomplete="off" required="" onblur="showDetailProduk(this.value)" onchange="showDetailProduk(this.value)" onkeyup="showDetailProduk(this.value)">
	                  <datalist id="list_produk" name="kd_produk">';
						<?php foreach ($produk as $row): ?>
	                  		<option value="<?= $row->kd_produk ?>"><?= $row->nm_produk ?></option>
	                  	<?php endforeach ?>
					</datalist>
				</div>
				<div class="form-group">
            		<label for="nm_produk">Nama Produk </label>
            		<input type="text" class="form-control reset" autocomplete="off" id="nm_produk" min="0" name="nm_produk" readonly="true" >
            	</div>
				<div class="form-group">
            		<label for="harga_produk">Harga Jual</label>
            		<input type="text" class="form-control reset" autocomplete="off" id="harga_produk" name="harga_produk" readonly="true" >
            	</div>
			    <?php
			    	}
			    ?>
            </div>
            <div class="col-md-4">
            	<div class="form-group">
                		<label for="kode_transaksi">Kode Produksi</label>
                		<input type="text" class="form-control reset" id="kode_transaksi" readonly="" name="kd_produksi" value="<?php echo (!empty($produksi_produk)) ? $produksi_produk['kd_produksi'] : $kodeunik; ?>" >
                </div>
                <div class="form-group">
                		<label for="nama_pelanggan">Nama Pelanggan</label>
                		<input type="text" class="form-control reset" id="nama_pelanggan" readonly="" name="nm_pelanggan" value="<?php echo (!empty($produksi_produk)) ? $produksi_produk['nm_pelanggan'] : ''?>">
                		<input type="hidden" class="form-control reset" id="id_pelanggan" readonly="" name="id_pelanggan" value="<?php echo (!empty($produksi_produk)) ? $produksi_produk['id_pelanggan'] : ''?>">
                </div>
                <div class="form-group">
                		<label for="tgl_transaksi">Tanggal</label>
                		<input type="text" class="form-control reset" id="tgl_transaksi" readonly="" name="tgl_transaksi" value="<?php echo date('Y-m-d'); ?>" >
                </div>
            </div>
            <!-- /.col -->
        </div>
        <?php
        	if(empty($produksi_produk)){
        ?>
		<input type="submit" class="btn btn-success" name="Finish" value="Check Produksi">
    </form>
    	<?php
        	} else {
        ?>
        <div class="form-group">
        <a href="<?php echo base_url('trx_produksi/detail_produk');?>"><button class="btn btn-primary" type="submit">Lihat Detail Produk</button></a>
  	</form>
  		<a href="<?php echo base_url('trx_produksi/check_produksi')."/".$produksi_produk['kd_pesanan'];?>"><button class="btn btn-success" type="button">Produksi</button></a>
  		<a href="<?php echo base_url('trx_produksi/cancel_produksi_produk');?>"><button class="btn btn-danger" type="button">Batal Produksi</button></a>
        </div>
  		
        <?php
        	}
    	?>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<script type="text/javascript">

	function subTotal(){

		var harga = $('#harga_produk').val().replace(/Rp|,|-/g, "").replace(".","");
		var qty = $('#qty').val();
		console.log(harga);

		$('#sub_total').val(convertToRupiah(harga * qty));
	}

	function convertToRupiah(angka){

	    var rupiah = '';    
	    var angkarev = angka.toString().split('').reverse().join('');
	    
	    for(var i = 0; i < angkarev.length; i++) 
	      if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
	    
	    return "Rp. " + rupiah.split('',rupiah.length-1).reverse().join('') + ",-";

	}

	function showCustomer(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('trx_produksi/getOneCustomer')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				$("#nama_pelanggan").val(res.data.nama_pelanggan);
				$("#id_pelanggan").val(res.data.kd_pelanggan);
				showProduk(id);
			}
		});
	}

	function showDetailProduk(obj){
		var produk = obj;
		var pesanan = $("#id_pesanan").val();
		$.ajax({
			url:"<?php echo site_url('trx_produksi/getOneDetailPesanan')?>/"+pesanan+"/"+produk,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				 $("#harga_produk").val(convertToRupiah(res.data.harga_prd));
				 $("#qty").val(res.data.qty_pesanan);
				 $("#nm_produk").val(res.data.nm_produk);
				 subTotal();
			}
		});
	}
</script>


