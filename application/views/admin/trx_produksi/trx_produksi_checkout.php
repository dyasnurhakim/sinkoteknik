<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> PD.Sinko Teknik
            <small class="pull-right">Tanggal: <?php echo date('Y-m-d'); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Admin.</strong><br>
            Bandung<br>
            Phone: (804) 123-5432<br>
            Email: Admin@gmail.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $customer->nama_pelanggan ?></strong><br>
            <?php echo $customer->alamat ?><br>
            Telp: <?php echo $customer->tlp ?><br>
            Email: <?php echo $customer->email ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Kode Transaksi : #<?php echo $produksi_produk['kd_produksi'] ?></b><br>
          <b>Tanggal Transaksi : <?php echo $produksi_produk['tgl_transaksi'] ?> </b><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <?php foreach($listproduk as $produk){?>
      <div class="col-xs-7 ">
        <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <th>Nama Produk</th>
                  <td><?php echo $produk->nm_produk; ?></td>
                </tr>
                <tr>
                  <th>Harga Jual</th>
                  <td>Rp. <?php echo number_format($produk->harga_prd); ?>,-</td>
                </tr>
                <tr>
                  <th>Qty</th>
                  <td><?php echo $produk->qty_pesanan; ?></td>
                </tr>
                <tr>
                  <th>Total</th>
                  <td>Rp. <?php echo number_format($produk->qty_pesanan * $produk->harga_prd);?>,-</td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Pekerjaan</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Pekerjaan</th>
              <th>Nama Pekerjaan</th>
              <th>Jumlah Hari</th>
              <th>Karyawan</th>
              <th>Tarif / Jam</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $total = 0; $i = 1; foreach($pekerjaan[$produk->kd_produk] as $row_pekerjaan): ?>
            <?php foreach($karyawan_produk[$produk->kd_produk][$row_pekerjaan->kd_pekerjaan]['karyawan'] as $row_karyawan): ?>
            <tr>
              <td><?php echo $i++;?></td>
              <td><?php echo $row_pekerjaan->kd_pekerjaan;?></td>
              <td><?php echo $row_pekerjaan->nama_pekerjaan;?></td>
              <td><?php echo $row_pekerjaan->jumlah_hari;?></td>
                <td><?php echo $row_karyawan['nm_karyawan']?></td>
                <td><?php echo number_format($row_karyawan['tarif'])?></td>
                <td><?php echo number_format($row_karyawan['tarif'] * $row_pekerjaan->jumlah_hari);?></td>
                <?php $total += ($row_karyawan['tarif'] * $row_pekerjaan->jumlah_hari); ?>
            </tr>
            <?php   endforeach ?>
            <?php endforeach ?>
            <tr>
              <th colspan="6" style="text-align:center">Total</th>
              <th><?php echo number_format($total); ?></th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Bahan Baku</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Bahan Baku</th>
              <th>Nama Bahan Baku</th>
              <th>Satuan</th>
              <th>Harga Beli</th>
              <th>Jumalah BB</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; $total=0; foreach($bbaku[$produk->kd_produk] as $row_bbaku): ?>
            <tr>
               <td><?php echo $i++;?></td>
              <td><?php echo $row_bbaku->kd_bbaku;?></td>
              <td><?php echo $row_bbaku->nama_bb;?></td>
              <td><?php echo $row_bbaku->satuan;?></td>
              <td><?php echo number_format($row_bbaku->harga)?></td>
              <td><?php echo $row_bbaku->qty * $produk->qty_pesanan;?></td>
              <td><?php echo number_format($row_bbaku->qty * $produk->qty_pesanan * $row_bbaku->harga)?></td>
            </tr>
            <?php $total = $total + ($row_bbaku->qty * $produk->qty_pesanan * $row_bbaku->harga); ?>
            <?php endforeach ?>
            <tr>
              <th colspan="5" style="text-align:center">Total</th>
              <th><?php echo number_format($total); ?></th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Kelompok Aktivitas</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Kode Aktivitas</th>
              <th>Nama Aktivitas</th>
              <th>Satuan Aktivitas</th>
              <th>Kebutuhan Produk</th>
              <th>Nominal</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; $total=0; foreach($bop[$produk->kd_produk] as $row_bop): ?>
            <tr>
               <td><?php echo $i++;?></td>
              <td><?php echo $row_bop->kd_bop;?></td>
              <td><?php echo $row_bop->nm_bop;?></td>
              <td><?php echo $row_bop->satuan_bop;?></td>
              <td><?php echo $row_bop->qty * $produk->qty_pesanan." ".$row_bop->satuan_bop;?></td>
              <td><?php echo number_format($row_bop->nominal)?></td>
              <td><?php echo number_format($row_bop->nominal * $row_bop->qty * $produk->qty_pesanan)?></td>
            </tr>
            <?php $total = $total + ($row_bop->nominal * $row_bop->qty * $produk->qty_pesanan); ?>
            <?php endforeach ?>
            <tr>
              <th colspan="5" style="text-align:center">Total</th>
              <th><?php echo number_format($total); ?></th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-xs-12">
          &nbsp;
        </div>
        <!-- /.col -->
      </div>
      <?php }?>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <a href="<?php echo base_url('trx_produksi/simpan_produksi');?>">
            <button type="submit" class="btn btn-warning pull-right" style="margin-right: 5px;">
              <i></i> Simpan
            </button>
          </a>
          <a href="<?php echo base_url('trx_produksi');?>">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i></i> Kembali
            </button>
          </a>
        </div>
      </div>
    </section>
    <script type="text/javascript">
      function showKaryawan(obj, kode){
        var id = obj;
        var kode = kode;
        $.ajax({
          url:"<?php echo site_url('karyawan/ajax_edit')?>/"+id,
          type:'GET',
          dataType: 'json',
          success: function(res) {
             $("#tarif_" + kode).val(res.tarif);
          }
        });
      }
    </script>
