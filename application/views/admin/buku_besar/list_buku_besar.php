
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>Buku Besar PD. Sinko Teknik Bandung
            <small class="pull-right">Bulan <?php echo $bulan;?> Tahun <?php echo $tahun;?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12" style="text-align: center">
              <H2>PD. Sinko Teknik Bandung</H2>
              <H3>Laporan Buku Besar</H3>
              <H3>per <?php echo date("F", mktime(0, 0, 0, $bulan, 10))." ".$tahun;?></H3>
            </div>
          </div>
          
        </div>
    <!-- /.box-header -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-bordered" >
				      <tr>
                <th rowspan="2">Tanggal</th>
                <th rowspan="2">Nama Akun</th>
                <th rowspan="2">No. Akun</th>
                <th rowspan="2">Debit</th>
                <th rowspan="2">Kredit</th>
                <th colspan="2" style="text-align: center;">Saldo</th>
              </tr>
              <tr>
                <th>Debit</th>
                <th>Kredit</th>
              </tr>
              <?php 
                if($data[0]->type == "Debit"){
                  $type = "Debit";
                } else {
                  $type = "Kredit";
                }
                    foreach($data as $row){
                        
              ?>

              <tr>
                <td><?php echo $row->tgl_jurnal;?></td>
                <td><?php echo $row->nama_akun;?></td>
                <td><?php echo $row->kd_akun;?></td>
                <td><?php echo ($row->type == "Debit") ? "Rp ".number_format($row->nominal).",-" : "";?></td>
                <td><?php echo ($row->type == "Kredit") ? "Rp ".number_format($row->nominal).",-" : "";?></td>
                <td><?php echo ($type == "Debit") ? "Rp ".number_format($row->saldo).",-" : "";?></td>
                <td><?php echo ($type == "Kredit") ? "Rp ".number_format($row->saldo).",-" : "";?></td>
              </tr>
              <?php 
                    }
              ?>
                      
              
				    </table>
        </div>
        <!-- /.col -->
      </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->

</section>