
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pemesanan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
              <tr>
                <th>NO</th>
                <th>KD PESANAN</th>
                <th>NAMA PELANGGAN</th>
                <th>TANGGAL PESAN</th>
                <th style="width:125px;">ACTION</p></th>
              </tr>
          </thead>
          <tbody>
          <?php  $i=1;foreach($data_pemesanan as $row){?>
              <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $row->kd_pesanan;?></td>
                  <td><?php echo $row->nama_pelanggan;?></td>
                  <td><?php echo $row->tgl_pesan;?></td>
                  <td>
                      <a href="<?php echo base_url('trx_pemesanan/detail_pesanan').'/'.$row->kd_pesanan;?>"><button class="btn btn-primary" ><i class="glyphicon glyphicon-list-alt"></i></button></a>
                  </td>
              </tr>
          <?php }?>
          </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
  
<script src="<?php echo base_url('assets/jquery/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/media/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>