<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> PD.Sinko Teknik
            <small class="pull-right">Tanggal: <?php echo date('Y-m-d'); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
    
      <div class="col-xs-7 ">
        <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <th>Kode Pesanan</th>
                  <td><?php echo $pesanan->kd_pesanan; ?></td>
                </tr>
                <tr>
                  <th>Pelanggan</th>
                  <td><?php echo $pesanan->nama_pelanggan; ?></td>
                </tr>
                <tr>
                  <th>Tanggal Beli</th>
                  <td><?php echo $pesanan->tgl_pesan; ?></td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <legend>Detail Pemesanan Produk</legend>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Produk</th>
              <th>Qty</th>
              <th>Harga Jual</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>

            <?php $total = 0; $i = 1; foreach($datadetail_pesanan as $row): ?>
            <tr>
              <td><?php echo $i++;?></td>
              <td><?php echo $row->nm_produk;?></td>
              <td><?php echo $row->qty_pesanan;?></td>
              <td>Rp. <?php echo number_format($row->harga_prd)?>,-</td>
              <td>Rp. <?php $total += $row->harga_prd * $row->qty_pesanan; echo number_format($row->harga_prd * $row->qty_pesanan)?>,-</td>
            </tr>
            <?php endforeach ?>
            <tr>
              <td colspan="3"></td>
              <td align="right"><b>Total :</b></td>
              <td>Rp. <?php echo number_format($total);?></td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="<?php echo base_url('trx_pemesanan/list_pesanan');?>">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
              <i></i> Kembali
            </button>
          </a>
        </div>
      </div>
    </section>
