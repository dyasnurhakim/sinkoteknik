<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Pemesanan Produk</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
    <form action="<?php echo base_url('trx_pemesanan/tambah_produk');?>" method="POST">
        <div class="row">
        	<div class="col-md-6">
			    <div class="form-group">
			      <label for="id">Pelanggan :</label>
			        <input list="list_customer" class="form-control reset" 
			        	placeholder="Isi id..." name="kd_customer" id="id" 
			        	autocomplete="off" required="" onblur="showCustomer(this.value)" onchange="showCustomer(this.value)" onkeyup="showCustomer(this.value)" <?php echo (!empty($detail_pembelian)) ? 'readonly="true" value="'.$detail_pembelian['id_pelanggan'].'"' : '';?>>
	                  <datalist id="list_customer" name="kd_customer">
	                  	<?php foreach ($customer as $row): ?>
	                  		<option value="<?= $row->id ?>"><?= $row->nama_pelanggan ?></option>
	                  	<?php endforeach ?>
	                  </datalist>
			    </div>
			     <div class="form-group">
			      <label for="id">Produk :</label>
			        <input list="list_produk" class="form-control reset" 
			        	placeholder="Isi id..." name="id_produk" id="id" 
			        	autocomplete="off" required="" onblur="showProduk(this.value)" onchange="showProduk(this.value)" onkeyup="showProduk(this.value)" >
	                  <datalist id="list_produk" name="id_produk">
	                  	<?php foreach ($produk as $row): ?>
	                  		<option value="<?= $row->kd_produk ?>"><?= $row->nm_produk ?></option>
	                  	<?php endforeach ?>
	                  </datalist>
			    </div>
			   <div class="form-group">
            		<label for="harga_produk">Harga Jual</label>
            		<input type="text" class="form-control reset" autocomplete="off" id="harga_produk" min="0" name="harga_produk" placeholder="Isi harga..."  onchange="subTotal()" onkeyup="subTotal()" readonly="true" >
            	</div>
            	<div class="form-group">
            		<label for="qty">Quantity </label>
            		<input type="number" class="form-control reset" autocomplete="off" id="qty" min="0" name="qty" placeholder="Isi qty..." onchange="subTotal()" onkeyup="subTotal()">
            	</div>
            </div>
            <div class="col-md-4">
            	<div class="form-group">
                		<label for="kode_transaksi">Kode Transaksi</label>
                		<input type="text" class="form-control reset" id="kode_transaksi" readonly="" name="kode_transaksi" value="<?php echo (!empty($detail_pembelian)) ? $detail_pembelian['kodeunik'] : $kodeunik; ?>" >
                </div>
                <div class="form-group">
                		<label for="nama_pelanggan">Nama Pelanggan</label>
                		<input type="text" class="form-control reset" id="nama_pelanggan" readonly="" name="nama_pelanggan" value="<?php echo (!empty($detail_pembelian)) ? $detail_pembelian['nama_pelanggan'] : ''?>">
                </div>
                <div class="form-group">
                		<label for="tgl_transaksi">Tanggal</label>
                		<input type="text" class="form-control reset" id="tgl_transaksi" readonly="" name="tgl_transaksi" value="<?php echo date('Y-m-d'); ?>" >
                </div>
                <div class="form-group">
				     	<label for="sub_total">Sub-Total (Rp):</label>
				        <input type="text" class="form-control reset" name="sub_total" id="sub_total" readonly="readonly">
			    </div>
            </div>
            <!-- /.col -->
        </div>
		<input type="submit" class="btn btn-success" name="Finish" value="Tambah">
    </form>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Detail Pemesanan Produk</h3>
        <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
        <div class="row">
        	<div class="col-md-12">
              	<table id="table_id" class="table table-striped table-bordered">
				      <thead>
				        <tr>
		                    <th>NO</th>
		                    <th>ID PRODUK</th>
		                    <th>NAMA PRODUK</th>
		                    <th>QTY</th>
		                    <th>HARGA JUAL</th>
		                    <th>SUBTOTAL</th>
		                    <th style="width:125px;">ACTION</p></th>
				        </tr>
				      </thead>
				      <tbody>
		                <?php
		                if(!empty($detail_pembelian)){
		                	$data = $detail_pembelian['data'];
			                for($i = 0 ; $i < $detail_pembelian['last']; $i++){?>
	                    <tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo $data[$i]['id_produk'];?></td>
							<td><?php echo $data[$i]['nm_produk'];?></td>
							<td><?php echo $data[$i]['qty'];?></td>
							<td><?php echo $data[$i]['harga'];?></td>
							<td><?php echo ($data[$i]['harga'] * $data[$i]['qty']);?></td>
                            <td>
                                <a href="<?php echo base_url('trx_pemesanan/remove_produk').'/'.$i;?>"><button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></a>
                            </td>
	                    </tr>
                     <?php } ?>
                     	<tr>
                     		<td colspan="8">
                     			<a href="<?php echo base_url('trx_pemesanan/check_pemesanan');?>"><button class="btn btn-warning">Finish</button></a>
                     			<a href="<?php echo base_url('trx_pemesanan/cancel_pemesanan');?>"><button class="btn btn-danger">Cancel</button></a>
                     		</td>
                     	</tr>
                     <?php
                     	}
                     ?>

				      </tbody>
				    </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="display: block;">
    	Master Data PD. Sinko Teknik
    </div> -->
</div>
<script type="text/javascript">

	function subTotal(){

		var harga = $('#harga_produk').val().replace(/Rp|,|-/g, "").replace(".","");
		var qty = $('#qty').val();
		console.log(harga);

		$('#sub_total').val(convertToRupiah(harga * qty));
	}

	function convertToRupiah(angka){

	    var rupiah = '';    
	    var angkarev = angka.toString().split('').reverse().join('');
	    
	    for(var i = 0; i < angkarev.length; i++) 
	      if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
	    
	    return "Rp. " + rupiah.split('',rupiah.length-1).reverse().join('') + ",-";

	}

	function showCustomer(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('trx_pemesanan/getOneCustomer')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				$("#nama_pelanggan").val(res.data.nama_pelanggan);
			}
		});
	}

	function showProduk(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('trx_pemesanan/getOneProduk')?>/"+id,
			type:'GET',
			dataType: 'json',
			success: function(res) {
				$("#harga_produk").val(convertToRupiah(res.data.harga));
				subTotal();
			}
		});
	}
</script>
