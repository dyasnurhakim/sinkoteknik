<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>T</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>PD</b>.Sinko Teknik</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?php echo site_url('dashboard/logout');?>">
              <span class="hidden-xs">Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('dashboard'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview <?php echo(
                                        $active_menu=='bbaku' || 
                                        $active_menu == 'produk' || 
                                        $active_menu == 'pekerjaan' ||
                                        $active_menu == 'coa' ||
                                        $active_menu == 'oh' ||
                                        $active_menu == 'customer' ||
                                        $active_menu == 'user' ||
                                        $active_menu == 'supplier' ||
                                        $active_menu == 'karyawan'
                                      )? "active" : "";?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Kelola data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo($active_menu=='karyawan')? " class='active'" : "";?>><a href="<?php echo base_url('karyawan'); ?>"><i class="fa fa-angle-right"></i> Karyawan</a></li>
            <li <?php echo($active_menu=='pekerjaan')? " class='active'" : "";?>><a href="<?php echo base_url('pekerjaan'); ?>"><i class="fa fa-angle-right"></i> Pekerjaan</a></li>
            <li <?php echo($active_menu=='customer')? " class='active'" : "";?>><a href="<?php echo base_url('customer'); ?>"><i class="fa fa-angle-right"></i> Pelanggan</a></li>
            <li <?php echo($active_menu=='supplier')? " class='active'" : "";?>><a href="<?php echo base_url('supplier'); ?>"><i class="fa fa-angle-right"></i> Supplier</a></li>
            <li <?php echo($active_menu=='bbaku')? " class='active'" : "";?>><a href="<?php echo base_url('bbaku'); ?>"><i class="fa fa-angle-right"></i> Bahan Baku</a></li>
            <li <?php echo($active_menu=='produk')? " class='active'" : "";?>><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-angle-right"></i> Produk</a></li>
            <li <?php echo($active_menu=='coa')? " class='active'" : "";?>><a href="<?php echo base_url('coa'); ?>"><i class="fa fa-angle-right"></i> COA</a></li>
            <li <?php echo($active_menu=='oh')? " class='active'" : "";?>><a href="<?php echo base_url('biaya_oh'); ?>"><i class="fa fa-angle-right"></i> Biaya OH</a></li>
          </ul>
        </li>
        <li class="treeview <?php echo(
                                        $active_menu=='trx_bbaku' || 
                                        $active_menu == 'data_trx_bbaku' || 
                                        $active_menu == 'trx_pemesanan' || 
                                        $active_menu == 'data_trx_pemesanan' || 
                                        $active_menu == 'trx_produksi' ||
                                        $active_menu == 'trx_pengeluaran'
                                      )? "active" : "";?>">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo($active_menu=='trx_pemesanan')? " class='active'" : "";?>><a href="<?php echo base_url('trx_pemesanan'); ?>"><i class="fa fa-circle-o"></i> Pemesanan</a></li>
            <li <?php echo($active_menu=='data_trx_pemesanan')? " class='active'" : "";?>><a href="<?php echo base_url('trx_pemesanan/list_pesanan'); ?>"><i class="fa fa-circle-o"></i> Data Pemesanan</a></li>
            <li <?php echo($active_menu=='trx_bbaku')? " class='active'" : "";?>><a href="<?php echo base_url('trx_bbaku'); ?>"><i class="fa fa-circle-o"></i> Pembelian Bahan Baku</a></li>
            <li <?php echo($active_menu=='data_trx_bbaku')? " class='active'" : "";?>><a href="<?php echo base_url('trx_bbaku/list_pembelian'); ?>"><i class="fa fa-circle-o"></i> Data Pembelian Bahan Baku</a></li>
            <li <?php echo($active_menu=='trx_produksi')? " class='active'" : "";?>><a href="<?php echo base_url('trx_produksi'); ?>"><i class="fa fa-circle-o"></i> Produksi</a></li>
            <li <?php echo($active_menu=='trx_pengeluaran')? " class='active'" : "";?>><a href="<?php echo base_url('trx_pengeluaran'); ?>"><i class="fa fa-circle-o"></i> Pengeluaran</a></li>
          </ul>
        </li>
        <li class="treeview <?php echo(
                                        $active_menu == 'lap_produksi' ||
                                        $active_menu == 'jurnal' ||
                                        $active_menu == 'buku_besar' ||
                                        $active_menu == 'lap_labarugi'
                                      )? "active" : "";?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo($active_menu=='jurnal')? " class='active'" : "";?>><a href="<?php echo base_url('jurnal'); ?>"><i class="fa fa-circle-o"></i> Jurnal</a></li>
            <li <?php echo($active_menu=='buku_besar')? " class='active'" : "";?>><a href="<?php echo base_url('buku_besar'); ?>"><i class="fa fa-circle-o"></i> Buku Besar</a></li>
            <li <?php echo($active_menu=='lap_produksi')? " class='active'" : "";?>><a href="<?php echo base_url('lap_produksi'); ?>"><i class="fa fa-circle-o"></i> Biaya Produksi</a></li>
            <li <?php echo($active_menu=='lap_labarugi')? " class='active'" : "";?>><a href="<?php echo base_url('lap_labarugi'); ?>"><i class="fa fa-circle-o"></i> Laba Rugi</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <small>PD. Sinko Teknik</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">